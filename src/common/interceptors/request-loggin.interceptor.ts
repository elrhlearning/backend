import { Injectable, NestInterceptor, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { RequestLogger } from '../loggers/request-logger';

/**
 * @author KARIM EL RHOURHA
 * @description log every incomming request and request duration
 */
@Injectable()
export class RequestLoggingInterceptor implements NestInterceptor {
  private nbRequest : number = 0;
//******************************************************************************************************************************
  constructor(private readonly logger : RequestLogger){

  }
//******************************************************************************************************************************  
  intercept(context: ExecutionContext,call$: Observable<any>,): Observable<any> {
    let req = context.switchToHttp().getRequest();
    this.nbRequest++;
    this.logger.getLogger().info(
          `Incoming %s request number ${this.nbRequest}, path : %s\nbody : %s\nheader : %s\nuser : %s`, 
          req.method,req.baseUrl,JSON.stringify(req.body),JSON.stringify(req.headers),JSON.stringify(req.user));
    const now = Date.now();
    return call$.pipe(
      tap(() => {
        this.logger.getLogger().info(
          `End of request number ${this.nbRequest}, After... ${Date.now() - now}ms`);
      })
      );
}
//******************************************************************************************************************************
}