import { Injectable} from '@nestjs/common';
import {Logger,} from 'winston';
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;
@Injectable()
export class AppLogger{
//******************************************************************************************************************************
    protected winstonLogger : Logger;
    private fileName = undefined;
    private loggerFormat : any = printf(({ level, message, label, timestamp }) => {
        return `\n*****************************************\n${timestamp} - File : ${this.fileName} - ${level}:\n${message}`;
      });
//******************************************************************************************************************************     
    constructor(){
        this.winstonLogger = createLogger({
            level: 'info',
            format: this.getFormat(),
            transports: this.getTransports()
          });
        this.getLogger().info('creating logger');
    }
 //******************************************************************************************************************************
    getLogger() : Logger {
        return this.winstonLogger;
    }
//******************************************************************************************************************************    
  /**
   * AppLogger log in info.log file and error in error.log file
   */  
  protected getTransports() : any {
          return[
            new transports.File({ filename: 'logs/error.log', level: 'error', handleExceptions: true }),
            new transports.File({ filename: 'logs/info.log' }),
          ]
    }
//******************************************************************************************************************************    
    protected getFormat() : any {
        return format.combine(
            format.splat(),
            timestamp(),
            this.loggerFormat
          );
    }
//****************************************************************************************************************************** 
    init(fileName : string){
      this.fileName = fileName;
    }
//******************************************************************************************************************************
    logObject(obj : any) : string {
      if(obj)
        return JSON.stringify(obj,null,"\t");
      return "";
    }
}
