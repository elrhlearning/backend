import { AppLogger } from './app-logger';
import { Injectable} from '@nestjs/common';
const DailyRotateFile = require('winston-daily-rotate-file');

@Injectable()
export class RequestLogger extends AppLogger{
//******************************************************************************************************************************     
    constructor(){
        super();
    }
//******************************************************************************************************************************    
  /**
   * Request logger log in a daily rotate file where name is ngNestRequests-DATE.log
   */  
  protected getTransports() : any {
          return[
            new DailyRotateFile({
                filename: 'ngNestRequests-%DATE%.log',
                datePattern: 'YYYY-MM-DD-HH',
                zippedArchive: true,
                maxSize: '20m',
                maxFiles: '14d',
                dirname : 'logs'
              }),
          ]
    }
//******************************************************************************************************************************    
}
