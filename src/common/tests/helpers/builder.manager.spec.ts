import { SubscribeReviewNotification } from './../../../database/entity/notifications/sunscribe-notification.entity';
import { UpdateReviewNotification } from './../../../database/entity/notifications/update-review-notification.entity';
import { SubscribeNotificationBuilder } from '../../../common/helpers/subscribe-notification.builder';
import { NotificationBuilderMananger } from '../../../common/helpers/builder.manager';
import { UpdateReviewNotificationBuilder } from '../../../common/helpers/update-review-notification.builder';
import { Notification } from '../../../database/entity/notifications/notification.abstract.entity';




describe('NotificationBuilderMananger tests', () => {
  let manager : NotificationBuilderMananger;
//*************************************************************************************  
  beforeAll(async () => {
    manager = new NotificationBuilderMananger();
  });

  it('test maneger',()=> {
    expect(manager).toBeDefined();
  })

  it('test instance of SubscribeNotificationBuilder',()=> {
    let builder : SubscribeNotificationBuilder = <SubscribeNotificationBuilder>manager.getBuilder(SubscribeNotificationBuilder);
    expect(builder).toBeDefined();
    expect(builder instanceof SubscribeNotificationBuilder).toBeTruthy();
    let notif : Notification =
    builder
    .start()
    .setSubscribtion(null)
    .setUser(null)
    .setState(true)
    .build();
    expect(notif instanceof SubscribeReviewNotification).toBeTruthy();
  });

  it('test instance of UpdateReviewNotificationBuilder',()=> {
    let builder = <UpdateReviewNotificationBuilder>manager.getBuilder(UpdateReviewNotificationBuilder);
    expect(builder).toBeDefined();
    expect(builder instanceof UpdateReviewNotificationBuilder).toBeTruthy();
    let notif : Notification =
    builder
    .start()
    .setDateModification(null)
    .setUser(null)
    .setState(true)
    .build();
    expect(notif instanceof UpdateReviewNotification).toBeTruthy();
  });
});