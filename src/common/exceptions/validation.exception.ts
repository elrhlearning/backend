import {HttpStatus,BadRequestException} from "@nestjs/common";

export class ValidationException extends BadRequestException{
    public errors;
    constructor(errors) {
        super(errors);
        this.errors = errors;
    }
}