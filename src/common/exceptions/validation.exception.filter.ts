import { type } from 'os';
import {Catch,ArgumentsHost } from "@nestjs/common";
import {ExceptionFilter} from "@nestjs/common/interfaces/exceptions";
import {ValidationException} from "./validation.exception";

@Catch(ValidationException)
export class ValidationExceptionFilter implements ExceptionFilter {
//****************************************************************************************    
    catch(exception: ValidationException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const request = ctx.getRequest();
        const status = exception.getStatus();
    
        response
          .status(status)
          .json({
            statusCode: status,
            timestamp: new Date().toISOString(),
            path: request.url,
            type : 'invalid_data',
            errors : this.minimizeErrorsObject(exception.errors)
          });
    }
//****************************************************************************************    
    private minimizeErrorsObject(errors : Array<any>) : Object {
        let resut : any = [];
        errors.forEach(element => {
            let error = {
                property : element.property,
                message : element.constraints
            }
            resut.push(error);
        });
        return resut;
    }
//****************************************************************************************    
}