import { Injectable } from '@nestjs/common';
import { ValidationOptions, validate } from 'class-validator';
import { AppEntity } from '../../database/entity/app-entity.abstract';
import { plainToClass } from 'class-transformer';
import { ValidationException } from '../exceptions/validation.exception';
@Injectable()
export class EntityValidator {
    
    async validate(object: any, validationOptions: ValidationOptions, type : typeof AppEntity): Promise<AppEntity> {
        if(!(object instanceof type))
            object = plainToClass(type,<Object>object);
        const errors: any = await validate(object,validationOptions);      
        if (errors.length > 0) {
            throw new ValidationException(errors);
        }
        return object;
    } 
}