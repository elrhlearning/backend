import { RequestLoggingInterceptor } from './interceptors/request-loggin.interceptor';
import { AppLogger } from './loggers/app-logger';
import { Module, Global } from '@nestjs/common';
import { RequestLogger } from './loggers/request-logger';
import { NotificationBuilderMananger } from './helpers/builder.manager';
import { RelationApplicator } from './interfaces/relation-applicator.class';
import { EntityValidator } from './validator/entity.validator';


@Global()
@Module({
  providers: [
              AppLogger,RequestLogger,RequestLoggingInterceptor,
              NotificationBuilderMananger,RelationApplicator,
              EntityValidator
            ],
  exports : [
              AppLogger,RequestLogger,RequestLoggingInterceptor,
              NotificationBuilderMananger,RelationApplicator,
              EntityValidator
    ]
})
export class CommonModule {}
