import { User } from '../../database/entity/user/user.entity';
export abstract class AbstractGateways {
//***********************************************************************************************    
    protected connectedUsers = new Map<string, User>(); 
    protected clients : any = [];
//***********************************************************************************************
    protected handleConnection(client: any, ...args: any[]) {
        this.clients.push(client);
        this.connectedUsers.set(client.id,<User> JSON.parse(client.handshake.query.user));
    }
//***********************************************************************************************
  /**
   * get userSocket if it is connected to messenger
   * @param user 
   */
  protected getClientSocket(user : User) : any{
    let notified : any = undefined;
    this.clients.forEach(client => {
      if(this.connectedUsers.get(client.id).id == user.id){
        notified = client;
        return;
      }
    });
    return notified;
  }  
//***********************************************************************************************
  protected handleDisconnect(client: any) {
    this.clients = this.clients.filter(c => {
      if(c.id != client.id)
        return c;
    });
    this.connectedUsers.delete(client.id);
  }
  //***********************************************************************************************   
}