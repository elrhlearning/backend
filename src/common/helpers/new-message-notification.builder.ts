import { NotificationBuilder } from "./abstract-notification.builder";
import { NewMessageNotification } from "../../database/entity/notifications/new-message-notification.entity";
import { Message } from "../../database/entity/messenger/message/message.entity";

export class NewMessageNotificationBuilder extends NotificationBuilder {
  start(): NewMessageNotificationBuilder {
    this.notification = new NewMessageNotification();
    return this;
  }
  protected cast(): NewMessageNotification {
    return <NewMessageNotification>this.notification;
  }
  constructor(){
    super();
  }

  setMessage(message : Message){
    this.cast().newMessage = message;
    return this;
  }
}