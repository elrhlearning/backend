import { UpdateReviewNotification } from '../../database/entity/notifications/update-review-notification.entity';
import { ReviewNotificationBuilder } from './review-notification.builder.1';
import { CommentReviewNotification } from '../../database/entity/notifications/comment-review-notification.entity';
import { UpdateReviewNotificationBuilder } from './update-review-notification.builder';
import { Comment } from '../../database/entity/comments/comment.entity';
export  class CommentedReviewNotificationBuilder extends UpdateReviewNotificationBuilder {

  protected cast() : CommentReviewNotification {
    return (<CommentReviewNotification>this.notification);
  }
  constructor(){
    super()
  }
  start(): CommentedReviewNotificationBuilder {
    this.notification = new CommentReviewNotification();
    return this;
  }

  setComment(comment? : Comment) : CommentedReviewNotificationBuilder{
    this.cast().comment  = comment;
    this.cast().comment.dateCreation = undefined;
    return this;
  }

}