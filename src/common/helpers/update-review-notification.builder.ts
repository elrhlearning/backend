import { UpdateReviewNotification } from '../../database/entity/notifications/update-review-notification.entity';
import { ReviewNotificationBuilder } from './review-notification.builder.1';
export  class UpdateReviewNotificationBuilder extends ReviewNotificationBuilder {

  protected cast() : UpdateReviewNotification {
    return (<UpdateReviewNotification>this.notification);
  }
  constructor(){
    super()
  }
  start(): UpdateReviewNotificationBuilder {
    this.notification = new UpdateReviewNotification();
    return this;
  }

  setDateModification(date? : string) : UpdateReviewNotificationBuilder{
    this.cast().dateModification  = date;
    return this;
  }
}