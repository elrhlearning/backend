import { NotificationBuilder } from './abstract-notification.builder';
import { Review } from 'src/database/entity/review/review.entity';
import { ReviewNotification } from 'src/database/entity/notifications/review-notification.entity';
export abstract class ReviewNotificationBuilder extends NotificationBuilder {

  constructor(){
    super()
  }

  public setReview(review : Review) : ReviewNotificationBuilder{
    (<ReviewNotification>this.notification).review =review;
    return this;
  }
}