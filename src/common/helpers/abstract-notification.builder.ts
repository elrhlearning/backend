import { User } from './../../database/entity/user/user.entity';
import { Notification } from 'src/database/entity/notifications/notification.abstract.entity';
export abstract class NotificationBuilder {
  protected notification : Notification;
  constructor(){

  }
  setUser(user : User) : NotificationBuilder {
    this.notification.notifiedUser = user;
    return this;
  }
  setState(state : boolean) : NotificationBuilder{
    this.notification.state = state;
    return this;
  }
  abstract start() : NotificationBuilder;
  protected abstract cast() : Notification;
  build() :  Notification {
    return this.notification;
  }
}