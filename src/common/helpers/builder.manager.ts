import { Injectable } from "@nestjs/common";
import { NotificationBuilder } from "./abstract-notification.builder";
import { SubscribeNotificationBuilder } from './subscribe-notification.builder';
import { UpdateReviewNotificationBuilder } from './update-review-notification.builder';
import { CommentedReviewNotificationBuilder } from './commentted-review-notification.builder';
import { NewMessageNotificationBuilder } from "./new-message-notification.builder";


@Injectable()
export class NotificationBuilderMananger {
  private builders : NotificationBuilder[] = [];
  constructor(){
    this.builders.push(new SubscribeNotificationBuilder());
    this.builders.push(new UpdateReviewNotificationBuilder());
    this.builders.push(new CommentedReviewNotificationBuilder());
    this.builders.push(new NewMessageNotificationBuilder());
  }
  getBuilder(builderType : typeof NotificationBuilder) : NotificationBuilder {
    let builderResult : NotificationBuilder = undefined;
    this.builders.forEach((builder : NotificationBuilder)=>{
      if(builder instanceof builderType){
        builderResult =  builder;
      }
    });
    return builderResult;
  }

}