import { SubscribeReviewNotification } from '../../database/entity/notifications/sunscribe-notification.entity';
import { User } from '../../database/entity/user/user.entity';
import { Notification } from 'src/database/entity/notifications/notification.abstract.entity';
import { NotificationBuilder } from './abstract-notification.builder';
import { ReviewNotificationBuilder } from './review-notification.builder.1';
import { Subscribtion } from 'src/database/entity/subscribtion/subscribtion.entity';
export  class SubscribeNotificationBuilder extends ReviewNotificationBuilder {

  protected cast() : SubscribeReviewNotification {
    return (<SubscribeReviewNotification>this.notification);
  }
  constructor(){
    super()
  }
  start(): SubscribeNotificationBuilder {
    this.notification = new SubscribeReviewNotification();
    return this;
  }

  setSubscribtion(subscribtion : Subscribtion){
    this.cast().subscribtion = subscribtion;
    this.setReview(undefined);
    if(subscribtion && subscribtion.review && subscribtion.review.user){
      this.setUser(subscribtion.review.user);
      subscribtion.review.user = undefined;
      subscribtion.review.items = undefined;
      subscribtion.review.image = undefined;
    } 

    return this
  }
}