import { Injectable } from "@nestjs/common";
import { SelectQueryBuilder } from "typeorm";
import { EntityRelation } from "./entity-relation.interface";

@Injectable()
export class RelationApplicator {
apply(queryBuilder : SelectQueryBuilder<any>, relations : EntityRelation[]) : SelectQueryBuilder<any> {
  if(queryBuilder){
    relations.forEach((relation : EntityRelation)=>{
      if(relation.notSelected)
        queryBuilder =  queryBuilder.leftJoin(relation.relation,relation.alias);
      else 
        queryBuilder =  queryBuilder.leftJoinAndSelect(relation.relation,relation.alias);
    })
  }
  return queryBuilder;
}
}