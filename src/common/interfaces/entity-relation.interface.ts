export interface EntityRelation{
  relation : string,
  alias : string,
  notSelected?: boolean;
  selected ? : string[];
}