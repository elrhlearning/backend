import { ValidationExceptionFilter } from './common/exceptions/validation.exception.filter';
import { NestFactory } from '@nestjs/core';
import { AppModule } from "./app.module";
import { join } from 'path';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // cors
  app.enableCors({
    origin : 'http://localhost:4200',
    allowedHeaders : ['Content-Type', 'Authorization','Accept'],
    methods : ['GET', 'PUT', 'POST','DELETE']
  });
  // prefix
  app.setGlobalPrefix('api/v1');
  app.useStaticAssets(join(__dirname, '..', 'public'));
  app.useGlobalFilters(new ValidationExceptionFilter());
  // pipes
  app.useGlobalPipes();
  await app.listen(3000);
}
bootstrap();
