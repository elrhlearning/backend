import { messengerRepositoryProviders } from '../providers/repositorys.provider';
import { UserModule } from '../../user/user.module';
import { IUserService } from '../../user/services/user.service.interface';
import { User } from '../../database/entity/user/user.entity';
import { TestingModule, Test } from '@nestjs/testing';
import { DatabaseModule } from '../../database/database.module';
import { CommonModule } from '../../common/common.module';
import { UserServiceToken } from '../../user/config';
import { IConversationRepository } from '../repositorys/conversation/conversation-repository.interface';
import { IMessageRepository } from '../repositorys/message/message-repository.interface';
import { ConversationRepositoryToken, MessageRepositoryToken } from '../config';
import { Conversation } from '../../database/entity/messenger/conversation/conversation.entity';
import { Where, OrderBy } from '../../review/services/query-helper.interface';
import { conversEntityMetaData } from '../../database/entity/messenger/conversation/conversation.metadata';
import { EntityRelation } from '../../common/interfaces/entity-relation.interface';
import { Message } from '../../database/entity/messenger/message/message.entity';
import { messageEntityMetaData } from '../../database/entity/messenger/message/message.metadata';
import { userEntityMetaData } from '../../database/entity/user/user.metadata';



describe('Message Repository tests', () => {
  let userService : IUserService;
  let convRepository : IConversationRepository;
  let msgRepository : IMessageRepository;
  let users : User[];
  let message : Message;
  let message2 : Message;
  let conv : Conversation;
  let conv2 : Conversation;
  let user : User;
//*************************************************************************************
  beforeAll(async () => {
      const module: TestingModule = await Test.createTestingModule({
          imports : [UserModule,DatabaseModule,CommonModule],
          providers : messengerRepositoryProviders
      }).compile();
      userService = module.get<IUserService>(UserServiceToken);
      convRepository = module.get<IConversationRepository>(ConversationRepositoryToken);
      msgRepository = module.get<IMessageRepository>(MessageRepositoryToken);
  });
//*************************************************************************************
  it('should be defined', () => {
      expect(userService).toBeDefined();
      expect(convRepository).toBeDefined();
      expect(msgRepository).toBeDefined();
  });
//*************************************************************************************  
  it('should get user associed to review', async ()  => {
      users = (await userService.findAll());
      expect(users.length).toBeGreaterThan(1);
  });
 //*************************************************************************************
  it('should add conversations', async ()  => {
    conv = new Conversation();
    conv.users = [users[0],users[1]];
    message = new Message();
    message.sender=conv.users[0];
    message.content = `First mesasge from ${message.sender.name}`;
    conv.messages = [message];
    conv = await convRepository.saveConversation(conv);
    expect(conv.id).toBeDefined();
    expect(conv.messages[0].id).toBeDefined();

    conv2 = new Conversation();
    conv2.users = [users[0],users[1]];
    message = new Message();
    message.sender=conv.users[0];
    message.content = `Another mesasge from ${message.sender.name} in another conv`;
    conv2.messages = [message];
    conv2 = await convRepository.saveConversation(conv2);
    expect(conv2.id).toBeDefined();
    expect(conv2.messages[0].id).toBeDefined();
  });
 //*************************************************************************************
 it('should add message', async ()  => {
    message2 = new Message();
    message2.sender=conv.users[0];
    message2.content = `Second mesasge from ${message.sender.name}`;
    message2.conversation = conv;
    conv.messages.push(message2);
    message2 = await msgRepository.saveMessage(message2);
    expect(message2.id).toBeDefined();
});
//************************************************************************************
  test('should get messages', async ()  => {
    
    let nbElement : number = 2 ;
      // conv
      let convWhere : Where = {
        where : conversEntityMetaData.columnsAlias.id + ' = :val',
        params : {val : conv.id}
      };
      // relatiion
      let userRelation : EntityRelation = {
        relation  : messageEntityMetaData.relations.sender,
        alias : userEntityMetaData.tableName
      }; 
      let convRelation : EntityRelation = {
        relation  : messageEntityMetaData.relations.conversation,
        alias : conversEntityMetaData.tableName
      }; 
      // order 
      let dateOrder : OrderBy = {
        column  : messageEntityMetaData.columnsAlias.dateCreation,
        order : 'DESC'
      } ;         
      // get message
      let r : Message[] = await msgRepository.getMessages([convWhere],[userRelation,convRelation],[dateOrder],20,0);
      expect(r).toBeDefined();
      expect(r.length).toBeLessThanOrEqual(nbElement);
      expect(r[0].conversation).toBeDefined();
      expect(r[1].conversation).toBeDefined();
  });    
//*************************************************************************************  
  test('should get latest messages', async ()  => {
    let msgs = await msgRepository.getLatestMessages(users[1]);
    expect(msgs).toBeDefined();
  });
//************************************************************************************* 
  test('should delete conversation', async ()  => {
      conv = await convRepository.deleteConversation(conv);
      expect(conv.id).toBeUndefined();
  });
//*************************************************************************************

});
