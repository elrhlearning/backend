import { messengerRepositoryProviders } from '../providers/repositorys.provider';
import { UserModule } from '../../user/user.module';
import { IUserService } from '../../user/services/user.service.interface';
import { User } from '../../database/entity/user/user.entity';
import { TestingModule, Test } from '@nestjs/testing';
import { DatabaseModule } from '../../database/database.module';
import { CommonModule } from '../../common/common.module';
import { UserServiceToken } from '../../user/config';
import { IConversationRepository } from '../repositorys/conversation/conversation-repository.interface';
import { ConversationRepositoryToken, ConversationServiceToken } from '../config';
import { Conversation } from '../../database/entity/messenger/conversation/conversation.entity';
import { Where, OrderBy } from '../../review/services/query-helper.interface';
import { conversEntityMetaData } from '../../database/entity/messenger/conversation/conversation.metadata';
import { EntityRelation } from '../../common/interfaces/entity-relation.interface';
import { Message } from '../../database/entity/messenger/message/message.entity';
import { messageEntityMetaData } from '../../database/entity/messenger/message/message.metadata';
import { IConversationService } from '../services/conversation/conversation-service.interface';
import { messengerServiceProviders } from '../providers/services.provider';



describe('Conversation Service tests', () => {
  let userService : IUserService;
  let convRepository : IConversationRepository;
  let convService : IConversationService;
  let users : User[];
  let message : Message;
  let conv : Conversation;
  let user : User;
//*************************************************************************************
  beforeAll(async () => {
      const module: TestingModule = await Test.createTestingModule({
          imports : [UserModule,DatabaseModule,CommonModule],
          providers : [...messengerRepositoryProviders,...messengerServiceProviders]
      }).compile();
      userService = module.get<IUserService>(UserServiceToken);
      convRepository = module.get<IConversationRepository>(ConversationRepositoryToken);
      convService = module.get<IConversationService>(ConversationServiceToken);
  });
//*************************************************************************************
  it('should be defined', () => {
      expect(userService).toBeDefined();
      expect(convRepository).toBeDefined();
      expect(convService).toBeDefined();
  });
//*************************************************************************************  
  it('should get user associed to review', async ()  => {
      users = (await userService.findAll());
      expect(users.length).toBeGreaterThan(1);
  });
 //*************************************************************************************
  it('should add conversations', async ()  => {
    conv = new Conversation();
    conv.users = [users[0],users[1]];
    message = new Message();
    message.sender=conv.users[0];
    message.content = `First mesasge from ${message.sender.name}`;
    conv.messages = [message];
    conv = await convService.save(conv);
    expect(conv.id).toBeDefined();
    expect(conv.messages[0].id).toBeDefined();
  });
 //*************************************************************************************
  it('should get conversation with all users', async ()  => {
    let conv2 = await convService.getWithUsers(conv);
    expect(conv2).toBeDefined();
    expect(conv2.users).toBeDefined();
    expect(conv2.users.length).toEqual(2);
  });
//************************************************************************************
  test('should get all conversations', async ()  => {      
    // get conversations
    let r : Conversation[] = await convService.getAllConversations(users[1]);
    expect(r).toBeDefined();
    expect(r.length).toBeGreaterThanOrEqual(1);
    expect(r[0].messages).toBeDefined();
    expect(r[0].messages.length).toEqual(conv.messages.length);
    expect(r[0].users).toBeDefined();
    expect(r[0].users.length).toEqual(conv.users.length);
  });    
//*************************************************************************************  
  test('should delete conversation', async ()  => {
      conv = await convRepository.deleteConversation(conv);
      expect(conv.id).toBeUndefined();
  });
//*************************************************************************************

});
