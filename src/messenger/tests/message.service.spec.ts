import { messengerRepositoryProviders } from '../providers/repositorys.provider';
import { UserModule } from '../../user/user.module';
import { IUserService } from '../../user/services/user.service.interface';
import { User } from '../../database/entity/user/user.entity';
import { TestingModule, Test } from '@nestjs/testing';
import { DatabaseModule } from '../../database/database.module';
import { CommonModule } from '../../common/common.module';
import { UserServiceToken } from '../../user/config';
import { IConversationRepository } from '../repositorys/conversation/conversation-repository.interface';
import { ConversationRepositoryToken, ConversationServiceToken, MessageServiceToken } from '../config';
import { Conversation } from '../../database/entity/messenger/conversation/conversation.entity';
import { Where, OrderBy } from '../../review/services/query-helper.interface';
import { conversEntityMetaData } from '../../database/entity/messenger/conversation/conversation.metadata';
import { EntityRelation } from '../../common/interfaces/entity-relation.interface';
import { Message } from '../../database/entity/messenger/message/message.entity';
import { messageEntityMetaData } from '../../database/entity/messenger/message/message.metadata';
import { IConversationService } from '../services/conversation/conversation-service.interface';
import { messengerServiceProviders } from '../providers/services.provider';
import { IMessageService } from '../services/message/message-service.interface';



describe('Conversation Service tests', () => {
  let userService : IUserService;
  let convRepository : IConversationRepository;
  let convService : IConversationService;
  let msgService : IMessageService;
  let users : User[];
  let message : Message;
  let conv : Conversation;
  let date = null;
//*************************************************************************************
  beforeAll(async () => {
      const module: TestingModule = await Test.createTestingModule({
          imports : [UserModule,DatabaseModule,CommonModule],
          providers : [...messengerRepositoryProviders,...messengerServiceProviders]
      }).compile();
      userService = module.get<IUserService>(UserServiceToken);
      convRepository = module.get<IConversationRepository>(ConversationRepositoryToken);
      convService = module.get<IConversationService>(ConversationServiceToken);
      msgService = module.get<IMessageService>(MessageServiceToken);
  });
//*************************************************************************************
  it('should be defined', () => {
      expect(userService).toBeDefined();
      expect(convRepository).toBeDefined();
      expect(convService).toBeDefined();
      expect(msgService).toBeDefined();
  });
//*************************************************************************************  
  it('should get user associed to review', async ()  => {
      users = (await userService.findAll());
      expect(users.length).toBeGreaterThan(1);
  });
 //*************************************************************************************
  it('should add conversations', async ()  => {
    conv = new Conversation();
    conv.users = [users[0],users[1]];
    message = new Message();
    message.sender=conv.users[0];
    message.dateCreation = '2019-03-21 12:11:21';
    message.content = `First mesasge from ${message.sender.name}`;
    conv.messages = [message];
    conv = await convService.save(conv);
    expect(conv.id).toBeDefined();
    expect(conv.messages[0].id).toBeDefined();
  });   
//*************************************************************************************  
  test('should add message', async ()  => {      
    message = new Message();
    message.sender=conv.users[1];
    message.content = `Response message from ${conv.users[1].id}`;
    message.conversation = conv;
    message.dateCreation = '2019-03-21 13:11:21';
    message = await msgService.save(message);
    expect(message).toBeDefined();
    expect(message.id).toBeDefined();
  });    
//************************************************************************************
  test('should conversation messages', async ()  => {   
    let r : Message[] = await msgService.getConversationMessages(conv,10,1);
    expect(r).toBeDefined();
    expect(r.length).toBeLessThanOrEqual(9);
    expect(r[0].sender).toBeDefined();
  });
//************************************************************************************
  test('should get latest message', async ()  => {      
    let r : Message[] = await msgService.getLatestMessages(users[1]);
    expect(r).toBeDefined();
    expect(r.length).toBeGreaterThanOrEqual(1);
    expect(r[0].sender).toBeDefined();
    expect(r[0].content).toEqual(`Response message from ${conv.users[1].id}`);
  });   
//*************************************************************************************  
/*   test('should delete conversation', async ()  => {
      conv = await convRepository.deleteConversation(conv);
      expect(conv.id).toBeUndefined();
  }); */
//*************************************************************************************

});
