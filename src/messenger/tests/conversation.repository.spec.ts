import { messengerRepositoryProviders } from '../providers/repositorys.provider';
import { UserModule } from '../../user/user.module';
import { IUserService } from '../../user/services/user.service.interface';
import { User } from '../../database/entity/user/user.entity';
import { TestingModule, Test } from '@nestjs/testing';
import { DatabaseModule } from '../../database/database.module';
import { CommonModule } from '../../common/common.module';
import { UserServiceToken } from '../../user/config';
import { IConversationRepository } from '../repositorys/conversation/conversation-repository.interface';
import { ConversationRepositoryToken } from '../config';
import { Conversation } from '../../database/entity/messenger/conversation/conversation.entity';
import { Where, OrderBy } from '../../review/services/query-helper.interface';
import { conversEntityMetaData } from '../../database/entity/messenger/conversation/conversation.metadata';
import { EntityRelation } from '../../common/interfaces/entity-relation.interface';
import { Message } from '../../database/entity/messenger/message/message.entity';
import { messageEntityMetaData } from '../../database/entity/messenger/message/message.metadata';



describe('Conversation Repository tests', () => {
  let userService : IUserService;
  let convRepository : IConversationRepository;
  let users : User[];
  let message : Message;
  let conv : Conversation;
  let user : User;
//*************************************************************************************
  beforeAll(async () => {
      const module: TestingModule = await Test.createTestingModule({
          imports : [UserModule,DatabaseModule,CommonModule],
          providers : messengerRepositoryProviders
      }).compile();
      userService = module.get<IUserService>(UserServiceToken);
      convRepository = module.get<IConversationRepository>(ConversationRepositoryToken);
  });
//*************************************************************************************
  it('should be defined', () => {
      expect(userService).toBeDefined();
      expect(convRepository).toBeDefined();
  });
//*************************************************************************************  
  it('should get user associed to review', async ()  => {
      users = (await userService.findAll());
      expect(users.length).toBeGreaterThan(1);
  });
 //*************************************************************************************
  it('should add conversations', async ()  => {
    conv = new Conversation();
    conv.users = [users[0],users[1]];
    message = new Message();
    message.sender=conv.users[0];
    message.content = `First mesasge from ${message.sender.name}`;
    conv.messages = [message];
    conv = await convRepository.saveConversation(conv);
    expect(conv.id).toBeDefined();
    expect(conv.messages[0].id).toBeDefined();
  });

//************************************************************************************
  test('should get conversations', async ()  => {
    
    let nbElement : number = 1 ;
      // defin condition (simple condition to test operator)
      let where : Where = {
              where : conversEntityMetaData.columnsAlias.id + ' > :val',
              params : {val : '0'}
      };
      // relatiion
      let userRelation : EntityRelation = {
        relation  : conversEntityMetaData.relations.users,
        alias : 'users'
      }; 
      let messageRelation : EntityRelation = {
        relation  : conversEntityMetaData.relations.messages,
        alias : messageEntityMetaData.tableName
      }; 
      // order 
      let dateOrder : OrderBy = {
        column  : messageEntityMetaData.columnsAlias.dateCreation,
        order : 'DESC'
      } ;         
      // get conversations
      let r : Conversation[] = await convRepository.getConversations([where],[userRelation,messageRelation],[dateOrder],20,0);
      expect(r).toBeDefined();
      expect(r.length).toBeGreaterThanOrEqual(nbElement);
      expect(r[0].users).toBeDefined();
      expect(r[0].users.length).toBeGreaterThan(0);
  });    
//*************************************************************************************  
  test('should delete conversation', async ()  => {
      conv = await convRepository.deleteConversation(conv);
      expect(conv.id).toBeUndefined();
  });
//*************************************************************************************

});
