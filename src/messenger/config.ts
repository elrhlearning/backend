// repo
export const MessageRepositoryToken  = 'MsgRepoToken';
export const ConversationRepositoryToken  = 'ConvsRepoToken';
// service
export const MessageServiceToken = 'MsgServiceToken';
export const ConversationServiceToken = 'ConvServiceToken';
export const NbMessagesPeerRequest = 20;