import { EntityRelation } from '../../../common/interfaces/entity-relation.interface';
import { Where, OrderBy } from '../../../review/services/query-helper.interface';
import { Message } from '../../../database/entity/messenger/message/message.entity';
import { User } from '../../../database/entity/user/user.entity';
import { Conversation } from '../../../database/entity/messenger/conversation/conversation.entity';
export interface IMessageRepository {
    /**
     * 
     * @param message create new message in db 
     */
    saveMessage(message : Message) : Promise<Message>;
    /**
     * delete received mesasge in param
     * @param message 
     */
    deleteMessage(message : Message) : Promise<Message>;
    /**
     * Return latest messages in each conversation ordered by date desending,
     * object format :
     * [
            {
                "read": false,
                "dateCreation": "2019-03-21 13:11:21",
                "content": "Response message from 23",
                "conversation": {
                    "id": 136
                },
                "sender": {
                    "id": 23,
                    "name": "EL RHORUHA",
                    "lastname": "KARIM"
                }
            }
        ]
     * @param user u
     */
    getLatestMessages(user : User) : Promise<Message[]>;


    getConversationMessages(conversation: Conversation, take?: number, skip?: number): Promise<Message[]>
}