import { IMessageRepository } from './message-repository.interface';
import { OrderBy, Where } from '../../../review/services/query-helper.interface';
import { EntityRelation } from '../../../common/interfaces/entity-relation.interface';
import { Message } from '../../../database/entity/messenger/message/message.entity';
import { MessageRepositoryTypeORMToken } from '../../../database/config';
import { RelationApplicator } from '../../../common/interfaces/relation-applicator.class';
import { AndWhereSetter } from '../../../database/services/and-where-setter';
import { OrderBySetter } from '../../../database/services/order-by-setter';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { Inject } from '@nestjs/common';
import { messageEntityMetaData } from '../../../database/entity/messenger/message/message.metadata';
import { User } from '../../../database/entity/user/user.entity';
import { conversEntityMetaData } from '../../../database/entity/messenger/conversation/conversation.metadata';
import { userEntityMetaData } from '../../../database/entity/user/user.metadata';
import { Conversation } from '../../../database/entity/messenger/conversation/conversation.entity';
export class MessageRepository implements IMessageRepository {
//**************************************************************************************************************************************
    constructor(
        @Inject(MessageRepositoryTypeORMToken)
        private readonly msgTypeOrmRepo : Repository<Message>,
        private readonly relationApplicator : RelationApplicator,
        private readonly andWhereSetter : AndWhereSetter,
        private readonly orderSetter : OrderBySetter
    ){

    }
//**************************************************************************************************************************************
    private getMessages(wheres?: Where[], relations?: EntityRelation[], orders?: OrderBy[], take?: number, skip?: number,select?: string[]): Promise<Message[]> {
        let queryBuilder = this.getQueryBuilder();
        if(relations && relations.length > 0) {
            queryBuilder = this.relationApplicator.apply(queryBuilder,relations);
        }
        if(select)
            queryBuilder = queryBuilder.select(select);
        if(wheres && wheres.length > 0) 
            queryBuilder = <SelectQueryBuilder<Message>>this.andWhereSetter.setWheres(queryBuilder,wheres);
        if(orders && orders.length > 0) 
            queryBuilder = this.orderSetter.setOrdersBy(queryBuilder,orders);
        if(skip)
            queryBuilder = queryBuilder.skip(skip);
        if(take)
            queryBuilder = queryBuilder.take(take);  
        return queryBuilder.getMany();
    }    
//**************************************************************************************************************************************
    saveMessage(message: Message): Promise<Message> {
        return this.msgTypeOrmRepo.save(message);
    }
//**************************************************************************************************************************************
    deleteMessage(message: Message): Promise<Message> {
        return this.msgTypeOrmRepo.remove(message);
    }
//**************************************************************************************************************************************
    private getQueryBuilder() : SelectQueryBuilder<Message> {
        return this.msgTypeOrmRepo.createQueryBuilder(messageEntityMetaData.tableName);
    }
//**************************************************************************************************************************************    
    async getLatestMessages(user: User): Promise<Message[]> {
        // relation
        let convRelation : EntityRelation = {
            relation : messageEntityMetaData.relations.conversation,
            alias : conversEntityMetaData.tableName,
        };
        // message sender
        let userRelation : EntityRelation = {
            relation : messageEntityMetaData.relations.sender,
            alias : userEntityMetaData.tableName,
        };
        // order 
        let msgOrder : OrderBy = {
            column : messageEntityMetaData.columnsAlias.dateCreation,
            order : 'ASC'
        }
        let queryBuilder = this.getQueryBuilder();
        queryBuilder = this.relationApplicator.apply(queryBuilder,[convRelation,userRelation]);
        queryBuilder = queryBuilder.where(qb => {
            const subQuery = qb.subQuery()
            .select("users_conversations.conversationId")
            .from('users_conversations', "users_conversations")
            .where("users_conversations.userId = :userId")
            .getQuery();
            return `${conversEntityMetaData.columnsAlias.id} IN` + subQuery;
        })
        .setParameter("userId", user.id);
        queryBuilder = this.orderSetter.setOrdersBy(queryBuilder,[msgOrder]);
        return queryBuilder.select(
            [
                conversEntityMetaData.columnsAlias.id,
                messageEntityMetaData.columnsAlias.content,
                messageEntityMetaData.columnsAlias.dateCreation, 
                messageEntityMetaData.columnsAlias.read,
                `${userEntityMetaData.tableName}.${userEntityMetaData.columns.id.name}`,
                `${userEntityMetaData.tableName}.${userEntityMetaData.columns.name.name}`,
                `${userEntityMetaData.tableName}.${userEntityMetaData.columns.lastname.name}`
            ]
        )
        .groupBy('conversation.dateCreation')
        .getMany();
    }
//**************************************************************************************************************************************    
    getConversationMessages(conversation: Conversation, take?: number, skip?: number): Promise<Message[]> {
        // where
        let where : Where = {
            where : `${conversEntityMetaData.columnsAlias.id} = :id`,
            params : { id : conversation.id}
        };
        // relation
        let convRelation : EntityRelation = {
            relation : messageEntityMetaData.relations.conversation,
            alias : conversEntityMetaData.tableName,
            notSelected : true
        }
        let senderRelation : EntityRelation = {
            relation : messageEntityMetaData.relations.sender,
            alias : userEntityMetaData.tableName,
        }
        // order 
        let msgOrder : OrderBy = {
            column : messageEntityMetaData.columnsAlias.dateCreation,
            order : 'DESC'
        };
        // data to load when getting messages
        let select : string[] = [
            conversEntityMetaData.columnsAlias.id,
            `${userEntityMetaData.tableName}.id`,
            messageEntityMetaData.columnsAlias.content,
            messageEntityMetaData.columnsAlias.dateCreation,
            messageEntityMetaData.columnsAlias.id,
            messageEntityMetaData.columnsAlias.read
        ]
        return this.getMessages([where],[convRelation,senderRelation],[msgOrder],take,skip,select);
    }
//**************************************************************************************************************************************    
}