import { EntityRelation } from '../../../common/interfaces/entity-relation.interface';
import { Where, OrderBy } from '../../../review/services/query-helper.interface';
import { Message } from '../../../database/entity/messenger/message/message.entity';
import { Conversation } from '../../../database/entity/messenger/conversation/conversation.entity';
export interface IConversationRepository {
    getConversations(wheres? : Where[],relations? : EntityRelation[],orders? : OrderBy[],take? : number,skip?: number) : Promise<Conversation[]>;
    saveConversation(conversation : Conversation) : Promise<Conversation>;
    deleteConversation(conversation : Conversation) : Promise<Conversation>;
}