import { IConversationRepository } from './conversation-repository.interface';
import { Conversation } from '../../../database/entity/messenger/conversation/conversation.entity';
import { Where, OrderBy } from '../../../review/services/query-helper.interface';
import { EntityRelation } from '../../../common/interfaces/entity-relation.interface';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { Inject } from '@nestjs/common';
import { ConversationRepositoryTypeORMToken } from '../../../database/config';
import { conversEntityMetaData } from '../../../database/entity/messenger/conversation/conversation.metadata';
import { RelationApplicator } from '../../../common/interfaces/relation-applicator.class';
import { AndWhereSetter } from '../../../database/services/and-where-setter';
import { OrderBySetter } from '../../../database/services/order-by-setter';
export class ConversationRepository implements IConversationRepository {
//**************************************************************************************************************************************
    constructor(
        @Inject(ConversationRepositoryTypeORMToken)
        private readonly convTypeOrmRepo : Repository<Conversation>,
        private readonly relationApplicator : RelationApplicator,
        private readonly andWhereSetter : AndWhereSetter,
        private readonly orderSetter : OrderBySetter
    ){

    }
//**************************************************************************************************************************************
    getConversations(wheres?: Where[], relations?: EntityRelation[], orders?: OrderBy[], take?: number, skip?: number): Promise<Conversation[]> {
        let queryBuilder = this.getQueryBuilder();
        if(relations && relations.length > 0) {
            queryBuilder = this.relationApplicator.apply(queryBuilder,relations);
        }
        if(wheres && wheres.length > 0) 
            queryBuilder = <SelectQueryBuilder<Conversation>>this.andWhereSetter.setWheres(queryBuilder,wheres);
        if(orders && orders.length > 0) 
            queryBuilder = this.orderSetter.setOrdersBy(queryBuilder,orders);
        if(skip)
            queryBuilder = queryBuilder.skip(skip);
        if(take)
            queryBuilder = queryBuilder.take(take);    
        return queryBuilder.getMany();
    } 
//**************************************************************************************************************************************       
    saveConversation(conversation: Conversation): Promise<Conversation> {
        return this.convTypeOrmRepo.save(conversation);
    }
//**************************************************************************************************************************************
    deleteConversation(conversation: Conversation): Promise<Conversation> {
        return this.convTypeOrmRepo.remove(conversation);
    }
//**************************************************************************************************************************************
    private getQueryBuilder() : SelectQueryBuilder<Conversation> {
        return this.convTypeOrmRepo.createQueryBuilder(conversEntityMetaData.tableName);
    }
//**************************************************************************************************************************************
}