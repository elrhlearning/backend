import { Conversation } from '../../../database/entity/messenger/conversation/conversation.entity';
import { IConversationService } from './conversation-service.interface';
import { User } from '../../../database/entity/user/user.entity';
import { IConversationRepository } from '../../repositorys/conversation/conversation-repository.interface';
import { Inject } from '@nestjs/common';
import { ConversationRepositoryToken, MessageServiceToken } from '../../config';
import { AppLogger } from '../../../common/loggers/app-logger';
import { IMessageService } from '../message/message-service.interface';
import { UserServiceToken } from '../../../user/config';
import { IUserService } from '../../../user/services/user.service.interface';
import { conversEntityMetaData } from '../../../database/entity/messenger/conversation/conversation.metadata';
import { EntityRelation } from '../../../common/interfaces/entity-relation.interface';
import { OrderBy, Where } from '../../../review/services/query-helper.interface';
import { messageEntityMetaData } from '../../../database/entity/messenger/message/message.metadata';
import { userEntityMetaData } from '../../../database/entity/user/user.metadata';
import { Message } from '../../../database/entity/messenger/message/message.entity';
export class ConversationService implements IConversationService {
    constructor(
        @Inject(ConversationRepositoryToken)
        private readonly convRepository : IConversationRepository,
        @Inject(MessageServiceToken)
        private readonly msgService : IMessageService,
        @Inject(UserServiceToken)
        private readonly userService : IUserService,
        private logger : AppLogger
    ){

    }
//**************************************************************************************************************************************
    async getAllConversations(user: User): Promise<Conversation[]> { 
        let msgs : Message[];
        let conv : Conversation[] = [];
        msgs = await this.msgService.getLatestMessages(user);
        for(let message of msgs){
            let conver = await this.getWithUsers(message.conversation);
            message.conversation = undefined;
            conver.messages = [message];
            conv.push(conver);
        }
        return conv;
    } 
//**************************************************************************************************************************************       
    async getConversation(conversation: Conversation): Promise<Conversation> {
        let where : Where = {
            where : `${conversEntityMetaData.columnsAlias.id} = :id`,
            params : {id : conversation.id}
            };
        // relatiion
        let userRelation : EntityRelation = {
            relation  : conversEntityMetaData.relations.users,
            alias : userEntityMetaData.tableName
        };         
        // get conversations
        let r : Conversation[];
        try {
            r =  await this.convRepository.getConversations([where],[userRelation],undefined,20,0);   
        } catch (error) {
            this.logger.getLogger().error(`getConversation() - error  : ${this.logger.logObject(error)} `);
        }
        this.logger.getLogger().info(`getConversation() - returning  : ${r.length} conversations `);
        let conv : Conversation = r[0];
        conv.messages = await this.msgService.getConversationMessages(conv,20,0);
        this.logger.getLogger().info(`getConversation() - returning Conversation  : ${conv.id} with ${conv.messages.length} messages`);
        return conv;
    }
//**************************************************************************************************************************************    
    async getMinimal(conversation: Conversation): Promise<Conversation> {
        let where : Where = {
            where : `${conversEntityMetaData.columnsAlias.id} = :id`,
            params : {id : conversation.id}
        };
        // get conversations
        let r : Conversation[];
        try {
            r =  await this.convRepository.getConversations([where]);   
        } catch (error) {
            this.logger.getLogger().error(`getMinimal() - error  : ${this.logger.logObject(error)} `);
        }
        if(!r || r.length == 0){
            this.logger.getLogger().warn(`getMinimal() - conversation not found  : ${this.logger.logObject(conversation)} `);
        }
        return r[0];
    }
//**************************************************************************************************************************************    
    async getWithUsers(conversation: Conversation): Promise<Conversation> {
        let where : Where = {
            where : `${conversEntityMetaData.columnsAlias.id} = :id`,
            params : {id : conversation.id}
        };
        // users Relation
        let usersRelation : EntityRelation = {
            relation : conversEntityMetaData.relations.users,
            alias : 'users'
        }
        // get conversations
        let r : Conversation[];
        try {
            r =  await this.convRepository.getConversations([where],[usersRelation]);   
        } catch (error) {
            this.logger.getLogger().error(`getWithUsers() - error  : ${this.logger.logObject(error)} `);
        }
        if(!r || r.length == 0){
            this.logger.getLogger().warn(`getWithUsers() - conversation not found  : ${this.logger.logObject(conversation)} `);
        }
        return r[0];
    }    
//**************************************************************************************************************************************    
    async save(conversation: Conversation): Promise<Conversation> {
        let conv : Conversation;
        try {
            conv = await this.convRepository.saveConversation(conversation);
        } catch (error) {
            this.logger.getLogger().error(`save() - error  : ${this.logger.logObject(error)} `);
        }
        this.logger.getLogger().error(`getMinimal() - conversation saved  : ${this.logger.logObject(conv)} `);
        return conv;
    }
}