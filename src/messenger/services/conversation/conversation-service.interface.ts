import { Conversation } from '../../../database/entity/messenger/conversation/conversation.entity';
import { User } from '../../../database/entity/user/user.entity';
export interface IConversationService {
    getAllConversations(user : User) : Promise<Conversation[]>;
    getConversation(conversation : Conversation) : Promise<Conversation>;
    getMinimal(conversation : Conversation) : Promise<Conversation>;
    getWithUsers(conversation : Conversation) : Promise<Conversation>;
    save(conversation : Conversation) : Promise<Conversation>;
}