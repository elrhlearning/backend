import { IMessageService } from './message-service.interface';
import { Message } from '../../../database/entity/messenger/message/message.entity';
import { Conversation } from '../../../database/entity/messenger/conversation/conversation.entity';
import { IMessageRepository } from '../../repositorys/message/message-repository.interface';
import { Inject, BadRequestException, Injectable } from '@nestjs/common';
import { MessageRepositoryToken } from '../../config';
import { AppLogger } from '../../../common/loggers/app-logger';
import { User } from '../../../database/entity/user/user.entity';

@Injectable()
export class MessageService implements IMessageService{

//**************************************************************************************************************************************
    constructor(
        @Inject(MessageRepositoryToken)
        private readonly msgRepository : IMessageRepository,
        private logger : AppLogger
    ){

    }   
//**************************************************************************************************************************************    
    async getConversationMessages(conversation: Conversation, take?: number, skip?: number): Promise<Message[]> {
        let msgs : Message[] = [];
        if(!skip)
            skip = 0;
        if(!take)
            take = 30;
        try {
            return this.conversationMessages(conversation,skip,take);
        } catch (error) {
            this.logger.getLogger().error(`getConversationMessages() - error : ${this.logger.logObject(error)}`);
        }
        this.logger.getLogger().info(`getConversationMessages() - returning : ${msgs.length} messages`);
        return msgs;
    }

//**************************************************************************************************************************************
    async save(message: Message): Promise<Message> {
        let msg : Message;
        try {
            msg = await this.msgRepository.saveMessage(message);
        } catch (error) {
            this.logger.getLogger().error(`save() - error : ${this.logger.logObject(error)}`);
        }
        this.logger.getLogger().info(`save() - new message saved : ${this.logger.logObject(msg)}`);
        return msg;
    }
//**************************************************************************************************************************************
    private async conversationMessages(conversation : Conversation,skip : number,take : number) : Promise<Message[]> {
        let msgs : Message[];
        try {
            msgs = await this.msgRepository.getConversationMessages(conversation,take,skip);
            this.logger.getLogger().info(`conversationMessages() - returning conversatin mesasges, nb : ${msgs.length}`); 
            return msgs;
        } catch (error) {
            this.logger.getLogger().error(`conversationMessages() - error : ${this.logger.logObject(error)}`);
        }
    }
//**************************************************************************************************************************************    
    async getLatestMessages(user: User): Promise<Message[]> {
        let msgs : Message[];
        try {
            msgs = await this.msgRepository.getLatestMessages(user);
            this.logger.getLogger().info(`getLatestMessages() - returning latest msgs for user : ${user.id} - messages : ${this.logger.logObject(msgs)}`);
            return msgs;
        } catch (error) {
            this.logger.getLogger().error(`getLatestMessages() - error : ${this.logger.logObject(error)}`);
        }
    }
//**************************************************************************************************************************************    
}