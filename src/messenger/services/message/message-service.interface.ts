import { Conversation } from '../../../database/entity/messenger/conversation/conversation.entity';
import { Message } from '../../../database/entity/messenger/message/message.entity';
import { User } from '../../../database/entity/user/user.entity';
export interface IMessageService {
    getConversationMessages(conversation :Conversation,take? : number,skip? : number) : Promise<Message[]>;
    save(message : Message) : Promise<Message>;
    getLatestMessages(user : User) : Promise<Message[]>;
}