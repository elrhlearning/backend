import { Controller, Inject, Get, Param,Query, Post,Body, InternalServerErrorException } from "@nestjs/common";
import { IMessageService } from "../services/message/message-service.interface";
import { MessageServiceToken, ConversationServiceToken, NbMessagesPeerRequest } from '../config';
import { IConversationService } from "../services/conversation/conversation-service.interface";
import { User } from '../../database/entity/user/user.entity';
import { Conversation } from '../../database/entity/messenger/conversation/conversation.entity';
import { Message } from "../../database/entity/messenger/message/message.entity";
import { NewMessageNotification } from "../../database/entity/notifications/new-message-notification.entity";
import { NotificationBuilderMananger } from "../../common/helpers/builder.manager";
import { NewMessageNotificationBuilder } from "../../common/helpers/new-message-notification.builder";
import { INotificationService } from "../../notification/services/notification/notification-service.interface";
import { NOTIFICATION_SSERVICE_TOKEN } from "../../notification/config";
import { MessengerGateway } from '../gateways/messenger.gateway';

@Controller('messenger')
export class MessengerController {
//***********************************************************************************************
  constructor(
    @Inject(MessageServiceToken)
    private readonly messageService : IMessageService,
    @Inject(ConversationServiceToken)
    private readonly conversationService : IConversationService,
    @Inject(NOTIFICATION_SSERVICE_TOKEN)
    private readonly notifService : INotificationService,
    private readonly notifManager : NotificationBuilderMananger,
    private readonly messengerGateway : MessengerGateway
  )
  {

  }
//***********************************************************************************************
  /**
   * Return  conversations of user
   * @param id 
   */
  @Get('convs/:id')
  getUserConversations(@Param('id') id : number) {
    let user : User = new User();
    user.id = id;
    return this.conversationService.getAllConversations(user);
  }
//***********************************************************************************************
  /**
   * Return conversation messages
   * @param id 
   * @param skip 
   */
  @Get('messages/:idConversation')
  loadMessages(@Param('idConversation') id : number, @Query('skip') skip : number) {
    let conv = new Conversation();
    conv.id = id;
    return this.messageService.getConversationMessages(conv,NbMessagesPeerRequest,skip);
  }
//***********************************************************************************************
  @Post('convs/start')
  async startConversation(@Body() conversation : Conversation) : Promise<any> {
    conversation = await  this.conversationService.save(conversation);
    if(!conversation.id)
      throw new InternalServerErrorException();

    return {
      idMessage : conversation.messages[0].id,
      idConversation : conversation.id
    }
  }
//***********************************************************************************************
  @Post('messages')
  async addMessage(@Body() message : Message) {
    let msg = await this.messageService.save(message);
    if(msg && msg.id){
      // foreach user we will send the message if it is connected otherwase we send 
      // notification 
      this.conversationService.getWithUsers(message.conversation)
      .then((conversation : Conversation)=> {
        for(let user of conversation.users){
          // if user is not the sender and message not sent then send notification
          if(user.id != message.sender.id && !this.messengerGateway.sendNewMessage(msg,user)){
            // send notification
            this.notifService.handleNewMessageNotification(this.createNewMessageNotification(msg,user));
          }
        }
      });
    }
    return msg.id;
  }
//***********************************************************************************************  
  private createNewMessageNotification(message : Message,notified  : User) : NewMessageNotification {
        return  <NewMessageNotification>
                ((<NewMessageNotificationBuilder>this.notifManager.getBuilder(NewMessageNotificationBuilder))
                .start()
                .setMessage(message)
                .setState(true)
                .setUser(notified)
                .build());
  }
//***********************************************************************************************  
}