import { Module, forwardRef } from '@nestjs/common';
import { UserModule } from '../user/user.module';
import { messengerRepositoryProviders } from './providers/repositorys.provider';
import { messengerServiceProviders } from './providers/services.provider';
import { MessengerController } from './controllers/messenger.controller';
import { NotificationModule } from '../notification/notification.module';
import { MessengerGateway } from './gateways/messenger.gateway';

@Module({
  controllers : [MessengerController],
  imports : [UserModule,forwardRef(() => NotificationModule)],
  providers : [...messengerRepositoryProviders,...messengerServiceProviders,MessengerGateway],
  exports : [...messengerRepositoryProviders,...messengerServiceProviders,MessengerGateway]
})
export class MessengerModule {}
