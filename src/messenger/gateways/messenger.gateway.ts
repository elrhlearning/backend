import { SubscribeMessage, WebSocketGateway } from '@nestjs/websockets';
import { User } from '../../database/entity/user/user.entity';
import { AppLogger } from '../../common/loggers/app-logger';
import { AbstractGateways } from '../../common/gateway/abstract.gateways';
import { Message } from '../../database/entity/messenger/message/message.entity';

@WebSocketGateway({namespace : 'messenger'})
export class MessengerGateway extends AbstractGateways{
//***********************************************************************************************
  constructor(private readonly logger : AppLogger){
    super();
  }
//***********************************************************************************************
  handleConnection(client: any, ...args: any[]) {
    this.logger.getLogger().info(`Client number ${this.clients.length + 1} connected to messenger, id : ${client.id}`);
    super.handleConnection(client,args);
  }
//***********************************************************************************************
  handleDisconnect(client: any) {
    this.logger.getLogger().info(`client ${client.id} Disconnected from messenger!`);
    super.handleDisconnect(client);
  }
//***********************************************************************************************
  /**
   * create 
   * @param subscribtion 
   */
  sendNewMessage(message : Message,user : User){
      let client : any = this.getClientSocket(user)
      if(client){
        this.logger.getLogger().info(`sending new message to user : ${user.id}`);
        client.emit('newMessage',{message : message});
        return true;
      }
      return false;
  }
//***********************************************************************************************    
}
