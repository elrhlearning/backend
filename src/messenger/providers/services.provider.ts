import { MessageServiceToken, ConversationServiceToken } from '../config';
import { MessageService } from '../services/message/message.service';
import { ConversationService } from '../services/conversation/conversation.service';
export const messengerServiceProviders = [
    MessageService,
    {
        provide : MessageServiceToken,
        useClass : MessageService
    },
    ConversationService,
    {
        provide : ConversationServiceToken,
        useClass : ConversationService
    },
]