import { MessageRepository } from '../repositorys/message/message.repository';
import { MessageRepositoryToken, ConversationRepositoryToken } from '../config';
import { ConversationRepository } from '../repositorys/conversation/conversation.repository';
export const messengerRepositoryProviders = [
    MessageRepository,
    {
        provide : MessageRepositoryToken,
        useClass : MessageRepository
    },
    ConversationRepository,
    {
        provide : ConversationRepositoryToken,
        useClass : ConversationRepository
    },
]