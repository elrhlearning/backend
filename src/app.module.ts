import { ReviewModule } from './review/review.module';
import { UserController } from './user/controllers/user.controller';
import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { UserModule } from './user/user.module';
import { DatabaseModule } from './database/database.module';
import { AuthentificationModule } from './authentification/authentification.module';
import { CommonModule } from './common/common.module';
import { NotificationModule } from './notification/notification.module';
import { MessengerModule } from './messenger/messenger.module';
import * as passport from 'passport';


@Module({
  imports: [UserModule, DatabaseModule, AuthentificationModule, CommonModule,ReviewModule, NotificationModule, MessengerModule],
  controllers: [AppController],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): MiddlewareConsumer | void {
/*     consumer
    .apply(passport.authenticate('jwt', { session: false }))
    .forRoutes(UserController); */
  }
}
