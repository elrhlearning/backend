import { BadRequestException, InternalServerErrorException } from '@nestjs/common';
import { IReviewService } from './../../../review/services/review.service.interface';
import { ReviewServiceToken } from './../../../review/config';
import { IUserService } from 'src/user/services/user.service.interface';
import { UserService } from './../../../user/services/user.service';
import { UserServiceToken } from './../../../user/config';
import { SUBSCRIBTION_REPOSITORY_TOKEN } from './../../config';
import { ISubscribtionRepository } from './../../repositorys/subscribtion/subscibtion-repository.interface';
import { Subscribtion } from './../../../database/entity/subscribtion/subscribtion.entity';
import { Review } from './../../../database/entity/review/review.entity';
import { User } from './../../../database/entity/user/user.entity';
import { ISubscribtionService } from './subscribtion-service.interface';
import { Injectable,Inject} from '@nestjs/common';
import { AppLogger } from '../../../common/loggers/app-logger';

@Injectable()
export class SubscribtionService implements ISubscribtionService {

//***********************************************************************************************************    
    constructor(
        @Inject(SUBSCRIBTION_REPOSITORY_TOKEN) private readonly subscribRepo : ISubscribtionRepository,
        @Inject(UserServiceToken) private readonly userService : IUserService,
        @Inject(ReviewServiceToken) private readonly reviewService : IReviewService,
        private readonly logger : AppLogger
    )
    {
    }
//***********************************************************************************************************    
    async startSubscribtion(review: Review, user: User): Promise<Subscribtion> {
        this.logger.init(__filename);
        // check user and review
        user = await this.userService.getUser(user);
        review = await this.reviewService.getReviewById(review);
        if(user.id == review.user.id){
            throw new BadRequestException('Owner of review cant subscribe to his review');
        }
        // remove same data
        review.items = undefined;
        review.comments = undefined;
        review.image = undefined;
        review.subscribtions = undefined;
        let subscrib : Subscribtion = new Subscribtion();
        subscrib.review = review;
        subscrib.subscribed = user;
        // save susbcription
        try {
          subscrib = await this.subscribRepo.save(subscrib);
        } catch (error) {
          this.logger.getLogger().error(`startSubscribtion() - ${this.logger.logObject(error)}`);
          throw new InternalServerErrorException();
        }
        // return result
        this.logger.getLogger().info(`startSubscribtion() - Saved correctly :  ${this.logger.logObject(subscrib)}`);
        return subscrib;
    }   
//***********************************************************************************************************    
    async stopSubscribtion(subscrib: Subscribtion): Promise<boolean> {
      this.logger.init(__filename);
      let r : boolean;
      // call repo
      try {
        r =  await this.subscribRepo.delete(subscrib);
      } catch (error) {
        this.logger.getLogger().error(
          `stopSubscribtion() - error:  ${this.logger.logObject(error)}`
        );
        throw new InternalServerErrorException();
      }
      // subscription not found
      if(!r){
        this.logger.getLogger().warn(
          `stopSubscribtion() - subscribtion not found:  ${this.logger.logObject(subscrib)}`
        );
        throw new BadRequestException(`stopSubscribtion() - subscribtion not found:  ${subscrib.id}`);
      }
      // return result
      this.logger.getLogger().info(
        `stopSubscribtion() - subscription stoped:  ${this.logger.logObject(subscrib)}`
      );
      return r;
    }
//***********************************************************************************************************
    async getUserSubscribtions(user: User): Promise<Subscribtion[]> {
      this.logger.init(__filename);
      user = await this.userService.getUser(user);
      let sbs : Subscribtion[];
      try {
        sbs = await this.subscribRepo.getUserSubscribtions(user);
      } catch (error) {
        this.logger.getLogger().error(
          `getUserSubscribtions() - error:  ${this.logger.logObject(error)}`
        );
        throw new InternalServerErrorException();
      }
      return sbs;
    }
//***********************************************************************************************************    
    async getReviewSubscribtions(review: Review): Promise<Subscribtion[]> {
        this.logger.init(__filename);
        // get review
        let r = await this.reviewService.getMinimizedReview(review);
        let sbs : Subscribtion[];
        // call repo
        try {
          sbs = await this.subscribRepo.getReviewSubscribtions(r);
        } catch (error) {
          this.logger.getLogger().error(
            `getReviewSubscribtions() - error:  ${this.logger.logObject(error)}`
          );
          throw new InternalServerErrorException();
        }
        // return result
        this.logger.getLogger().info(
          `getReviewSubscribtions() - return subscription of review ${review.id} :  ${this.logger.logObject(sbs)}`
        );
        return sbs;
    }
//***********************************************************************************************************    
}
