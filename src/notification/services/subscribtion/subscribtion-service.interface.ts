import { Subscribtion } from './../../../database/entity/subscribtion/subscribtion.entity';
import { User } from './../../../database/entity/user/user.entity';
import { Review } from './../../../database/entity/review/review.entity';
export interface ISubscribtionService {
    startSubscribtion(review : Review,user : User) : Promise<Subscribtion>;
    stopSubscribtion(subscrib : Subscribtion) : Promise<boolean>;
    getUserSubscribtions(user : User) : Promise<Subscribtion[]>;
    getReviewSubscribtions(review : Review) : Promise<Subscribtion[]>;
}
