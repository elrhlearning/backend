import { User } from './../../../database/entity/user/user.entity';
import { Notification } from './../../../database/entity/notifications/notification.abstract.entity';
import { ReviewNotification } from '../../../database/entity/notifications/review-notification.entity';
import { NewMessageNotification } from '../../../database/entity/notifications/new-message-notification.entity';

export interface INotificationService {
  saveNotification(notification : Notification) : Promise<Notification>;
  markAsRead(notification: Notification) : Promise<boolean>;
  getAllNotification(user : User) : Promise<Notification[]>;
  handleReviewNotification(notification : ReviewNotification);
  handleNewMessageNotification(notification : NewMessageNotification);
  deleteNotification(notification : Notification) : Promise<boolean>;
}