import { User } from './../../../database/entity/user/user.entity';
import { Notification } from 'src/database/entity/notifications/notification.abstract.entity';
import { INotificationRepository } from './../../repositorys/notification/notification-reposoitory.interface';
import { NOTIFICATION_REPOSITORY_TOKEN, NB_ACTIF_NOTIFICATION } from './../../config';
import { INotificationService } from './notification-service.interface';
import { Injectable, Inject, InternalServerErrorException } from '@nestjs/common';
import { NotificationBuilderMananger } from '../../../common/helpers/builder.manager';
import { SUBSCRIBTION_SSERVICE_TOKEN } from '../../config';
import { ISubscribtionService } from '../subscribtion/subscribtion-service.interface';
import { Subscribtion } from '../../../database/entity/subscribtion/subscribtion.entity';
import { ReviewNotificationGateway } from '../../gateways/review-notification.gateway';
import { AppLogger } from '../../../common/loggers/app-logger';
import { ReviewNotification } from '../../../database/entity/notifications/review-notification.entity';
import { NewMessageNotification } from '../../../database/entity/notifications/new-message-notification.entity';
import { ConversationServiceToken } from '../../../messenger/config';
import { IConversationService } from '../../../messenger/services/conversation/conversation-service.interface';
import { MessengerGateway } from '../../../messenger/gateways/messenger.gateway';








/**
 * service de notification
 */
@Injectable()
export class NotificationService implements INotificationService {
//************************************************************************************************************
  constructor(
              @Inject(NOTIFICATION_REPOSITORY_TOKEN)
              private readonly notifRepo : INotificationRepository,
              @Inject(SUBSCRIBTION_SSERVICE_TOKEN)
              private readonly subscService : ISubscribtionService,
              private reviewGateway : ReviewNotificationGateway,
              private readonly logger : AppLogger
  )
  {
  }
//************************************************************************************************************  
  async saveNotification(notification: Notification): Promise<Notification> {
    this.logger.init(__filename);
    let n : Notification;
    try {
      n =  await this.notifRepo.add(notification);
    } catch (error) {
      this.logger.getLogger().error(`saveNotification() - error :${this.logger.logObject(error)}`);
      throw new InternalServerErrorException();
    }
    this.logger.getLogger().info(`saveNotification() - saving notif :${this.logger.logObject(n)}`);
    return n;
  }
//************************************************************************************************************  
  async getAllNotification(user : User): Promise<Notification[]> {
    this.logger.init(__filename);
    let notif : Notification[];
    try {
      notif =  await this.notifRepo.getLastNbNotifications(NB_ACTIF_NOTIFICATION,user);
      this.logger.getLogger().info(`getAllNotification() - notification for  user : ${user.id}  :${this.logger.logObject(notif)}`);
      return notif;
    } catch (error) {
      this.logger.getLogger().error(`getAllNotification() - error :${this.logger.logObject(error)}`);
      throw new InternalServerErrorException();
    }
  }
//************************************************************************************************************  
  async markAsRead(notification: Notification): Promise<boolean> {
    this.logger.init(__filename);
    let n : Notification;
      if(!notification || !notification.state)
        return false;
      notification.state = false;
      try {
        n  = await this.notifRepo.update(notification);
      } catch (error) {
        this.logger.getLogger().error(`markAsRead() - error :${this.logger.logObject(error)}`);
        throw new InternalServerErrorException();
      }
      this.logger.getLogger().info(`markAsRead() - notification marked as read :${this.logger.logObject(n)}`);
      return !n.state;
  } 
//************************************************************************************************************  
  /** 
   * take notification of type ReviewNotification
   * get all subscriber to this review
   * and save update review notification
   * finnaly send notification via websocket
   * @param review review updated
   */
  handleReviewNotification(notification : ReviewNotification) {
    // get all subscriber
    let subsc : Promise<Subscribtion[]> =  this.subscService.getReviewSubscribtions(notification.review)
    subsc.then((data : Subscribtion[])=>{
        this.logger.init(__filename);
        this.logger.getLogger().info(
          `handleReviewNotification() - sending notification for ${data.length} subscribers to review : ${this.logger.logObject(notification.review)}`
          );
        data.forEach((sub) => {
            // create notif for each subscriber
            let subscriber = sub.subscribed;
            // create notif for each subscriber
            notification.notifiedUser = subscriber;
            // add notif
            this.saveNotification(notification)
            .then((notification) => {
              this.logger.getLogger().info(
                `handleReviewNotification() - notification saved in database : ${notification.id}`
                );
                // send notif via websocket
                this.reviewGateway.sendReviewNotification(<ReviewNotification>notification);
            });
        });
    });
  }
//************************************************************************************************************
  handleNewMessageNotification(notification: NewMessageNotification) {
      this.logger.init(__filename);
      this.saveNotification(notification)
      .then((notif : Notification)=> {
          this.logger.getLogger().info(`New message notification saved - notif : ${notif.id}`)
          // send notification via websocket
          this.reviewGateway.sendNewMessageNotification(<NewMessageNotification>notif);
      });
  }
//************************************************************************************************************  
  async deleteNotification(notification : Notification): Promise <boolean> {
    this.logger.init(__filename);
    try {
      let id = notification.id;
      notification = await this.notifRepo.delete(notification);
      this.logger.getLogger().info(`notif deleted, notif : ${id}`);
      return notification.id == undefined ? true : false; 
    } catch (error) {
      this.logger.getLogger().error(`deleteNotification (), error : ${this.logger.logObject(error)}`);
    }
  }
//************************************************************************************************************
}
