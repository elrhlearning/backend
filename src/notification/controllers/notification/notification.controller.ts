
import { BadRequestException, UseInterceptors } from '@nestjs/common';
import { UserServiceToken } from './../../../user/config';
import { User } from './../../../database/entity/user/user.entity';
import { INotificationService } from './../../services/notification/notification-service.interface';
import { NOTIFICATION_SSERVICE_TOKEN } from './../../config';
import { Controller, Inject, Get, Post, Body } from '@nestjs/common';
import { IUserService } from 'src/user/services/user.service.interface';
import { async } from 'rxjs/internal/scheduler/async';
import { RequestLoggingInterceptor } from '../../../common/interceptors/request-loggin.interceptor';
import { Notification } from '../../../database/entity/notifications/notification.abstract.entity';
import { SubscribeReviewNotification } from '../../../database/entity/notifications/sunscribe-notification.entity';

@Controller('notification')
@UseInterceptors(RequestLoggingInterceptor)
export class NotificationController {
//***********************************************************************************************************
    constructor(
        @Inject(NOTIFICATION_SSERVICE_TOKEN)
        private readonly notifServicec : INotificationService,
        @Inject(UserServiceToken)
        private userService : IUserService
    ){

    }
//***********************************************************************************************************    
    @Post()
    async getAll(@Body() user : User) : Promise<any> {
      user = await this.userService.getUser(user);
      if(!user)
        throw new BadRequestException(`Inknow user : ${user.id}`);
      return this.notifServicec.getAllNotification(user); 
    }
//***********************************************************************************************************
    @Post('read')
    async markNotificationsAsRead(@Body() notification : Notification) : Promise<any>{
      return this.notifServicec.markAsRead(notification);
    }
//***********************************************************************************************************
    @Post('delete')
    async deleteNotification(@Body() notification : Notification) : Promise<any> {
      return this.notifServicec.deleteNotification(notification);
    }
//***********************************************************************************************************
}
