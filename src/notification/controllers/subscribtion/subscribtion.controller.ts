
import { INotificationService } from './../../services/notification/notification-service.interface';
import { HttpStatus, BadRequestException, UseInterceptors } from '@nestjs/common';
import { Subscribtion } from './../../../database/entity/subscribtion/subscribtion.entity';
import { Review } from './../../../database/entity/review/review.entity';
import { User } from './../../../database/entity/user/user.entity';
import { SUBSCRIBTION_SSERVICE_TOKEN, NOTIFICATION_SSERVICE_TOKEN } from './../../config';
import { ISubscribtionService } from './../../services/subscribtion/subscribtion-service.interface';
import { Controller, Post, Inject,Param,Body,HttpCode} from '@nestjs/common';
import { NotificationBuilderMananger } from '../../../common/helpers/builder.manager';
import { SubscribeNotificationBuilder } from '../../../common/helpers/subscribe-notification.builder';
import { RequestLoggingInterceptor } from '../../../common/interceptors/request-loggin.interceptor';
import { ReviewNotification } from '../../../database/entity/notifications/review-notification.entity';
import { Notification } from '../../../database/entity/notifications/notification.abstract.entity';
import { ReviewNotificationGateway } from '../../gateways/review-notification.gateway';


@Controller('subscrib')
@UseInterceptors(RequestLoggingInterceptor)
export class SubscribtionController {
//***********************************************************************************************************
    constructor(@Inject(SUBSCRIBTION_SSERVICE_TOKEN) 
                private readonly subscribService : ISubscribtionService,
                private readonly reviewGateway : ReviewNotificationGateway,
                @Inject(NOTIFICATION_SSERVICE_TOKEN) 
                private readonly notificationService : INotificationService,
                private builderManager : NotificationBuilderMananger)
    {
    }
//***********************************************************************************************************    
    @Post('start/:userId/:reviewId')
    @HttpCode(HttpStatus.OK)
    async startSubscribtion(@Param('userId') userId : number,@Param('reviewId') reviewId : number) : Promise<any>{
        // create objects
        let user : User = new User();
        user.id = userId;
        let review : Review = new Review();
        review.id = reviewId;
        // create subscribtion 
        let subsc = await  this.subscribService.startSubscribtion(review,user);
        // create and save notification
        let builder = (<SubscribeNotificationBuilder>this.builderManager.getBuilder(SubscribeNotificationBuilder));
        let notif : Notification = builder.start().setSubscribtion(subsc).setState(true).build();
        // save and send Notification (asynchroun)
        this.notificationService.saveNotification(notif)
        .then((notif : Notification) => {
            this.reviewGateway.sendReviewNotification(<ReviewNotification>notif);
        })
        // return subscription
        return subsc;
    }
//***********************************************************************************************************
    @Post('stop')
    @HttpCode(HttpStatus.OK)
    stopSubscribtion( @Body() subscrib : Subscribtion) : Promise<any>{
        return this.subscribService.stopSubscribtion(subscrib);
    }
//***********************************************************************************************************    
    @Post('userSubscribtions')
    @HttpCode(HttpStatus.OK)
    getUserSubscribtions(@Body() user : User){
        if(!user || !user.id)
            throw new BadRequestException();
        return this.subscribService.getUserSubscribtions(user);
    }
//***********************************************************************************************************    
}