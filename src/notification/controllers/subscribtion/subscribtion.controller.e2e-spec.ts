import { HttpStatus } from '@nestjs/common';
import { Subscribtion } from './../../../database/entity/subscribtion/subscribtion.entity';
import { User } from './../../../database/entity/user/user.entity';
import { AppController } from './../../../app.controller';
import { NotificationModule } from './../../notification.module';
import { ReviewModule } from './../../../review/review.module';
import { CommonModule } from './../../../common/common.module';
import { AuthentificationModule } from './../../../authentification/authentification.module';
import { DatabaseModule } from './../../../database/database.module';
import { UserModule } from './../../../user/user.module';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';


describe('SubscribController (e2e)', () => {
  let app;
  let subscrib : Subscribtion;
//***********************************************************************************************************  
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [UserModule, DatabaseModule, AuthentificationModule, CommonModule,ReviewModule, NotificationModule],
      controllers: [AppController],
      providers: [],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });
//***********************************************************************************************************
  it('/api/v1/subscrib/start (POST)', () => {
    return request(app.getHttpServer())
      .post('/subscrib/start/23/64')
      .expect(HttpStatus.OK)
      .then(response => {
        expect(response.body.id).toBeDefined();
        subscrib = new Subscribtion();
        subscrib.id=response.body.id;
    });
  });
//***********************************************************************************************************
  it('/api/v1/subscrib/stop (POST)', () => {
    
    return request(app.getHttpServer())
      .post('/subscrib/stop')
      .send(JSON.stringify(subscrib))
      .expect(HttpStatus.OK)
      .then(response => {
        expect(response.body).toBeTruthy();
    });
  });
//***********************************************************************************************************  
});
