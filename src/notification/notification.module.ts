import { NotificationController } from './controllers/notification/notification.controller';
import { SubscribtionController } from './controllers/subscribtion/subscribtion.controller';
import { ReviewModule } from './../review/review.module';
import { UserModule } from './../user/user.module';
import { notificationProviders } from './providers/notification.provider';
import { Module, forwardRef } from '@nestjs/common';
import { ReviewNotificationGateway } from './gateways/review-notification.gateway';
import { MessengerModule } from '../messenger/messenger.module';

@Module({
  imports : [UserModule, forwardRef(() => ReviewModule),forwardRef(() => MessengerModule)],
  controllers : [SubscribtionController,NotificationController],
  providers : [...notificationProviders,ReviewNotificationGateway],
  exports : [...notificationProviders,ReviewNotificationGateway]
})
export class NotificationModule {
}
