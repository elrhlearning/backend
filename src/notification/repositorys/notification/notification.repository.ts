import { Where } from './../../../review/services/query-helper.interface';
import { OrWhereSetter } from './../../../database/services/or-where-setter';
import { User } from './../../../database/entity/user/user.entity';
import { NotificationLinker } from './../../../database/entity/notifications/notification-linker.entity';
import { SubscribeReviewNotification } from './../../../database/entity/notifications/sunscribe-notification.entity';
import { SubscrReviewNotificationTypeOrmRepositoryToken, NotificationLnkerTypeOrmRepositoryToken, NewMessageNotificationTypeOrmRepositoryToken } from './../../../database/config';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { UpdateReviewNotificationTypeOrmRepositoryToken, CommentedReviewNotificationTypeOrmRepositoryToken } from '../../../database/config';
import { Injectable,Inject } from '@nestjs/common';
import { INotificationRepository } from "./notification-reposoitory.interface";
import { Notification } from "../../../database/entity/notifications/notification.abstract.entity";
import { UpdateReviewNotification } from '../../../database/entity/notifications/update-review-notification.entity';
import { CommentReviewNotification } from '../../../database/entity/notifications/comment-review-notification.entity';
import { NewMessageNotification } from '../../../database/entity/notifications/new-message-notification.entity';

@Injectable()
export class NotificationRepository implements INotificationRepository{
//********************************************************************************************
  constructor(
    @Inject(UpdateReviewNotificationTypeOrmRepositoryToken) 
    private readonly updateNotifTypeORMrepository : Repository<UpdateReviewNotification>,
    @Inject(SubscrReviewNotificationTypeOrmRepositoryToken) 
    private readonly subscribNotifTypeORMrepository : Repository<SubscribeReviewNotification>,
    @Inject(CommentedReviewNotificationTypeOrmRepositoryToken) 
    private readonly commentedReviewNotifTypeORMrepository : Repository<SubscribeReviewNotification>,
    @Inject(NotificationLnkerTypeOrmRepositoryToken)
    private readonly notifLinkerTypeOrm : Repository<NotificationLinker>,
    @Inject(NewMessageNotificationTypeOrmRepositoryToken)
    private readonly newMessageNotifRepo : Repository<NewMessageNotification>,
    private readonly whereSetter : OrWhereSetter
  )
  {

  }
//********************************************************************************************
  async getLastNbNotifications(nb: number,user : User): Promise<Notification[]> {
    let all : NotificationLinker[] = [];
    let qr = 
        this.notifLinkerTypeOrm.createQueryBuilder('linker')
        // SubscribeReviewNotification
        .leftJoinAndSelect('linker.subscribeNotification','SubscribeReviewNotification')
        .leftJoinAndSelect('SubscribeReviewNotification.notifLinked','notifLinkedSubsc')
        .leftJoinAndSelect('SubscribeReviewNotification.subscribtion','Subscribtion')
        .leftJoinAndSelect('Subscribtion.review','SubscribReview')
        .leftJoinAndSelect('Subscribtion.subscribed','User')
        // update notif
        .leftJoinAndSelect('linker.updateReviewNotification','UpdateReviewNotification')
        .leftJoinAndSelect('UpdateReviewNotification.notifLinked','notifLinkedUpdate')
        .leftJoinAndSelect('UpdateReviewNotification.review','UpdatedReview')
        // comment notifs
        .leftJoinAndSelect('linker.commentReviewNotification','CommentReviewNotification')
        .leftJoinAndSelect('CommentReviewNotification.notifLinked','notifLinkedComment')
        .leftJoinAndSelect('CommentReviewNotification.review','CommentedReview')
        .leftJoinAndSelect('CommentReviewNotification.comment','Comment')
        // new message notifs
        .leftJoinAndSelect('linker.newMessageNotification','NewMsgNotification')
        .leftJoinAndSelect('NewMsgNotification.newMessage','NewMessage')
        .leftJoinAndSelect('NewMessage.sender','MsgSender')
        .leftJoinAndSelect('NewMessage.conversation','Conversation');

        // user condition
        qr = <SelectQueryBuilder<NotificationLinker>>this.whereSetter.setWheres(
          qr,
          [
            {where : 'SubscribeReviewNotification.notifiedUser.id = :userId',params : {userId : user.id}},
            {where : 'CommentReviewNotification.notifiedUser.id = :userId',params : {userId : user.id}},
            {where : 'UpdateReviewNotification.notifiedUser.id = :userId',params : {userId : user.id}},
            {where : 'NewMsgNotification.notifiedUser.id = :userId',params : {userId : user.id}}
          ]
        );
        // add here new type of notification
        qr.orderBy('SubscribeReviewNotification.dateCreation','DESC')
          .addOrderBy('CommentReviewNotification.dateCreation','DESC')
          .addOrderBy('UpdateReviewNotification.dateCreation','DESC')
          .addOrderBy('NewMsgNotification.dateCreation','DESC')
          .take(nb);
     // get result
     try {
      all = await qr.getMany(); 
     } catch (error) {
       console.log(error); 
     }
     let notifications : Notification[] = [];
     all.forEach((notif)=> {
        
        if(notif.subscribeNotification){
          notifications.push(notif.subscribeNotification);
        }
        else if (notif.commentReviewNotification) {
          notifications.push(notif.commentReviewNotification);
        }
        else if(notif.newMessageNotification){
          notifications.push(notif.newMessageNotification);
        }
     });
     return notifications;
  }  
//********************************************************************************************  
  async add(notification: Notification): Promise<Notification> {
      if(!notification.notifLinked)
          notification.notifLinked =  new NotificationLinker();
      let notif : Notification = await this.getRepository(notification).save(notification);
      return notif;
  }
//********************************************************************************************
  async delete(notification : Notification): Promise<Notification> {
      return this.getRepository(notification).remove(notification);
  }
//********************************************************************************************
  update(notification: Notification) : Promise<Notification>{
    let r = this.getRepository(notification);
    return r.save(notification);
  }
//********************************************************************************************
  private getRepository(notification? : any) : Repository<Notification>{
    if(notification instanceof SubscribeReviewNotification || notification.subscribtion)
      return this.subscribNotifTypeORMrepository;
    if(notification instanceof CommentReviewNotification || notification.comment)
      return this.commentedReviewNotifTypeORMrepository;
    if(notification instanceof UpdateReviewNotification || notification.dateModification)
      return this.updateNotifTypeORMrepository;
    if(notification instanceof NewMessageNotification || notification.newMessage)
      return this.newMessageNotifRepo;      
  }
//********************************************************************************************
}