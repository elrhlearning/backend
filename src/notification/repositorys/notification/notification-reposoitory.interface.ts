import { User } from './../../../database/entity/user/user.entity';
import { NotificationLinker } from './../../../database/entity/notifications/notification-linker.entity';
import { Notification } from './../../../database/entity/notifications/notification.abstract.entity';

export interface INotificationRepository {
  getLastNbNotifications(nb : number,user : User) : Promise<Notification[]>;
  add(notification : Notification) : Promise<Notification>;
  delete(notification : Notification) : Promise<Notification>;
  update(notification : Notification) : Promise<Notification>;
}