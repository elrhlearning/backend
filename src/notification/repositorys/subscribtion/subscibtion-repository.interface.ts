import { User } from './../../../database/entity/user/user.entity';
import { Subscribtion } from './../../../database/entity/subscribtion/subscribtion.entity';
import { Review } from '../../../database/entity/review/review.entity';
export interface ISubscribtionRepository {
    save(subscrib : Subscribtion) : Promise<Subscribtion>;    
    delete(subscrib : Subscribtion) : Promise<boolean>;
    getUserSubscribtions(user : User) : Promise<Subscribtion[]>;
    getReviewSubscribtions(review : Review) : Promise<Subscribtion[]>;
}