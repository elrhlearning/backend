import { Where } from './../../../review/services/query-helper.interface';
import { userEntityMetaData } from './../../../database/entity/user/user.metadata';
import { reviewEntityMetaData } from './../../../database/entity/review/review.metadata';
import { OrderBySetter } from './../../../database/services/order-by-setter';
import { AndWhereSetter } from './../../../database/services/and-where-setter';
import { OrWhereSetter } from './../../../database/services/or-where-setter';
import { subscribtionEntityMetaData } from './../../../database/entity/subscribtion/subscribtion.metadata';
import { User } from './../../../database/entity/user/user.entity';
import { SubscribtionTypeOrmRepositoryToken } from './../../../database/config';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { Subscribtion } from './../../../database/entity/subscribtion/subscribtion.entity';
import { SUBSCRIBTION_REPOSITORY_TOKEN } from './../../config';
import { Inject, Injectable } from '@nestjs/common';
import { ISubscribtionRepository } from './subscibtion-repository.interface';
import { Review } from '../../../database/entity/review/review.entity';

@Injectable()
export class SubscribtionRepository implements ISubscribtionRepository{
//*****************************************************************************************************
    constructor(
       @Inject(SubscribtionTypeOrmRepositoryToken) private readonly subscribTypeOrmRepository : Repository<Subscribtion>,
       private readonly orWheresSetter : OrWhereSetter,
       private readonly andWheresSetter : AndWhereSetter,
       private readonly orderBySetter :  OrderBySetter
    )
    {

    }
//*****************************************************************************************************    
    save(subscrib: Subscribtion): Promise<Subscribtion> {
        return this.subscribTypeOrmRepository.save(subscrib);
    }
//*****************************************************************************************************
    async delete(subscrib : Subscribtion) : Promise<boolean> {
        subscrib = await this.subscribTypeOrmRepository.remove(subscrib);
        return subscrib != null;
    }
//*****************************************************************************************************
    async getUserSubscribtions(user : User) : Promise<Subscribtion[]> {
        let queryBuilder =  this.getQueryBuilder()
        queryBuilder.select(
            [
            subscribtionEntityMetaData.columnsAlias.id,
            subscribtionEntityMetaData.columnsAlias.dateCreation,
            `${reviewEntityMetaData.columnsAlias.id}`
            ]
        )
        // relation to get in results
        .leftJoin(subscribtionEntityMetaData.relations.review,reviewEntityMetaData.tableName)
        .leftJoin(reviewEntityMetaData.relations.user,userEntityMetaData.tableName);
        //
        let userWhere: Where = {where : `${subscribtionEntityMetaData.relations.user}.${userEntityMetaData.columns.id.name} = :id`,params : { id : user.id}};
        queryBuilder = <SelectQueryBuilder<Subscribtion>>this.andWheresSetter.setWheres(queryBuilder,[userWhere]);
        return queryBuilder.getMany();
    }    
//**************************************************************************************************** 
    async getReviewSubscribtions(review : Review) : Promise<Subscribtion[]> {
        let queryBuilder =  this.getQueryBuilder()
        // relation to get in results
        .leftJoinAndSelect(subscribtionEntityMetaData.relations.review,reviewEntityMetaData.tableName)
        .leftJoinAndSelect(subscribtionEntityMetaData.relations.user,userEntityMetaData.tableName);
        //wheres
        let reviewWhere: Where = {where : `${reviewEntityMetaData.columnsAlias.id} = :id`,params : { id : review.id}};
        queryBuilder = <SelectQueryBuilder<Subscribtion>>this.andWheresSetter.setWheres(queryBuilder,[reviewWhere]);
        return queryBuilder.getMany();
    }    
    //**************************************************************************************************** 
    private  getQueryBuilder() : SelectQueryBuilder<Subscribtion> {
        return this.subscribTypeOrmRepository.createQueryBuilder(subscribtionEntityMetaData.tableName);
    }
//****************************************************************************************************    
}