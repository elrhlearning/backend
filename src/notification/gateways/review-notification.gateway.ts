import { AppLogger } from './../../common/loggers/app-logger';

import { User } from './../../database/entity/user/user.entity';
import { SubscribeMessage, WebSocketGateway,OnGatewayConnection, OnGatewayDisconnect} from '@nestjs/websockets';
import { ReviewNotification } from 'src/database/entity/notifications/review-notification.entity';
import { CommentReviewNotification } from '../../database/entity/notifications/comment-review-notification.entity';
import { NewMessageNotification } from '../../database/entity/notifications/new-message-notification.entity';
import { Message } from '../../database/entity/messenger/message/message.entity';
import { AbstractGateways } from '../../common/gateway/abstract.gateways';


@WebSocketGateway()
export class ReviewNotificationGateway extends AbstractGateways implements  OnGatewayConnection,OnGatewayDisconnect{
//***********************************************************************************************
  constructor(private readonly logger : AppLogger){
    super();
  }
//***********************************************************************************************
  handleConnection(client: any, ...args: any[]) {
    this.logger.getLogger().info(`Client number ${this.clients.length + 1} connected, id : ${client.id}`);
    super.handleConnection(client,args);
  }
//***********************************************************************************************
  /**
   * create 
   * @param subscribtion 
   */
  sendReviewNotification(notif : ReviewNotification){
      let client : any = this.getClientSocket(notif.notifiedUser);
      if(client){
        if(notif instanceof CommentReviewNotification){
          notif.comment.review = undefined;
        }
        this.logger.getLogger().info(`sending review notification to user  : ${notif.notifiedUser.id} - notification : ${this.logger.logObject(notif)}`);
        notif.notifiedUser = undefined;
        client.emit('hello',{notification : notif});
        return;
      }
  }
//***********************************************************************************************
  /**
   * create 
   * @param subscribtion 
   */
  sendNewMessageNotification(notif : NewMessageNotification){
    let client : any = this.getClientSocket(notif.notifiedUser)
    if(client){
      this.logger.getLogger().info(
          `sending new message notification to user  : ${notif.notifiedUser.id}
          - notification : ${this.logger.logObject(notif)}`
       );
      notif.notifiedUser = undefined;
      client.emit('hello',{notification : notif});
      return true;
    }
}  
//***********************************************************************************************
  handleDisconnect(client: any) {
    this.logger.getLogger().info(`client ${client.id} Disconnected from review gateway!`);
    super.handleDisconnect(client);
  }
//***********************************************************************************************  
}
