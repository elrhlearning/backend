import { Test, TestingModule } from '@nestjs/testing';
import { ReviewNotificationGateway } from './review-notification.gateway';

describe('ReviewNotificationGateway', () => {
  let gateway: ReviewNotificationGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReviewNotificationGateway],
    }).compile();

    gateway = module.get<ReviewNotificationGateway>(ReviewNotificationGateway);
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });
});
