import { Subscribtion } from './../../../database/entity/subscribtion/subscribtion.entity';
import { ReviewRepositoryToken, ReviewServiceToken } from './../../../review/config';
import { ItemReview } from './../../../database/entity/item-review/item-review.entity';
import { Review } from './../../../database/entity/review/review.entity';
import { ISubscribtionService } from './../../services/subscribtion/subscribtion-service.interface';
import { SubscribeReviewNotification } from './../../../database/entity/notifications/sunscribe-notification.entity';
import { User } from './../../../database/entity/user/user.entity';
import { UserRepositoryToken } from './../../../user/config';
import { NOTIFICATION_SSERVICE_TOKEN, SUBSCRIBTION_SSERVICE_TOKEN } from './../../config';
import { INotificationService } from './../../services/notification/notification-service.interface';
import { notificationProviders } from './../../providers/notification.provider';
import { UserModule } from './../../../user/user.module';
import { ReviewModule } from './../../../review/review.module';
import { CommonModule } from './../../../common/common.module';
import { DatabaseModule } from './../../../database/database.module';
import { Test, TestingModule } from '@nestjs/testing';
import { IUserRepository } from 'src/user/repositorys/user.repository.interface';
import { IReviewCrudRepository } from 'src/review/repositorys/review/review-crud.interface.repository';
import { NotificationBuilderMananger } from '../../../common/helpers/builder.manager';
import { SubscribeNotificationBuilder } from '../../../common/helpers/subscribe-notification.builder';
import { IReviewService } from '../../../review/services/review.service.interface';
import { CommentedReviewNotificationBuilder } from '../../../common/helpers/commentted-review-notification.builder';



describe('NotificationService', () => {
//*************************************************************************************  
  let notifService: INotificationService;
  let userRepo : IUserRepository;
  let user : User;
  let users : User[];
  let notification : SubscribeReviewNotification;
  let subscService : ISubscribtionService;
  let subscription : Subscribtion;
  let reviewService : IReviewService;
  let review : Review;
  let manager : NotificationBuilderMananger;
//*************************************************************************************  
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
        imports : [UserModule,DatabaseModule,CommonModule,ReviewModule],
        providers : notificationProviders
    }).compile();
    notifService = module.get<INotificationService>(NOTIFICATION_SSERVICE_TOKEN);
    userRepo = module.get<IUserRepository>(UserRepositoryToken);
    reviewService = module.get<IReviewService>(ReviewServiceToken);
    subscService = module.get<ISubscribtionService>(SUBSCRIBTION_SSERVICE_TOKEN);
    manager = module.get<NotificationBuilderMananger>(NotificationBuilderMananger);
  });
//*************************************************************************************
  it('should be defined', () => {
    expect(notifService).toBeDefined();
    expect(userRepo).toBeDefined();
    expect(reviewService).toBeDefined();
    expect(subscService).toBeDefined();
    expect(manager).toBeDefined();
  });
//*************************************************************************************
  it('get user to create notification', async () => {
    users = await userRepo.find();
    expect(users.length).toBeGreaterThan(1);
    user = users[0];
  });
//*************************************************************************************
  it('create Review', async () => {
    review = new Review();
    review.link='test link' ;
    review.title='test title';
    review.description = 'description test';
    review.user = user;
    review = await reviewService.addReview(review);
    expect(review.id).toBeDefined();
  });
//*************************************************************************************  
it('create Subscription', async () => {
  subscription = await subscService.startSubscribtion(review,users[1]);
  expect(subscription).toBeDefined();
  expect(subscription.id).toBeDefined();
});
//*************************************************************************************  
it('create Notification', async () => {
  let builder = (<SubscribeNotificationBuilder>manager.getBuilder(SubscribeNotificationBuilder));
  notification = <SubscribeReviewNotification>builder.start().setSubscribtion(subscription).setState(true).build();
  notification = <SubscribeReviewNotification>await notifService.saveNotification(notification);
  expect(notification).toBeDefined();
  expect(notification.id).toBeDefined();
});
//*************************************************************************************  
it('mark Notification as READ', async () => {
  let r = await notifService.markAsRead(notification);
  expect(r).toBeDefined();
  expect(r).toBeTruthy();
});
//*************************************************************************************  
it('delete review Review', async () => {
  expect((await reviewService.deleteReview(review))).toBeTruthy();
});
//*************************************************************************************
});
