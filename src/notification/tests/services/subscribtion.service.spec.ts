import { ISubscribtionService } from './../../services/subscribtion/subscribtion-service.interface';
import { Subscribtion } from './../../../database/entity/subscribtion/subscribtion.entity';
import { ISubscribtionRepository } from './../../repositorys/subscribtion/subscibtion-repository.interface';
import { NOTIFICATION_REPOSITORY_TOKEN, SUBSCRIBTION_REPOSITORY_TOKEN, SUBSCRIBTION_SSERVICE_TOKEN } from './../../config';
import { IUserService } from '../../../user/services/user.service.interface';
import { UserServiceToken } from '../../../user/config';
import { Review } from '../../../database/entity/review/review.entity';
import { UpdateReviewNotification } from '../../../database/entity/notifications/update-review-notification.entity';
import { ReviewServiceToken } from '../../../review/config';
import { IReviewService } from '../../../review/services/review.service.interface';
import { User } from '../../../database/entity/user/user.entity';
import { ReviewModule } from '../../../review/review.module';
import { CommonModule } from '../../../common/common.module';
import { DatabaseModule } from '../../../database/database.module';
import { UserModule } from '../../../user/user.module';
import { TestingModule,Test } from "@nestjs/testing";
import { notificationProviders } from "../../providers/notification.provider";


//*****************************************************************************************************
describe('SubscribtionService tests', () => {
  let subscribService : ISubscribtionService;
  let user : User;
  let owner : User;
  let review : Review;
  let subscrib : Subscribtion;
  let reviewService : IReviewService;
  let userService : IUserService;
//*************************************************************************************  
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
        imports : [UserModule,DatabaseModule,CommonModule,ReviewModule],
        providers : notificationProviders
    }).compile();
    subscribService = module.get<ISubscribtionService>(SUBSCRIBTION_SSERVICE_TOKEN);
    reviewService = module.get<IReviewService>(ReviewServiceToken);
    userService = module.get<IUserService>(UserServiceToken);
  });
//*************************************************************************************
  it('dependency injection tests', () => {
    expect(subscribService).toBeDefined();
    expect(reviewService).toBeDefined();
    expect(userService).toBeDefined();
  });
//*************************************************************************************  
  it('should get user associed to review', async ()  => {
    user = (await userService.findAll())[0];
    owner = (await userService.findAll())[1];
    expect(user).toBeDefined();
    expect(owner).toBeDefined();
  });
//*************************************************************************************
  it('should add review', async ()  => {
    review = new Review();
    review.link='test link' ;
    review.title='test title';
    review.description = 'description test';
    review.user = user;
    review = await reviewService.addReview(review);
  }); 
//*************************************************************************************
  it('start subscribtion', async ()  => {
    subscrib = undefined;
    subscrib = await subscribService.startSubscribtion(review,owner);
    expect(subscrib).toBeDefined();
    expect(subscrib.id).toBeDefined();
  });  
 
//************************************************************************************* 
  it('stop subscribtion', async ()  => {
    let r : boolean = await subscribService.stopSubscribtion(subscrib);
    expect(r).toBeTruthy();
  }); 
//*************************************************************************************
  it('should delete review', async ()  => {
    let r : boolean = await reviewService.deleteReview(review);
    expect(r).toBeTruthy();
  }); 
  //*************************************************************************************
})