import { Subscribtion } from './../../../database/entity/subscribtion/subscribtion.entity';
import { ISubscribtionRepository } from './../../repositorys/subscribtion/subscibtion-repository.interface';
import { NOTIFICATION_REPOSITORY_TOKEN, SUBSCRIBTION_REPOSITORY_TOKEN } from './../../config';
import { IUserService } from '../../../user/services/user.service.interface';
import { UserServiceToken } from '../../../user/config';
import { Review } from '../../../database/entity/review/review.entity';
import { UpdateReviewNotification } from '../../../database/entity/notifications/update-review-notification.entity';
import { ReviewServiceToken } from '../../../review/config';
import { IReviewService } from '../../../review/services/review.service.interface';
import { User } from '../../../database/entity/user/user.entity';
import { ReviewModule } from '../../../review/review.module';
import { CommonModule } from '../../../common/common.module';
import { DatabaseModule } from '../../../database/database.module';
import { UserModule } from '../../../user/user.module';
import { TestingModule,Test } from "@nestjs/testing";
import { notificationProviders } from "../../providers/notification.provider";
import { INotificationRepository } from '../../repositorys/notification/notification-reposoitory.interface';
import { Notification } from '../../../database/entity/notifications/notification.abstract.entity';



describe('SubscrptionRepository tests', () => {
  let subscribRepository : ISubscribtionRepository;
  let user : User;
  let review : Review;
  let subscrib : Subscribtion;
  let reviewService : IReviewService;
  let userService : IUserService;
//*************************************************************************************  
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
        imports : [UserModule,DatabaseModule,CommonModule,ReviewModule],
        providers : notificationProviders
    }).compile();
    subscribRepository = module.get<ISubscribtionRepository>(SUBSCRIBTION_REPOSITORY_TOKEN);
    reviewService = module.get<IReviewService>(ReviewServiceToken);
    userService = module.get<IUserService>(UserServiceToken);
  });
//*************************************************************************************
  it('dependency injection tests', () => {
    expect(subscribRepository).toBeDefined();
    expect(reviewService).toBeDefined();
    expect(userService).toBeDefined();
  });
//*************************************************************************************
  it('add subscribtion', async ()  => {
    subscrib = new Subscribtion();
    let r = new Review();
    r.id = 64;
    review = await reviewService.getMinimizedReview(r);
    expect(review).toBeDefined();
    user = new User();
    user.id = 23;
    user = await userService.getUser(user);
    expect(user).toBeDefined();
    subscrib.review = review;
    subscrib.subscribed = user;
    subscrib = await subscribRepository.save(subscrib);
    expect(subscrib).toBeDefined();
    expect(subscrib.id).toBeDefined();
  });  
//************************************************************************************* 
  it('get user subscrbtions', async ()=>{
    let subscribs : Subscribtion[] = await subscribRepository.getUserSubscribtions(user);
    expect(subscribs).toBeDefined();
    expect(subscribs.length).toBeGreaterThan(0)
  });  
//************************************************************************************* 
  it('delete subscribtion', async ()  => {
    console.log(`Subscribtion  ${subscrib.id} is deleted`);
    let r : boolean = await subscribRepository.delete(subscrib);
    expect(r).toBeTruthy();
  }); 
//*************************************************************************************
})