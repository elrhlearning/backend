import { NotificationLinker } from './../../../database/entity/notifications/notification-linker.entity';
import { Subscribtion } from './../../../database/entity/subscribtion/subscribtion.entity';
import { ISubscribtionService } from './../../services/subscribtion/subscribtion-service.interface';
import { SubscribeReviewNotification } from './../../../database/entity/notifications/sunscribe-notification.entity';
import { NOTIFICATION_REPOSITORY_TOKEN, SUBSCRIBTION_SSERVICE_TOKEN } from './../../config';
import { IUserService } from '../../../user/services/user.service.interface';
import { UserServiceToken } from '../../../user/config';
import { Review } from '../../../database/entity/review/review.entity';
import { ReviewServiceToken } from '../../../review/config';
import { IReviewService } from '../../../review/services/review.service.interface';
import { User } from '../../../database/entity/user/user.entity';
import { ReviewModule } from '../../../review/review.module';
import { CommonModule } from '../../../common/common.module';
import { DatabaseModule } from '../../../database/database.module';
import { UserModule } from '../../../user/user.module';
import { TestingModule,Test } from "@nestjs/testing";
import { notificationProviders } from "../../providers/notification.provider";
import { INotificationRepository } from '../../repositorys/notification/notification-reposoitory.interface';
import { Notification } from '../../../database/entity/notifications/notification.abstract.entity';



describe('NotificationRepository tests', () => {
  let notificationRepository : INotificationRepository;
  let notification : Notification;
  let user : User;
  let reviewService : IReviewService;
  let userService : IUserService;
  let subscService : ISubscribtionService;
//*************************************************************************************  
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
        imports : [UserModule,DatabaseModule,CommonModule,ReviewModule,UserModule],
        providers : notificationProviders
    }).compile();
    notificationRepository = module.get<INotificationRepository>(NOTIFICATION_REPOSITORY_TOKEN);
    reviewService = module.get<IReviewService>(ReviewServiceToken);
    userService = module.get<IUserService>(UserServiceToken);
    subscService = module.get<ISubscribtionService>(SUBSCRIBTION_SSERVICE_TOKEN);
  });
//*************************************************************************************
  it('dependency injection tests', () => {
    expect(notificationRepository).toBeDefined();
    expect(reviewService).toBeDefined();
    expect(userService).toBeDefined();
    expect(subscService).toBeDefined();
  });
//*************************************************************************************
  it('add notification', async ()  => {
    notification = new SubscribeReviewNotification();
    user = new User();
    user.id = 23;
    user = await userService.getUser(user);
    expect(user).toBeDefined();
    let sunbsc : Subscribtion[] = await subscService.getUserSubscribtions(user);
    expect(sunbsc).toBeDefined();
    expect(sunbsc.length).toBeGreaterThan(0);
    (<SubscribeReviewNotification>notification).subscribtion = sunbsc[0];
    (<SubscribeReviewNotification>notification).notifiedUser = user;
    notification = await notificationRepository.add(notification);
    expect(notification).toBeDefined();
    expect(notification.id).toBeDefined();
  });  
//*************************************************************************************  
  it('mark notification as read', async ()  => {
    console.log(`Updating notification number : ${notification.id}`);
    notification.state = false;
    notification = await notificationRepository.update(notification);
    expect(notification.state).toBeFalsy();
  });
//*************************************************************************************
  it('get All', async ()  => {
    expect((await notificationRepository.getLastNbNotifications(20,user)).length).toBeGreaterThan(0);
  });  
//*************************************************************************************  
  it('delete notification', async ()  => {
    console.log(`Deleting notification number : ${notification.id}`);
    let r : boolean = await notificationRepository.delete(notification);
    expect(r).toBeTruthy();
  });
//*************************************************************************************  
});