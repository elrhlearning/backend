import { CommonModule } from '../../../common/common.module';
import { Test, TestingModule } from '@nestjs/testing';
import { AuthentificationController } from '../../../authentification/controllers/authentification/authentification.controller';
import { forwardRef } from '@nestjs/common';
import { UserModule } from '../../../user/user.module';
import { authProviders, authProvidersToExport } from '../../../authentification/providers/auth-provider';
import { DatabaseModule } from '../../../database/database.module';
import { User } from '../../../database/entity/user/user.entity';
import { IUserService } from '../../../user/services/user.service.interface';
import { UserService } from '../../../user/services/user.service';

describe('Authentification Controller Tests', () => {
  let controller: AuthentificationController;
  let userService : IUserService;
  let user : User;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        forwardRef(() => UserModule),DatabaseModule,CommonModule
      ],
      providers: [...authProviders],
      controllers: [AuthentificationController],
      exports : [...authProvidersToExport]
    }).compile();

    controller = module.get<AuthentificationController>(AuthentificationController);
    userService = module.get<IUserService>(UserService);
  });
// ********************************************************************************************  
  // test if dependecy injection is correct
  it('controller should be defined', () => {
    expect(controller).toBeDefined();
    expect(userService).toBeDefined();
  });
// ********************************************************************************************
  it('add user, get token, and delete the user', async () => {
      user = new User();
      user.name = "test name";
      user.lastname = "test lastname";
      user.email = "name.lastname@gmail.com";
      user.password = "elrhourha";
      let id  = await userService.addUser(user);
      expect(id).toBeGreaterThan(0);
      let newUser : any = await controller.createToken(user);
      expect(newUser).toBeDefined();
      expect(newUser.token.accessToken).toBeDefined();
      expect(await userService.deleteUser(id)).toBeDefined();
  });
// ********************************************************************************************  
});
