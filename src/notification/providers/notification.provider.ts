import { NotificationService } from './../services/notification/notification.service';
import { SubscribtionService } from './../services/subscribtion/subscribtion.service';
import { SubscribtionRepository } from './../repositorys/subscribtion/subscribtion.repository';
import { NotificationRepository } from './../repositorys/notification/notification.repository';
import { NOTIFICATION_REPOSITORY_TOKEN, SUBSCRIBTION_REPOSITORY_TOKEN, SUBSCRIBTION_SSERVICE_TOKEN, NOTIFICATION_SSERVICE_TOKEN } from './../config';




export const notificationProviders = [
    NotificationRepository,
    SubscribtionRepository,
    SubscribtionService,
    NotificationService,
    {
        provide : NOTIFICATION_REPOSITORY_TOKEN,
        useClass : NotificationRepository         
    },
    {
        provide : SUBSCRIBTION_REPOSITORY_TOKEN,
        useClass : SubscribtionRepository
    },
    {
        provide : SUBSCRIBTION_SSERVICE_TOKEN,
        useClass : SubscribtionService
    },
    {
      provide : NOTIFICATION_SSERVICE_TOKEN,
      useClass : NotificationService
  },
];