import { ColumnType } from "typeorm";

export const DbConnectionToken : string = "DbConnectionToken";
export const UserTypeOrmRepositoryToken : string = "UTRT";
export const ImageTypeOrmRepositoryToken : string = "PITRT";
export const ReviewTypeOrmRepositoryToken : string = "RTRT";
export const ItemReviewTypeOrmRepositoryToken : string = "IRTRT";
export const UpdateReviewNotificationTypeOrmRepositoryToken : string ="URNTRT";
export const SubscrReviewNotificationTypeOrmRepositoryToken : string ="SRNTRT";
export const CommentedReviewNotificationTypeOrmRepositoryToken : string ="CommRNTRT";
export const NotificationLnkerTypeOrmRepositoryToken : string ="NLTRT";
export const SubscribtionTypeOrmRepositoryToken : string = "STRT";
export const CommentTypeOrmRepositoryToken : string = "COMMTRT";
export const AllUsers : string = 'UCT';
export const UsersWithEmail : string = 'UWECT';
// MESSEGNER TOKENS
export const MessageRepositoryTypeORMToken : string = 'MsgRepoTOToken';
export const ConversationRepositoryTypeORMToken : string = 'ConvRepoTOToken';
// MESSENGER NOTIF
export const NewMessageNotificationTypeOrmRepositoryToken : string ="NewMsgTRT";



export const CacheTokens  = {
    AllUsers :  'UCT',
    UsersWithEmail : 'UWECT'
};

export interface DbType {
  stringType? : ColumnType,
  numberType? : ColumnType,
  intType? : ColumnType,
  boolType? : ColumnType,
  dateType? : ColumnType,
}

/**
 * SQLite config
 */
const sqliteCongif : DbType = {
  stringType : 'text',
  dateType : 'text',
  boolType : 'boolean'
}



/**
 * used config, to change create DbType implemetation and assign it here
 */
export const DB_TYPE_CONFIG : DbType =  sqliteCongif;