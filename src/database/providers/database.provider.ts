import { Comment } from './../entity/comments/comment.entity';
import { NotificationLinker } from './../entity/notifications/notification-linker.entity';
import { SubscribeReviewNotification } from './../entity/notifications/sunscribe-notification.entity';
import { Subscribtion } from './../entity/subscribtion/subscribtion.entity';
import { OrWhereSetter } from './../services/or-where-setter';
import { AndWhereSetter } from './../services/and-where-setter';
import { createConnection, Connection } from 'typeorm';
import {    DbConnectionToken, UserTypeOrmRepositoryToken, ImageTypeOrmRepositoryToken, 
            ReviewTypeOrmRepositoryToken, ItemReviewTypeOrmRepositoryToken, 
            SubscrReviewNotificationTypeOrmRepositoryToken,
            NotificationLnkerTypeOrmRepositoryToken,
            CommentTypeOrmRepositoryToken,
            UpdateReviewNotificationTypeOrmRepositoryToken, SubscribtionTypeOrmRepositoryToken, NewMessageNotificationTypeOrmRepositoryToken } from './../config';
import { User } from '../entity/user/user.entity';
import { Image } from '../entity/image/image.entity';
import { Review } from '../entity/review/review.entity';
import { ItemReview } from '../entity/item-review/item-review.entity';
import { OrderBySetter } from '../services/order-by-setter';
import { UpdateReviewNotification } from '../entity/notifications/update-review-notification.entity';
import { CommentedReviewNotificationTypeOrmRepositoryToken, MessageRepositoryTypeORMToken, ConversationRepositoryTypeORMToken } from '../config';
import { CommentReviewNotification } from '../entity/notifications/comment-review-notification.entity';
import { Message } from '../entity/messenger/message/message.entity';
import { Conversation } from '../entity/messenger/conversation/conversation.entity';
import { NewMessageNotification } from '../entity/notifications/new-message-notification.entity';

/**
 * const that will help to provide all typeorm Repository with tokens
 */
export const databaseProviders = [
    {
        provide: DbConnectionToken,
        useFactory: async () => await createConnection(),
    },
    {
        provide: UserTypeOrmRepositoryToken,
        useFactory: (connection: Connection) => connection.getRepository(User),
        inject: [DbConnectionToken],
    },
    {
        provide: ImageTypeOrmRepositoryToken,
        useFactory: (connection: Connection) => connection.getRepository(Image),
        inject: [DbConnectionToken],
    },
    {
        provide: ReviewTypeOrmRepositoryToken,
        useFactory: (connection: Connection) => connection.getRepository(Review),
        inject: [DbConnectionToken],
    },  
    {
        provide: ItemReviewTypeOrmRepositoryToken,
        useFactory: (connection: Connection) => connection.getRepository(ItemReview),
        inject: [DbConnectionToken],
    },    
    {
      provide: UpdateReviewNotificationTypeOrmRepositoryToken,
      useFactory: (connection: Connection) => connection.getRepository(UpdateReviewNotification),
      inject: [DbConnectionToken],
    }, 
    {
        provide: SubscrReviewNotificationTypeOrmRepositoryToken,
        useFactory: (connection: Connection) => connection.getRepository(SubscribeReviewNotification),
        inject: [DbConnectionToken],
    },   
    {
        provide: CommentedReviewNotificationTypeOrmRepositoryToken,
        useFactory: (connection: Connection) => connection.getRepository(CommentReviewNotification),
        inject: [DbConnectionToken],
    }, 
    {
      provide: CommentTypeOrmRepositoryToken,
      useFactory: (connection: Connection) => connection.getRepository(Comment),
      inject: [DbConnectionToken]
    },  
    {
        provide: NotificationLnkerTypeOrmRepositoryToken,
        useFactory: (connection: Connection) => connection.getRepository(NotificationLinker),
        inject: [DbConnectionToken],
    },   
    {
        provide: SubscribtionTypeOrmRepositoryToken,
        useFactory: (connection: Connection) => connection.getRepository(Subscribtion),
        inject: [DbConnectionToken],
    }, 
    {
        provide: MessageRepositoryTypeORMToken,
        useFactory: (connection: Connection) => connection.getRepository(Message),
        inject: [DbConnectionToken],
    }, 
    {
        provide: ConversationRepositoryTypeORMToken,
        useFactory: (connection: Connection) => connection.getRepository(Conversation),
        inject: [DbConnectionToken],
    }, 
    {
      provide: NewMessageNotificationTypeOrmRepositoryToken,
      useFactory: (connection: Connection) => connection.getRepository(NewMessageNotification),
      inject: [DbConnectionToken],
    }, 
    OrderBySetter,
    AndWhereSetter,
    OrWhereSetter
];