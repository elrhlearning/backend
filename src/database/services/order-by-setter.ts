import { OrderBy } from './../../review/services/query-helper.interface';
import { Entity, SelectQueryBuilder } from 'typeorm';
import {  } from '../../review/repositorys/review/review-crud.interface.repository';
import { Injectable } from '@nestjs/common';

@Injectable()
export class OrderBySetter {
    
    setOrdersBy(queryBuilder : SelectQueryBuilder<any>,orderBys : OrderBy[]) : SelectQueryBuilder<any>  {
        if(orderBys != null && orderBys.length > 0)
        orderBys.forEach((orderBy : OrderBy, index) => {
            if(index == 0)
                queryBuilder = queryBuilder.orderBy(orderBy.column,orderBy.order);
            else
                queryBuilder = queryBuilder.addOrderBy(orderBy.column,orderBy.order);
        });
        return queryBuilder;
    }

}