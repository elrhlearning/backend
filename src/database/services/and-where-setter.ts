import { Where } from './../../review/services/query-helper.interface';
import { SelectQueryBuilder,WhereExpression } from 'typeorm';
import { Injectable } from '@nestjs/common';

/**
 * @author KARIM EL RHOURHA
 * @description classs used to apply list of where condition (with AND operator) to a query builder OR WhereExpression
 */
@Injectable()
export class AndWhereSetter {
    
    setWheres(queryBuilder : SelectQueryBuilder<any> | WhereExpression ,wheres : Where[]) : SelectQueryBuilder<any> | WhereExpression  {
        if(wheres != null && wheres.length > 0)
            wheres.forEach((where : Where, index) => {
                if(index == 0)
                    queryBuilder = queryBuilder.where(where.where,where.params);
                else
                    queryBuilder = queryBuilder.andWhere(where.where,where.params);
            });
        return queryBuilder;
    }

}