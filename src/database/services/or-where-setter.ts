import { Where } from './../../review/services/query-helper.interface';
import { Entity, SelectQueryBuilder , WhereExpression} from 'typeorm';
import { Injectable } from '@nestjs/common';

/**
 * @author KARIM EL RHOURHA
 * @description classs used to apply list of where condition (with OR operator) to a query builder OR WhereExpression
 */
@Injectable()
export class OrWhereSetter {
    
    setWheres(queryBuilder : SelectQueryBuilder<any> | WhereExpression,wheres : Where[]) : SelectQueryBuilder<any> | WhereExpression {
        if(wheres != null && wheres.length > 0)
            wheres.forEach((where : Where, index) => {
                if(index == 0)
                    queryBuilder = queryBuilder.where(where.where,where.params);
                else
                    queryBuilder = queryBuilder.orWhere(where.where,where.params);
            });
        return queryBuilder;
    }

}