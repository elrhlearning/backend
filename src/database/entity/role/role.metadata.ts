import { ColumnOptions } from 'typeorm';
import { PrimaryGeneratedColumnNumericOptions } from "typeorm/decorator/options/PrimaryGeneratedColumnNumericOptions";
import { DB_TYPE_CONFIG } from '../../config';

/**
 * 
 */
const roleId: PrimaryGeneratedColumnNumericOptions = { name: 'id' };
const roleLabel: ColumnOptions = { type: DB_TYPE_CONFIG.stringType, name: 'label', length: 200, nullable: false };
export const roleEntityMetaData  = {
    tableName : 'role',
    columns : {
        idColumn : roleId,
        labelColumn : roleLabel
    },
    relations : {
        user  : 'users'
    }
}