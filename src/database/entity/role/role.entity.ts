import { Transform } from 'class-transformer';
import { User } from '../user/user.entity';
import { Entity, Column, ManyToMany, PrimaryGeneratedColumn, ColumnOptions } from "typeorm";
import { roleEntityMetaData } from './role.metadata';
import { AppEntity } from '../app-entity.abstract';
/**
 * 
 */
@Entity({name : roleEntityMetaData.tableName})
export class Role extends AppEntity {
    @PrimaryGeneratedColumn(roleEntityMetaData.columns.idColumn)
    @Transform(value => parseInt(value, 10), { toClassOnly: true })
    id : number;

    @Column(roleEntityMetaData.columns.labelColumn)
    label : string;

    @ManyToMany(type => User, user => user.roles)
    users : User[];
}