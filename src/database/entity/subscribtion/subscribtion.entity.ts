import { SubscribeReviewNotification } from './../notifications/sunscribe-notification.entity';
import { OneToOne, Unique, Index } from 'typeorm';
import { User } from './../user/user.entity';
import { Review } from './../review/review.entity';
import { ManyToOne, Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { subscribtionEntityMetaData } from './subscribtion.metadata';
import { AppEntity } from '../app-entity.abstract';

@Entity({name : subscribtionEntityMetaData.tableName})
@Index(["subscribed", "review"], { unique: true })
export class Subscribtion  extends AppEntity{
@PrimaryGeneratedColumn(subscribtionEntityMetaData.columns.id)
id : number;

@Column(subscribtionEntityMetaData.columns.dateCreation)
dateCreation : number;

@ManyToOne(type => Review, review => review.subscribtions,{ onDelete: 'CASCADE', onUpdate : 'CASCADE',eager : true})
review : Review;

@ManyToOne(type => User,{eager : true,onDelete : 'CASCADE',onUpdate : 'CASCADE'})
subscribed : User;

@OneToOne(type => SubscribeReviewNotification,notification => notification.subscribtion,{onDelete : 'CASCADE'})
notification : SubscribeReviewNotification;
}