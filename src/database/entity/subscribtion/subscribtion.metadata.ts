import { ColumnOptions } from 'typeorm';
import { PrimaryGeneratedColumnNumericOptions } from 'typeorm/decorator/options/PrimaryGeneratedColumnNumericOptions';
import { DB_TYPE_CONFIG } from '../../config';

//****************************************************************************************************************
/**
 * Column options that will be used in the app the to avoid using column name hard coded
 */

const id: PrimaryGeneratedColumnNumericOptions = { name: 'id' };
const dateCreation : ColumnOptions = { type: DB_TYPE_CONFIG.dateType, default : () => 'CURRENT_TIMESTAMP' , name: 'date_creation', nullable: false };
const tableName : string = "subscribtion";
//****************************************************************************************************************
/**
 * metadata object of the entity
 */
export const subscribtionEntityMetaData = {
  tableName : tableName,
  columns : {
      id : id,
      dateCreation : dateCreation
  },
  columnsAlias : {
    id :`${tableName}.${id.name}`,
    dateCreation : `${tableName}.${dateCreation.name}`
  },
  relations : {
    review  : `${tableName}.review`,
    user : `${tableName}.subscribed`
}
}
//****************************************************************************************************************