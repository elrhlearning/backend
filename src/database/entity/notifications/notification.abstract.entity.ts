import { JoinColumn } from 'typeorm';
import { NotificationLinker } from './notification-linker.entity';
import { OneToOne } from 'typeorm';
import { User } from './../user/user.entity';
import { PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { notifAbstactEntityMetaData } from './notification.abstract.metadata';
import { AppEntity } from '../app-entity.abstract';
export abstract class Notification extends AppEntity{

  @PrimaryGeneratedColumn(notifAbstactEntityMetaData.columns.id)
  id : number;

  @Column(notifAbstactEntityMetaData.columns.dateCreation)
  dateCreation : string;

  @Column(notifAbstactEntityMetaData.columns.state)
  state : boolean = false;

  @ManyToOne(type => User)
  notifiedUser : User;

  notifLinked : NotificationLinker;
  
}