import { Entity, OneToOne, JoinColumn, ManyToOne } from 'typeorm';
import { Review } from '../review/review.entity';
import { type } from 'os';
import { Notification } from './notification.abstract.entity';
import { Subscribtion } from '../subscribtion/subscribtion.entity';

export abstract class ReviewNotification extends Notification{

  @ManyToOne(type => Review,{eager : true,onDelete : 'CASCADE',onUpdate : 'CASCADE'})
  @JoinColumn()
  review : Review;

  @OneToOne(type => Subscribtion,
    subscribtion=>subscribtion.notification,
    {onDelete : 'CASCADE',onUpdate : 'CASCADE'})
  @JoinColumn()
  subscribtion : Subscribtion;
}