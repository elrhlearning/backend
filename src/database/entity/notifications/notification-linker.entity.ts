import { SubscribeReviewNotification } from './sunscribe-notification.entity';
import { OneToOne, JoinColumn } from 'typeorm';
import { Entity, PrimaryGeneratedColumn } from 'typeorm';
import { AppEntity } from '../app-entity.abstract';
import { CommentReviewNotification } from './comment-review-notification.entity';
import { UpdateReviewNotification } from './update-review-notification.entity';
import { NewMessageNotification } from './new-message-notification.entity';

@Entity()
export class NotificationLinker extends AppEntity{
    @PrimaryGeneratedColumn()
    id : number;


    @OneToOne(
        type => SubscribeReviewNotification,
        subscribeNotification => subscribeNotification.notifLinked,
        {onDelete : 'CASCADE',onUpdate : 'CASCADE'}
    )
    subscribeNotification : SubscribeReviewNotification;
 
    @OneToOne(  
        type => CommentReviewNotification,
        commentReviewNotification => commentReviewNotification.notifLinked
        ,{onDelete : 'CASCADE',onUpdate : 'CASCADE'}
    )
    commentReviewNotification : CommentReviewNotification; 


    @OneToOne(  
        type => UpdateReviewNotification,
        updateReviewNotification => updateReviewNotification.notifLinked
        ,{onDelete : 'CASCADE',onUpdate : 'CASCADE'}
    )
    updateReviewNotification : UpdateReviewNotification; 

    @OneToOne(  
        type => NewMessageNotification,
        newMessageNotification => newMessageNotification.notifLinked
        ,{onDelete : 'CASCADE',onUpdate : 'CASCADE'}
    )
    newMessageNotification : NewMessageNotification; 
}