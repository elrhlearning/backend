import { Subscribtion } from './../subscribtion/subscribtion.entity';
import { NotificationLinker } from "./notification-linker.entity";
import { Entity, OneToOne, JoinColumn } from "typeorm";
import { ReviewNotification } from './review-notification.entity';

@Entity({name : 'notification_subscribe_to_review'})
export class SubscribeReviewNotification extends ReviewNotification{

  constructor(){
    super();
  }

  @OneToOne(type => NotificationLinker,
  notifLinked =>  notifLinked.subscribeNotification,
  {cascade : true,onDelete : 'CASCADE',nullable : false})
  @JoinColumn()
  notifLinked : NotificationLinker;
}