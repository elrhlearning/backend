import { Entity, OneToOne, JoinColumn, Column, ManyToOne } from 'typeorm';
import { UpdateReviewNotification } from './update-review-notification.entity';
import { Comment } from '../comments/comment.entity';
import { NotificationLinker } from './notification-linker.entity';

@Entity({name : 'notification_new_comment'})
export  class CommentReviewNotification extends UpdateReviewNotification{

    @ManyToOne(type => Comment,{cascade : false, onDelete : 'CASCADE'})
    comment : Comment;


    @OneToOne(type => NotificationLinker,
        notifLinked =>  notifLinked.commentReviewNotification,
        {cascade : ['insert'],onDelete : 'CASCADE',nullable : false})
    @JoinColumn()
    notifLinked : NotificationLinker;
}