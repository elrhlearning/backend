import { Message } from "../messenger/message/message.entity";
import { ManyToOne, Entity, OneToOne, JoinColumn } from "typeorm";
import { Notification } from "./notification.abstract.entity";
import { NotificationLinker } from './notification-linker.entity';

@Entity({name : 'notification_new_message'})
export class NewMessageNotification extends Notification {

  @OneToOne(type => Message,{onDelete : 'CASCADE'})
  @JoinColumn()
  newMessage : Message;

  @OneToOne(type => NotificationLinker,
    notifLinked =>  notifLinked.newMessageNotification,
    {cascade : true,onDelete : 'CASCADE',nullable : false})
  @JoinColumn()
  notifLinked : NotificationLinker;
}