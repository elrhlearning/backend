import { ColumnOptions } from 'typeorm';
import { PrimaryGeneratedColumnNumericOptions } from "typeorm/decorator/options/PrimaryGeneratedColumnNumericOptions";
import { DB_TYPE_CONFIG } from '../../config';

/**
 * 
 */
const id : PrimaryGeneratedColumnNumericOptions = { name: 'id' };
const dateCreation : ColumnOptions = { type: DB_TYPE_CONFIG.dateType, default : () => 'CURRENT_TIMESTAMP' , name: 'date_creation', nullable: false };
const state : ColumnOptions = {type : DB_TYPE_CONFIG.boolType,default : true, name : 'state'}
const tableName : string = 'notification';
export const notifAbstactEntityMetaData  = {
    tableName : tableName,
    columns : {
        id : id,
        dateCreation : dateCreation,
        state : state
    },
}
