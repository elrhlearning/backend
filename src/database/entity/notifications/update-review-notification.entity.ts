import { Entity, OneToOne, JoinColumn, Column } from 'typeorm';
import { ReviewNotification } from './review-notification.entity';
import { NotificationLinker } from './notification-linker.entity';

@Entity({name : 'notification_update_review'})
export  class UpdateReviewNotification extends ReviewNotification{

    @Column({ type: 'text', default : () => 'CURRENT_TIMESTAMP' , name: 'date_modification', nullable: false })
    dateModification : string;


    @OneToOne(type => NotificationLinker,
        notifLinked =>  notifLinked.updateReviewNotification,
        {cascade : true,onDelete : 'CASCADE',nullable : false})
    @JoinColumn()
    notifLinked : NotificationLinker;
}