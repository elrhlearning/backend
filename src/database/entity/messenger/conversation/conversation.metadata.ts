import { ColumnOptions } from 'typeorm';
import { PrimaryGeneratedColumnNumericOptions } from "typeorm/decorator/options/PrimaryGeneratedColumnNumericOptions";
import { DB_TYPE_CONFIG } from '../../../config';


/**
 * 
 */
const id : PrimaryGeneratedColumnNumericOptions = { name: 'id' };
const dateCreation : ColumnOptions = { type: DB_TYPE_CONFIG.dateType, default : () => 'CURRENT_TIMESTAMP' , name: 'date_creation', nullable: false };
const tableName : string = 'conversation';
export const conversEntityMetaData  = {
    tableName : tableName,
    columns : {
        id : id,
        dateCreation : dateCreation,
    },
    columnsAlias : {
        id : `${tableName}.${id.name}`,
        dateCreation : `${tableName}.dateCreation`,
    },
    relations : {
        messages  : `${tableName}.messages`,
        users : `${tableName}.users`
    },
}
