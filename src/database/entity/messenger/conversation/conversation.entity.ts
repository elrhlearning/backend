import { AppEntity } from '../../app-entity.abstract';
import { PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, ManyToMany, Entity, JoinTable } from 'typeorm';
import { User } from '../../user/user.entity';
import { conversEntityMetaData } from './conversation.metadata';
import { Message } from '../message/message.entity';

@Entity(conversEntityMetaData.tableName)
export class Conversation extends AppEntity {
    @PrimaryGeneratedColumn(conversEntityMetaData.columns.id)
    id : number;
  
    @Column(conversEntityMetaData.columns.dateCreation)
    dateCreation : string;

    @OneToMany(type => Message, message => message.conversation,{cascade : ['insert']})
    messages : Message[];

    @ManyToMany(type => User)
    @JoinTable({name : 'users_conversations'})
    users : User[];
  
}