import { ColumnOptions } from 'typeorm';
import { PrimaryGeneratedColumnNumericOptions } from "typeorm/decorator/options/PrimaryGeneratedColumnNumericOptions";
import { DB_TYPE_CONFIG } from '../../../config';


/**
 * 
 */
const id : PrimaryGeneratedColumnNumericOptions = { name: 'id' };
const dateCreation : ColumnOptions = { type: DB_TYPE_CONFIG.dateType, default : () => 'CURRENT_TIMESTAMP' , name: 'date_creation', nullable: false };
const read : ColumnOptions = {type : DB_TYPE_CONFIG.boolType,default : true, name : 'read'}
const content : ColumnOptions = {type : DB_TYPE_CONFIG.stringType,name : 'content' , nullable : false,length : 4000}
const tableName : string = 'message';
export const messageEntityMetaData  = {
    tableName : tableName,
    columns : {
        id : id,
        dateCreation : dateCreation,
        read : read,
        content : content,
    },
    relations : {
        sender : `${tableName}.sender`,
        conversation : `${tableName}.conversation`
    },
    columnsAlias : {
        id : `${tableName}.id`,
        dateCreation : `${tableName}.dateCreation`,
        read : `${tableName}.read`,
        content : `${tableName}.content`,
    },
}
