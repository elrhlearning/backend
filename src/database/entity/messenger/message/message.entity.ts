import { AppEntity } from '../../app-entity.abstract';
import { PrimaryGeneratedColumn, Column, ManyToOne, Entity } from 'typeorm';
import { User } from '../../user/user.entity';
import { messageEntityMetaData } from './message.metadata';
import { Conversation } from '../conversation/conversation.entity';

@Entity(messageEntityMetaData.tableName)
export class Message extends AppEntity {
    @PrimaryGeneratedColumn(messageEntityMetaData.columns.id)
    id : number;
  
    @Column(messageEntityMetaData.columns.dateCreation)
    dateCreation : string;

    @Column(messageEntityMetaData.columns.content)
    content : string;
  
    @Column(messageEntityMetaData.columns.read)
    read : boolean = false;
  
    @ManyToOne(type => User,{cascade : false, onDelete  : 'CASCADE'})
    sender : User;

    @ManyToOne(type => Conversation,{cascade : ['update'], onDelete  : 'CASCADE'})
    conversation : Conversation;
}