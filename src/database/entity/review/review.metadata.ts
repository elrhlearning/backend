import { ColumnOptions } from 'typeorm';
import { PrimaryGeneratedColumnNumericOptions } from "typeorm/decorator/options/PrimaryGeneratedColumnNumericOptions";
import { DB_TYPE_CONFIG } from '../../config';

/**
 * 
 */
const id : PrimaryGeneratedColumnNumericOptions = { name: 'id' };
const link : ColumnOptions = { type: DB_TYPE_CONFIG.stringType, name: 'link', length: 500, nullable: false };
const title : ColumnOptions = { type: DB_TYPE_CONFIG.stringType, name: 'title', length: 500, nullable: false };
const description : ColumnOptions = { type: DB_TYPE_CONFIG.stringType, name: 'description', length: 1000, nullable: true };
const dateCreation : ColumnOptions = { type: DB_TYPE_CONFIG.dateType, default : () => 'CURRENT_TIMESTAMP' , name: 'date_creation', nullable: false };
const tableName : string = 'review';
export const reviewEntityMetaData  = {
    tableName : tableName,
    columns : {
        id : id,
        link  : link,
        title : title,
        description : description,
        dateCreation : dateCreation
    },
    columnsAlias : {
        id : tableName +"."+ id.name,
        link  : tableName +"."+ link.name,
        title : tableName  +"."+title.name,
        description : tableName  +"."+ description.name,
    },
    relations : {
        comments  : tableName +"."+'comments',
        items : tableName +"."+'items',
        image : tableName +"."+'image',
        user : tableName +"."+'user'
    },
    validationGroups : {
        add : 'add',
        update : 'update',
        delete : 'delete',
        find : 'find'
    }
}
