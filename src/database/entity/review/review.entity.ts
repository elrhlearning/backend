import { Comment } from './../comments/comment.entity';
import { Subscribtion } from './../subscribtion/subscribtion.entity';
import { Delete } from '@nestjs/common';
import { User } from '../user/user.entity';
import { MaxLength, IsPositive, IsInt, IsNumber, IsDefined} from 'class-validator';
import { MinLength } from 'class-validator';
import { reviewEntityMetaData } from './review.metadata';
import { Image } from '../image/image.entity';
import { ItemReview } from '../item-review/item-review.entity';
import { OneToMany, ManyToOne,PrimaryGeneratedColumn, OneToOne, JoinColumn, Column, Entity } from 'typeorm';
import { AppEntity } from '../app-entity.abstract';


@Entity(reviewEntityMetaData.tableName)
export class Review extends AppEntity{
//**********************************************************************************************************
    @IsDefined({
        groups : [
            reviewEntityMetaData.validationGroups.delete,
            reviewEntityMetaData.validationGroups.update,
            reviewEntityMetaData.validationGroups.find,
        ]
    })
    @PrimaryGeneratedColumn(reviewEntityMetaData.columns.id)
    id : number;
//**********************************************************************************************************
    @MinLength(20,{
        groups : [
            reviewEntityMetaData.validationGroups.add,
            reviewEntityMetaData.validationGroups.update,
        ]  
    })
    @MaxLength(<number>reviewEntityMetaData.columns.title.length,{
        groups : [
            reviewEntityMetaData.validationGroups.add,
            reviewEntityMetaData.validationGroups.update,
        ]  
    })
    @Column(reviewEntityMetaData.columns.title)
    title : string;
//**********************************************************************************************************
    @MinLength(20,{
        groups : [
            reviewEntityMetaData.validationGroups.add,
            reviewEntityMetaData.validationGroups.update,
        ]
    })
    @MaxLength(<number>reviewEntityMetaData.columns.description.length,{
        groups : [
            reviewEntityMetaData.validationGroups.add,
            reviewEntityMetaData.validationGroups.update,
        ]
    })
    @Column(reviewEntityMetaData.columns.description)
    description : string;
//**********************************************************************************************************
    @MinLength(10,{
        groups : [
            reviewEntityMetaData.validationGroups.add,
            reviewEntityMetaData.validationGroups.update,
        ]
    })
    @MaxLength(<number>reviewEntityMetaData.columns.link.length,{
        groups : [
            reviewEntityMetaData.validationGroups.add,
            reviewEntityMetaData.validationGroups.update,
        ]
    })
    @Column(reviewEntityMetaData.columns.link)
    link : string;
//**********************************************************************************************************
    @Column(reviewEntityMetaData.columns.dateCreation)    
    dateCreation : string;
//**********************************************************************************************************
    @OneToMany(type => ItemReview, item => item.review,{cascade : true})
    items : ItemReview [];
//**********************************************************************************************************
    @OneToOne(type => Image,{cascade : true})
    @JoinColumn()
    image : Image;
//**********************************************************************************************************
    @ManyToOne(type => User,user => user.reviews,{cascade : false})
    user : User;
//**********************************************************************************************************
    @OneToMany(type => Subscribtion,subscribtion => subscribtion.review)
    subscribtions : Subscribtion[];
//**********************************************************************************************************
    @OneToMany(type => Comment, comment => comment.review,{cascade : true})
    comments : Comment [];
//**********************************************************************************************************    
}