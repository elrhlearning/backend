import { Transform } from 'class-transformer';
import { Column, Entity } from 'typeorm';
import { PrimaryGeneratedColumn } from 'typeorm';
import { imageEntityMetaData } from './image.metadata';
import { AppEntity } from '../app-entity.abstract';


@Entity({name : imageEntityMetaData.tableName})
export class Image extends AppEntity {
    @PrimaryGeneratedColumn(imageEntityMetaData.columns.id)
    @Transform(value => parseInt(value, 10), { toClassOnly: true })
    id : number;

    @Column(imageEntityMetaData.columns.name)
    name : string;  

    @Column(imageEntityMetaData.columns.path)
    path : string;

    @Column(imageEntityMetaData.columns.desc)
    desc : string;
}