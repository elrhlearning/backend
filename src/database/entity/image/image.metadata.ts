import { ColumnOptions } from 'typeorm';
import { PrimaryGeneratedColumnNumericOptions } from "typeorm/decorator/options/PrimaryGeneratedColumnNumericOptions";
import { DB_TYPE_CONFIG } from '../../config';

/**
 * 
 */
const id: PrimaryGeneratedColumnNumericOptions = { name: 'id' };
const name: ColumnOptions = { type: DB_TYPE_CONFIG.stringType, name: 'name', length: 200, nullable: false };
const path: ColumnOptions = { type: DB_TYPE_CONFIG.stringType, name: 'path', length: 200, nullable: false };
const desc: ColumnOptions = { type: DB_TYPE_CONFIG.stringType, name: 'desc', length: 200, nullable: false };
export const imageEntityMetaData  = {
    tableName : 'photo',
    columns : {
        id : id,
        name : name,
        path : path,
        desc : desc
    },
}