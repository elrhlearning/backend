import { ColumnOptions } from 'typeorm';
import { PrimaryGeneratedColumnNumericOptions } from "typeorm/decorator/options/PrimaryGeneratedColumnNumericOptions";
import { DB_TYPE_CONFIG } from '../../config';

/**
 * 
 */
const id : PrimaryGeneratedColumnNumericOptions = { name: 'id' };
const key : ColumnOptions = { type: DB_TYPE_CONFIG.stringType, name: 'key', length: 200, nullable: false , unique : false};
const value : ColumnOptions = { type: DB_TYPE_CONFIG.stringType, name: 'value', length: 4000, nullable: false };
export const itemReviewEntityMetaData  = {
    tableName : 'item_review',
    columns : {
        id : id,
        key  : key,
        value : value
    },
    relations : {
        review  : 'review'
    }
}