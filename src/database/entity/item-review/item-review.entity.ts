import { Review } from '../review/review.entity';
import { Column, ManyToOne } from 'typeorm';
import { PrimaryGeneratedColumn } from 'typeorm';
import { itemReviewEntityMetaData } from './item-review.metadata';
import { Entity } from 'typeorm';
import { type } from 'os';
import { AppEntity } from '../app-entity.abstract';

@Entity(itemReviewEntityMetaData.tableName)
export class ItemReview extends AppEntity{
    @PrimaryGeneratedColumn(itemReviewEntityMetaData.columns.id)
    id : number;
    
    @Column(itemReviewEntityMetaData.columns.key)
    key : string;

    @Column(itemReviewEntityMetaData.columns.value)
    values : string;

    @ManyToOne(type => Review, review => review.items,{ onDelete: 'CASCADE', onUpdate : 'CASCADE'})
    review : Review;
}