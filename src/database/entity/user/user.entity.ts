import { Review } from '../review/review.entity';
import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, JoinTable, OneToOne, JoinColumn, OneToMany } from 'typeorm';
import { MinLength, MaxLength, IsEmail, IsOptional, IsDefined, IsPositive } from "class-validator";
import { Transform } from 'class-transformer';
import { Role } from '../role/role.entity';
import { userEntityMetaData } from './user.metadata';
import { Image } from '../image/image.entity';
import { AppEntity } from '../app-entity.abstract';
//****************************************************************************************************************
/**
 * user entity
 * @author KARIM EL RHOURHA
 * @description User entity that contains ORM definition (TypeOrm), data validation (class-validator)
 *              class-transformer (from json to class)
 * 
 */
@Entity()
export class User extends AppEntity {
  //**********************************************************************************************************
  @PrimaryGeneratedColumn(userEntityMetaData.columns.id)
  @IsDefined({ groups: [userEntityMetaData.validationGroups.id] })
  @Transform(value => parseInt(value, 10), { toClassOnly: true })
  id: number;
//**********************************************************************************************************
  @MinLength(3, { groups: [userEntityMetaData.validationGroups.addUser] })
  @MaxLength(<number>userEntityMetaData.columns.name.length, { groups: [userEntityMetaData.validationGroups.addUser] })
  @Column(userEntityMetaData.columns.name)
  name: string;
//**********************************************************************************************************
  @MinLength(3, { groups: [userEntityMetaData.validationGroups.addUser] })
  @MaxLength(<number>userEntityMetaData.columns.lastname.length, { groups: [userEntityMetaData.validationGroups.addUser] })
  @Column(userEntityMetaData.columns.lastname)
  lastname: string;
//**********************************************************************************************************
  @IsEmail(undefined, 
    { 
      groups: [
      userEntityMetaData.validationGroups.addUser,
      userEntityMetaData.validationGroups.signIn
      ]
    }
  )
  @Column(userEntityMetaData.columns.email)
  email: string;
//**********************************************************************************************************
  @IsOptional({ groups: [userEntityMetaData.validationGroups.addUser] })
  @MaxLength(<number>userEntityMetaData.columns.phoneNumber.length, { groups: [userEntityMetaData.validationGroups.addUser] })
  @Column(userEntityMetaData.columns.phoneNumber)
  phoneNumber: string;
//**********************************************************************************************************
  @MaxLength(<number>userEntityMetaData.columns.password.length,
    { 
      groups: [
      userEntityMetaData.validationGroups.addUser,
      userEntityMetaData.validationGroups.signIn
      ]
    }  
  )
  @Column(userEntityMetaData.columns.password)
  password : string;
//**********************************************************************************************************
  @ManyToMany(type => Role, role => role.users, {eager : false})
  @JoinTable({name : 'users_roles'})
  roles : Role[];
//**********************************************************************************************************
  @OneToMany(type => Review, review => review.user)
  reviews : Review[];  
//**********************************************************************************************************
  @OneToOne(type => Image)
  @JoinColumn()
  image : Image;
  //********************************************************************************************************** 
  token : any = undefined;
}