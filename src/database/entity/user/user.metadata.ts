import { ColumnOptions } from 'typeorm';
import { PrimaryGeneratedColumnNumericOptions } from 'typeorm/decorator/options/PrimaryGeneratedColumnNumericOptions';
import { DB_TYPE_CONFIG } from '../../config';

//****************************************************************************************************************
/**
 * Column options that will be used in the app the to avoid using column name hard coded
 */
const name: ColumnOptions = { type: DB_TYPE_CONFIG.stringType, name: 'name', length: 200 };
const id: PrimaryGeneratedColumnNumericOptions = { name: 'id' };
const lastname: ColumnOptions = { type: DB_TYPE_CONFIG.stringType, name: 'lastname', length: 200, nullable: true };
const email: ColumnOptions = { type: DB_TYPE_CONFIG.stringType, name: 'email', length: 200, nullable: true };
const phoneNumber: ColumnOptions = { type: DB_TYPE_CONFIG.stringType, name: 'phone_number', length: 20, nullable: true };
const password: ColumnOptions = { type: DB_TYPE_CONFIG.stringType, name: 'password', length: 50, nullable: true ,select : false};
const tableName : string = "user";
//****************************************************************************************************************
/**
 * metadata object of the entity
 */
export const userEntityMetaData = {
  tableName : tableName,
  columns : {
      name : name,
      id : id,
      lastname : lastname,
      email : email,
      phoneNumber : phoneNumber,
      password : password
  },
  relations : {
    role : tableName +"."+"roles",
    image : tableName +"."+"image"
  },
  validationGroups : {
    id: 'id',
    addUser: 'addUser',
    signIn : 'signIn'
  }
}
//****************************************************************************************************************