import { ColumnOptions } from 'typeorm';
import { PrimaryGeneratedColumnNumericOptions } from "typeorm/decorator/options/PrimaryGeneratedColumnNumericOptions";
import { DB_TYPE_CONFIG } from '../../config';

/**
 * 
 */
const id : PrimaryGeneratedColumnNumericOptions = { name: 'id' };
const content : ColumnOptions = { type: DB_TYPE_CONFIG.stringType, name: 'content', length: 3000, nullable: false , unique : false};
const dateCreation : ColumnOptions = { type: DB_TYPE_CONFIG.dateType, default : () => 'CURRENT_TIMESTAMP' , name: 'date_creation', nullable: false };
const tableName ='comment'
export const commentEntityMetaData  = {
    tableName : tableName,
    columnAlias : {
        id : `${tableName}.${id.name}`,
        content : `${tableName}.${content.name}`,
        dateCreation : `${tableName}.${dateCreation.name}`
    },
    columns : {
        id : id,
        content  : content,
        dateCreation : dateCreation
    },
    relations : {
        review  : `${tableName}.review`,
        owner : `${tableName}.owner`,
    }
    ,
    validationGroups : {
        add : 'addComment',
        update : 'updateComment',
        delete : 'deleteComment',
        find : 'findComment'
    }
}