import { Transform } from 'class-transformer';
import { IsNumber, IsDefined, MaxLength, MinLength } from 'class-validator';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Review } from '../review/review.entity';
import { User } from './../user/user.entity';
import { commentEntityMetaData } from './comment.metadata';
import { AppEntity } from '../app-entity.abstract';


@Entity(commentEntityMetaData.tableName)
export class Comment  extends AppEntity{
    @PrimaryGeneratedColumn(commentEntityMetaData.columns.id)
    @IsDefined({        
        groups: [
            commentEntityMetaData.validationGroups.update,
            commentEntityMetaData.validationGroups.delete,
            commentEntityMetaData.validationGroups.find,
    ],
    message : `Comment id must be positiv`})
    @Transform(value => parseInt(value, 10))
    id : number;
    
    @MinLength(5, { 
        groups: [
        commentEntityMetaData.validationGroups.update,
        commentEntityMetaData.validationGroups.add,
    ] })
    @MaxLength(<number>commentEntityMetaData.columns.content.length, { 
    groups: [
        commentEntityMetaData.validationGroups.update,
        commentEntityMetaData.validationGroups.add,
        ] 
    })
    @Column(commentEntityMetaData.columns.content)
    content : string;
    
    @Column(commentEntityMetaData.columns.dateCreation)
    dateCreation : string;

    @ManyToOne(type => Review,review => review.comments,{ cascade : false ,eager : true,onDelete: 'CASCADE', onUpdate : 'CASCADE'})
    review : Review;

    @ManyToOne(type => User,{eager : true, onDelete: 'CASCADE', onUpdate : 'CASCADE'})
    owner : User;
}