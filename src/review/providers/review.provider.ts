import { ItemReviewRepositoryToken, ReviewRepositoryToken, CommentRepositoryToken } from './../config';
import { ReviewService } from "../services/review.service";
import { ReviewServiceToken, ImageRepositoryToken, CommentServiceToken } from '../config';
import { ImageRepository } from "../repositorys/image/image.repository";
import { ReviewRepository } from '../repositorys/review/review.repository';
import { CommentRepository } from '../repositorys/comment/comment.repository';
import { CommentService } from '../services/comment/comment.service';


export const reviewProviders = [
    ReviewService,
    ReviewRepository,
    ImageRepository,
    CommentRepository,
    CommentService,
    {
        provide : ReviewRepositoryToken,
        useClass : ReviewRepository         
    },
    {
        provide : ImageRepositoryToken,
        useClass : ImageRepository  
    },
    {
        provide : ItemReviewRepositoryToken,
        useClass : ReviewRepository  
    },
    {
      provide : CommentRepositoryToken,
      useClass : CommentRepository  
    },
    {
        provide : ReviewServiceToken,
        useClass : ReviewService 
    },
    {
        provide : CommentServiceToken,
        useClass : CommentService
    }
];