import { ItemReview } from '../../../database/entity/item-review/item-review.entity';
export interface ItemType {
  item? :ItemReview;
  items?: ItemReview[];
}
export interface IItemReviewRepository {
    saveItems(items : ItemReview[])  : Promise<ItemReview[]>;
}