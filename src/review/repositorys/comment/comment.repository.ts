import { ICommentRepository } from "./comment-repository.interface";
import { CommentTypeOrmRepositoryToken } from "../../../database/config";
import { Repository } from "typeorm";
import { OrWhereSetter } from "../../../database/services/or-where-setter";
import { AndWhereSetter } from "../../../database/services/and-where-setter";
import { OrderBySetter } from "../../../database/services/order-by-setter";
import { Inject, Injectable } from "@nestjs/common";
import { Comment } from "../../../database/entity/comments/comment.entity";
import { EntityRelation } from "../../../common/interfaces/entity-relation.interface";
import { RelationApplicator } from "../../../common/interfaces/relation-applicator.class";
import { commentEntityMetaData } from "../../../database/entity/comments/comment.metadata";

@Injectable()
export class CommentRepository implements ICommentRepository{
  //****************************************************************************************************    
  constructor(
    @Inject(CommentTypeOrmRepositoryToken) 
    private readonly commentTypeOrmRepository : Repository<Comment>,
    private readonly relationsApplicator : RelationApplicator,
    )
    {

    }
  //****************************************************************************************************    
  async addComment(comment : Comment) : Promise<Comment>{
    this.commentTypeOrmRepository.createQueryBuilder()
    return this.commentTypeOrmRepository.save(comment);
  }
//****************************************************************************************************    
  async deleteComment(comment: Comment): Promise<Comment> {
    return this.commentTypeOrmRepository.remove(comment);
  }
//****************************************************************************************************    
  async updateComment(comment: Comment): Promise<Comment> {
    return this.commentTypeOrmRepository.save(comment);
  }
//****************************************************************************************************    
  getComment(id: number,relations? : EntityRelation[]): Promise<Comment> {
    let queryBuilder = this.createQueryBuilder();
    if(relations)
    queryBuilder = this.relationsApplicator.apply(queryBuilder,relations);
    queryBuilder.where(
      `${commentEntityMetaData.columnAlias.id} = :id`,{id : id}
    );
    return queryBuilder.getOne();
  }
//****************************************************************************************************  
  private createQueryBuilder(){
    return this.commentTypeOrmRepository.createQueryBuilder(commentEntityMetaData.tableName);
  }
}