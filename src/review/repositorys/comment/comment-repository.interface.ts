import { Comment } from './../../../database/entity/comments/comment.entity';
import { EntityRelation } from '../../../common/interfaces/entity-relation.interface';
export interface ICommentRepository {
  addComment(comment : Comment) : Promise<Comment>;
  deleteComment(comment : Comment) : Promise<Comment>;
  updateComment(comment : Comment) : Promise<Comment>;
  getComment(id : number,relations? : EntityRelation[]) : Promise<Comment>;
}