import { GetReviewsParams } from './../../services/query-helper.interface';
import { Review } from '../../../database/entity/review/review.entity';
import { EntityRelation } from '../../../common/interfaces/entity-relation.interface';
import { Where, OrderBy } from '../../services/query-helper.interface';

export interface IReviewCrudRepository {
    /**
     * Get Filtred reviews
     * @param params object of type GetReviewsParams that contains all params (filters,order,pagination,search...)
     */    
    getReviews(params : GetReviewsParams) : Promise<Review[]>;
    /**
     * add new review 
     * @param review 
     * @returns number id of review inserted 
     */
    addReview(review : Review) : Promise<Review>;
    /**
     * update review
     * @param review 
     * @returns true if updated
     */
    updateReview(review : Review) : Promise<Review>;
    /**
     * delete review 
     * @param review 
     * @returns true if deleted
     */
    deleteReview(review : Review) : Promise<Review>; 

    /**
     * Search review by given attribute
     */
    getReviewBy(wheres : Where[],relations? : EntityRelation[] ,orders? : OrderBy[]) : Promise<Review>;
}