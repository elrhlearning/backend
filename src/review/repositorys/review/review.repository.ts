import { CommentTypeOrmRepositoryToken } from './../../../database/config';
import { Comment } from './../../../database/entity/comments/comment.entity';
import { ICommentRepository } from './../comment/comment-repository.interface';
import { userEntityMetaData } from './../../../database/entity/user/user.metadata';
import { AndWhereSetter } from './../../../database/services/and-where-setter';
import { GetReviewsParams, Where } from './../../services/query-helper.interface';
import { OrWhereSetter } from './../../../database/services/or-where-setter';
import { OrderBySetter } from './../../../database/services/order-by-setter';
import { itemReviewEntityMetaData } from './../../../database/entity/item-review/item-review.metadata';
import { imageEntityMetaData } from './../../../database/entity/image/image.metadata';
import { reviewEntityMetaData } from './../../../database/entity/review/review.metadata';
import { SelectQueryBuilder } from 'typeorm';
import { ItemReviewTypeOrmRepositoryToken,ReviewTypeOrmRepositoryToken } from '../../../database/config';
import { ItemReview } from '../../../database/entity/item-review/item-review.entity';
import { Review } from '../../../database/entity/review/review.entity';
import { IReviewCrudRepository, } from "./review-crud.interface.repository";
import { Injectable, Inject, BadRequestException } from '@nestjs/common';
import { IItemReviewRepository } from '../item-review/item.repository.interface';
import { Repository, Brackets,WhereExpression } from 'typeorm';
import { commentEntityMetaData } from '../../../database/entity/comments/comment.metadata';
import { EntityRelation } from '../../../common/interfaces/entity-relation.interface';
import { OrderBy } from '../../services/query-helper.interface';
import { RelationApplicator } from '../../../common/interfaces/relation-applicator.class';




@Injectable()
export class ReviewRepository implements IReviewCrudRepository,IItemReviewRepository {

//**************************************************************************************************** 
    /** 
     * columns that will be used in search, if you want ta add another column, just add the name her 
     * from the entityMetadata object, dont put the column name hard coded, check if the column is part of
     * query result [getReviews()] 
     */ 
    private searchReviewsColumns : any = [
        reviewEntityMetaData.columnsAlias.id,
        reviewEntityMetaData.columnsAlias.link,
        reviewEntityMetaData.columnsAlias.description,
        reviewEntityMetaData.columnsAlias.title,
    ];
//****************************************************************************************************    
    constructor(
        @Inject(ItemReviewTypeOrmRepositoryToken) 
        private readonly itemReviewTypeOrmRepository : Repository<ItemReview>,
        @Inject(ReviewTypeOrmRepositoryToken) 
        private readonly reviewTypeOrmRepository : Repository<Review>,
        private readonly relationsApplicator : RelationApplicator,
        private readonly orWheresSetter : OrWhereSetter,
        private readonly andWheresSetter : AndWhereSetter,
        private readonly orderBySetter :  OrderBySetter
    ){

    }
//**************************************************************************************************** 
    async getReviews(params : GetReviewsParams): Promise<Review[]> {
            let queryBuilder =  this.getQueryBuilder()
            // relation to get in results
            .leftJoinAndSelect(reviewEntityMetaData.relations.image,imageEntityMetaData.tableName)
            .leftJoinAndSelect(reviewEntityMetaData.relations.user,userEntityMetaData.tableName)
            // all wheres (by AND)
            queryBuilder = <SelectQueryBuilder<Review>> this.andWheresSetter.setWheres(queryBuilder,params.wheres);
            // search
            queryBuilder.andWhere(
                new Brackets(qb => {
                    this.search(params.keywordSearch,qb);
                })
            );
            //skip
            if(params.skip != null)
                queryBuilder = queryBuilder.skip(params.skip);
            // take
            if(params.take != null)
                queryBuilder = queryBuilder.take(params.take);  
            // order
            queryBuilder = this.orderBySetter.setOrdersBy(queryBuilder,params.orderBy);
  
            return queryBuilder.getMany();   
    }    
//****************************************************************************************************     
    async addReview(review: Review): Promise<Review> {
        return await this.reviewTypeOrmRepository.save(review);
    }
//****************************************************************************************************     
    updateReview(review: Review): Promise<Review> {
        return this.addReview(review);
    }
//****************************************************************************************************     
    deleteReview(review: Review): Promise<Review> {
        return this.reviewTypeOrmRepository.remove(review);
    }
//**************************************************************************************************** 
    async saveItems(items: ItemReview[]): Promise<ItemReview[]> {
        return await this.itemReviewTypeOrmRepository.save(items);
    }
//**************************************************************************************************** 
    private  getQueryBuilder() : SelectQueryBuilder<Review> {
        return this.reviewTypeOrmRepository.createQueryBuilder(reviewEntityMetaData.tableName);
    }
//****************************************************************************************************  
    private search(keyword : string,queryBuilder : SelectQueryBuilder<Review> | WhereExpression ) : SelectQueryBuilder<Review> | WhereExpression {
        let wheres : Where [] = [];
        if(keyword == null)
            return queryBuilder;
        for(let column of this.searchReviewsColumns){
            wheres.push({where : column + ' like :key' , params : {key : '%'+keyword+'%'}});
        }
        return this.orWheresSetter.setWheres(queryBuilder,wheres);
    }
//****************************************************************************************************  
    async getReviewBy(wheres : Where[],relations? : EntityRelation[],orders? : OrderBy[]) : Promise<Review> {
        let queryBuilder =  this.getQueryBuilder();
        // get review without relations
        if(relations && relations.length > 0)
            queryBuilder = this.relationsApplicator.apply(queryBuilder,relations);
        // wheres  
        if(wheres && wheres.length > 0)
            queryBuilder = <SelectQueryBuilder<Review>> this.andWheresSetter.setWheres(queryBuilder,wheres);
        // orderbys
        if(orders && orders.length > 0)
            queryBuilder = this.orderBySetter.setOrdersBy(queryBuilder,orders);
        return queryBuilder.getOne();
    }
//****************************************************************************************************    
}