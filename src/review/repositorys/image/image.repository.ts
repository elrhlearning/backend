import { ImageTypeOrmRepositoryToken } from '../../../database/config';
import { Image } from '../../../database/entity/image/image.entity';
import { IImageRepositoy } from "./image.repository.interface";
import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export class ImageRepository implements IImageRepositoy{
//****************************************************************************************************     
    constructor(@Inject(ImageTypeOrmRepositoryToken) private readonly imageTypeOrmRepository : Repository<Image>){
//**************************************************************************************************** 
    }
    async saveImage(image: Image): Promise<Image> {
        return this.imageTypeOrmRepository.save(image);
    }
//****************************************************************************************************     
    async findImage(id : number) : Promise<Image> {
        return this.imageTypeOrmRepository.findOne(id);
    }
//****************************************************************************************************     
}