import { Image } from '../../../database/entity/image/image.entity';

export interface IImageRepositoy{
    saveImage(image : Image) : Promise<Image>;
    findImage(id : number) : Promise<Image>;
}