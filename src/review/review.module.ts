import { UserModule } from './../user/user.module';
import { Module, MulterModule, forwardRef } from '@nestjs/common';
import { ReviewController } from './controllers/review.controller';
import { reviewProviders } from './providers/review.provider';
import { NotificationModule } from '../notification/notification.module';

@Module({
  imports : [UserModule,MulterModule.register({
    dest: 'D:/data/nodejs/backend/upload/',
  }),
  forwardRef(() => NotificationModule),
  ],
  controllers: [ReviewController],
  providers : reviewProviders,
  exports : reviewProviders
})
export class ReviewModule {}
