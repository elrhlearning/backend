var multer  = require('multer');
var path = require('path');

export const ReviewServiceToken: string  = "RST";
export const CommentServiceToken: string  = "CommST";
export const ReviewRepositoryToken: string  = "RRT";
export const ImageRepositoryToken: string  = "IRT";
export const CategoryRepositoryToken: string  = "CRT";
export const ItemReviewRepositoryToken: string  = "IRRT";
export const CommentRepositoryToken : string = 'COMMRT';
// file upload config
const reviewsDir : string = 'public/v1/images/';
// multer config
export const  multerReviewConfig : any = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, reviewsDir)
      },
    filename: function (req, file, cb) {
        cb(null, req.query.imageId + path.extname(file.originalname))
    }
})