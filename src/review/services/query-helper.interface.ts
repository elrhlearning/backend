export interface Where {
    where : string;
    params? : any;
}

export interface OrderBy {
    column : string;
    order? : "ASC" | "DESC";
}

export interface GetReviewsParams {
    wheres? : Where[];
    skip? : number;
    take?: number;
    orderBy ?: OrderBy[],
    keywordSearch ? : string;
}