import { Comment } from './../../database/entity/comments/comment.entity';
import { Image } from './../../database/entity/image/image.entity';
import { GetReviewsParams } from './query-helper.interface';
import { Review } from '../../database/entity/review/review.entity';
import { ValidationOptions } from 'class-validator';

export interface IReviewService{
    /**
     * Get Filtred reviews
     * @param params object of type AllReviewsParams tnhat contains all params (filters,order,pagination,search...)
     */    
    getAllReviews(params : GetReviewsParams) : Promise<Review[]>;
    /**
     * add new review 
     * steps :
     * 1 - check if the user exist
     * 2 - check if the category exist
     * 3 - prepare relations
     * 4 - persist
     * @param review 
     * @returns number id of review inserted 
     */
    addReview(review : Review,file : any) : Promise<Review>;
    /**
     * update review
     * @param review 
     * @returns true if updated
     */
    updateReview(review : Review) : Promise<boolean>;
    /**
     * delete review 
     * @param review 
     * @returns true if deleted
     */
    deleteReview(review : Review) : Promise<boolean>;

    getReviewImage(image : Image) : Promise<Image>;
    /**
     * get review by id with all relation
     * @param review
     */
    getReviewById(review : Review) : Promise<Review>;

    /**
     * get review by id without relation
     * @param review 
     */
    getMinimizedReview(review : Review): Promise<Review>;

    checkExistingReview(review : Review);
}