import { ICommentService } from './comment-service.interface';
import { ICommentRepository } from '../../repositorys/comment/comment-repository.interface';
import { Inject, BadRequestException, Injectable, InternalServerErrorException } from '@nestjs/common';
import { CommentRepositoryToken } from '../../config';
import { Comment } from '../../../database/entity/comments/comment.entity';
import { EntityRelation } from '../../../common/interfaces/entity-relation.interface';
import { commentEntityMetaData } from '../../../database/entity/comments/comment.metadata';
import { reviewEntityMetaData } from '../../../database/entity/review/review.metadata';
import { AppLogger } from '../../../common/loggers/app-logger';

@Injectable()
export class CommentService implements ICommentService{
//****************************************************************************************************
    constructor(
        @Inject(CommentRepositoryToken)
        private readonly commentRepo : ICommentRepository,
        private readonly logger : AppLogger
    ){

    }
//****************************************************************************************************
    async getComment(comment: Comment): Promise<Comment> {
      this.logger.init(__filename);
      let m : Comment;
      // define relation
      let reviewRelation : EntityRelation = {
        relation : commentEntityMetaData.relations.review,
        alias : reviewEntityMetaData.tableName 
      };
      let ownerRelation : EntityRelation = {
          relation : commentEntityMetaData.relations.owner,
          alias : 'owner'
      };
      // get result
      try {
        m  = await this.commentRepo.getComment(comment.id,[reviewRelation,ownerRelation]);
      } catch (error) {
        this.logger.getLogger().error(`getComment() - error : ${this.logger.logObject(error)}`);
        throw new InternalServerErrorException();
      }
      if(!m){
        this.logger.getLogger().warn(`getComment() - comment not found ID :${comment.id}`);
        throw new BadRequestException(`comment not found ID :${comment.id}`);
      }
      this.logger.getLogger().info(`getComment() - returning comment  :${this.logger.logObject(m)}`);
      return m;

    }  
//****************************************************************************************************      
    async getMinimizedComment(comment: Comment): Promise<Comment> {
      this.logger.init(__filename);
      return await this.checkExistingComment(comment);
    }
//****************************************************************************************************    
    async addComment(comment: Comment): Promise<Comment> {
      this.logger.init(__filename);
      this.logger.getLogger().info(`addComment() - received object  : ${this.logger.logObject(comment)}`);
      let c : Comment;
      try {
        c = await this.commentRepo.addComment(comment); 
      } catch (error) {
        this.logger.getLogger().error(`addComment() - error : ${this.logger.logObject(error)}`);
        throw new InternalServerErrorException();
      }
      this.logger.getLogger().info(`addComment() - creating comment : ${this.logger.logObject(c)}`);
      return c;
    }
//****************************************************************************************************    
    async updateComment(comment: Comment): Promise<Comment> {
      this.logger.init(__filename);
      // check if comment existe
      await this.checkExistingComment(comment);
      let c : Comment;
      try {
        c = await this.commentRepo.updateComment(comment);
      } catch (error) {
        this.logger.getLogger().error(`updateComment() - error :${this.logger.logObject(error)}`);
        throw new InternalServerErrorException();
      }
      // return comment
      this.logger.getLogger().info(`updateComment() - updating comment  :${this.logger.logObject(c)}`);
      return c;
    }
//****************************************************************************************************    
    async deleteComment(comment: Comment): Promise<Comment> {
      this.logger.init(__filename);
      await this.checkExistingComment(comment);
      let c : Comment;
      try {
        c = await this.commentRepo.deleteComment(comment);
      } catch (error) {
        this.logger.getLogger().error(`deleteComment() - error :${this.logger.logObject(error)}`);
        throw new InternalServerErrorException();
      }
      this.logger.getLogger().info(`deleteComment() - delleting comment :${this.logger.logObject(c)}`);
      return c;
    }
//****************************************************************************************************    
    private async checkExistingComment(comment : Comment){
      this.logger.init(__filename);
      let c : any;
      try {
        c = (await this.commentRepo.getComment(comment.id));
      } catch (error) {
        this.logger.getLogger().error(`checkExistingComment() - error :${this.logger.logObject(error)}`);
        throw new InternalServerErrorException();
      }
      if( !c){
        this.logger.getLogger().info(`checkExistingComment() - comment not found ID  :${comment.id}`);
        throw new BadRequestException(`Comment not found ID : ${comment.id}`);
      }
      return c;
    }
//****************************************************************************************************    
}