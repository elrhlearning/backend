import { Comment } from '../../../database/entity/comments/comment.entity';
import { ValidationOptions } from 'class-validator';
import { Review } from '../../../database/entity/review/review.entity';
import { AppEntity } from '../../../database/entity/app-entity.abstract';
export interface ICommentService {
    getComment(comment : Comment) : Promise<Comment>;
    getMinimizedComment(comment :Comment) : Promise<Comment>;
    addComment(comment :  Comment) : Promise<Comment>;
    updateComment(comment : Comment | Comment) : Promise<Comment>;
    deleteComment(comment : Comment | Comment) : Promise<Comment>;
}