import { CommentRepositoryToken } from './../config';
import { Comment } from './../../database/entity/comments/comment.entity';
import { reviewEntityMetaData } from './../../database/entity/review/review.metadata';
import { Image } from './../../database/entity/image/image.entity';
import { GetReviewsParams, Where, OrderBy } from './query-helper.interface';
import { UserServiceToken } from './../../user/config';
import { IItemReviewRepository } from '../repositorys/item-review/item.repository.interface';
import { User } from '../../database/entity/user/user.entity';
import { IUserService } from 'src/user/services/user.service.interface';
import { IReviewCrudRepository } from '../repositorys/review/review-crud.interface.repository';
import { IReviewService } from './review.service.interface';
import { Review } from '../../database/entity/review/review.entity';
import { ValidationOptions } from 'class-validator';
import { Injectable, BadRequestException ,Inject, HttpException, InternalServerErrorException} from '@nestjs/common';
import { IImageRepositoy } from '../repositorys/image/image.repository.interface';
import { ReviewRepositoryToken, ImageRepositoryToken, ItemReviewRepositoryToken, CommentServiceToken } from '../config';
import { EntityRelation } from '../../common/interfaces/entity-relation.interface';
import { imageEntityMetaData } from '../../database/entity/image/image.metadata';
import { userEntityMetaData } from '../../database/entity/user/user.metadata';
import { commentEntityMetaData } from '../../database/entity/comments/comment.metadata';
import { itemReviewEntityMetaData } from '../../database/entity/item-review/item-review.metadata';
import { AppLogger } from '../../common/loggers/app-logger';

@Injectable()
export class ReviewService implements IReviewService{
//****************************************************************************************************
    constructor(@Inject(ReviewRepositoryToken) 
                private readonly reviewRepository : IReviewCrudRepository,
                private readonly logger : AppLogger,
                @Inject(ImageRepositoryToken) 
                private readonly imageRepository : IImageRepositoy,
                @Inject(UserServiceToken) private readonly userService : IUserService,
                ){

    }    
//****************************************************************************************************    
    async getAllReviews(params: GetReviewsParams): Promise<Review[]> {
      this.logger.init(__filename);
      try {
        return await this.reviewRepository.getReviews(params);
      } catch (error) {
        this.logger.getLogger().error(`getAllReviews() - error : ${this.logger.logObject(error)}`);
        throw new InternalServerErrorException();
      }
        
    }   
//****************************************************************************************************     
    async addReview(review: Review,file : any): Promise<Review> {
        this.logger.init(__filename);
        // step 1 
        let user : User = await this.userService.getUser(review.user) ;
        if(!user)
            throw new BadRequestException("user not found : " + review.user);
        // step 2 @To Do
        // step 3
        review.image = new Image();
        review.image.desc=`File name : ${file.originalname} - File Type : ${file.mimetype}`;
        review.image.path = file.destination;
        review.image.name = file.filename;
        // save user
        review.user = user;
        // step 4 - persist
        try {
            return await this.reviewRepository.addReview(review);  
        } catch (error) {
            this.logger.getLogger().error(`AddReview() - error : ${this.logger.logObject(error)}`);
            throw new InternalServerErrorException();
        }
    }
//****************************************************************************************************    
    async updateReview(review: Review): Promise<boolean> {
      this.logger.init(__filename);
      await this.checkExistingReview(review);
      try {
        review = await this.reviewRepository.updateReview(review);
      } catch (error) {
        this.logger.getLogger().error(`updateReview() - Error : ${this.logger.logObject(error)}`);
        throw new InternalServerErrorException();
      }
      if(review){
        this.logger.getLogger().info(`Review ${review.id} updated correctly`);
        return true;
      }  
      return false; 


    }
//****************************************************************************************************    
    async deleteReview(review: Review): Promise<boolean> {
      this.logger.init(__filename);
      await this.checkExistingReview(review);
      try {
        review = await this.reviewRepository.deleteReview(review);
      } catch (error) {
        this.logger.getLogger().error(`deleteReview() - ${this.logger.logObject(error)}`);
        throw new InternalServerErrorException();
      }
      if(review){
          this.logger.getLogger().info(`Review ${review.title} deleted correctly`);
          return true;
      }
      return false;
    } 
//**************************************************************************************************** 
  async getReviewImage(image : Image) : Promise<Image> {
      if(!image || !image.id)
          return null;
      return this.imageRepository.findImage(image.id);
  }
//****************************************************************************************************     
  async getReviewById(review : Review): Promise<Review> {
    this.logger.init(__filename);
    let r : Review;
      // where
      let whereID : Where = {
        where : `${reviewEntityMetaData.columnsAlias.id} = :id`,
        params : {id : review.id}
      }
      // relations
      let relations : EntityRelation[] = [
        { relation : reviewEntityMetaData.relations.image, alias : imageEntityMetaData.tableName},
        { relation : reviewEntityMetaData.relations.user, alias : userEntityMetaData.tableName},
        { relation : reviewEntityMetaData.relations.comments, alias : commentEntityMetaData.tableName},
        { relation : commentEntityMetaData.relations.owner, alias : 'owner'},
        { relation : reviewEntityMetaData.relations.items, alias :itemReviewEntityMetaData.tableName},
      ];
      // orders
      let orders : OrderBy[] = [
        {
          column : `${commentEntityMetaData.tableName}.${commentEntityMetaData.columns.dateCreation.name}`,
          order : 'DESC'
        }
      ]
      // call repo
      try {
        r = await this.reviewRepository.getReviewBy([whereID],relations,orders);
      } catch (error) {
        this.logger.getLogger().error(`getReviewById() error : ${this.logger.logObject(error)}`);
        throw new InternalServerErrorException();
      }
      if(!r){
        this.logger.getLogger().warn(`Review not found, review ID : ${review.id}`);
        throw new BadRequestException(`Review not found, review ID : ${review.id}`);
      }
      this.logger.getLogger().warn(`getReviewById() - returning review  : ${this.logger.logObject(r)}`);
      return r;
  }
//****************************************************************************************************     
  async getMinimizedReview(review : Review): Promise<Review> {
    this.logger.init(__filename);
    let r  : Review;
    // where
    let whereID : Where = {
      where : `${reviewEntityMetaData.columnsAlias.id} = :id`,
      params : {id : review.id}
    }
    // call repo
    try {
      r = await this.reviewRepository.getReviewBy([whereID]);
    } catch (error) {
      this.logger.getLogger().error(`getMinimizedReview() error : ${this.logger.logObject(error)}`);
      throw new InternalServerErrorException();
    }
    // check return value
    if(!r){
      this.logger.getLogger().warn(`getMinimizedReview() - Review not found, review ID : ${review.id}`);
      throw new BadRequestException(`Review not found, review ID : ${review.id}`);
    }
    this.logger.getLogger().warn(`getReviewById() - returning review  : ${this.logger.logObject(r)}`);
    return r;
  }    
//****************************************************************************************************
  async checkExistingReview(review : Review) {
    this.logger.init(__filename);
    return await this.getMinimizedReview(review);
  }
/* //****************************************************************************************************    
    async addComment(comment: Comment): Promise<Comment> {
      if(!comment || !comment.review)
          throw new BadRequestException('Invalid data request');
      let review = await this.getMinimizedReview(comment.review.id);
      if(!review)
        throw new BadRequestException();
      comment.review = review;
      let user = await this.userService.getUser(comment.owner);
      if(!user)
        throw new BadRequestException();
      comment.owner = user;
      return this.commentRepository.addComment(comment);
    }
//****************************************************************************************************    
    async deleteComment(comment: Comment): Promise<boolean> {
      if(!comment || !comment.id)
        throw new BadRequestException('Invalid data request');
      let c = await this.commentRepository.getComment(comment.id);
      if(!c )
        throw new BadRequestException('Comment not found');
      return (this.commentRepository.deleteComment(c) != undefined);
    }
//****************************************************************************************************    
    async updateComment(comment: Comment): Promise<boolean> {
      if(!comment || !comment.id)
        throw new BadRequestException('Invalid data request');
      let c = await this.commentRepository.getComment(comment.id); 
      if(!c )
        throw new BadRequestException('Comment not found');
      return (this.commentRepository.updateComment(comment) != undefined);
    } */
//****************************************************************************************************    
}