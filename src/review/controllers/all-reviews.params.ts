export class AllReviewsParams {
    filters : [];
    order : [];
    nbItems : number;
    currentPage : number;
    searchTerm : string;
}