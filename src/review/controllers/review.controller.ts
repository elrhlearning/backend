import { Comment } from './../../database/entity/comments/comment.entity';
import { Image } from './../../database/entity/image/image.entity';
import { Review } from './../../database/entity/review/review.entity';
import { ReviewServiceToken } from './../config';
import { IReviewService } from './../services/review.service.interface';
import { GetReviewsParams } from './../services/query-helper.interface';
import { multerReviewConfig } from './../config';
import { NOTIFICATION_SSERVICE_TOKEN } from '../../notification/config';
import { INotificationService } from '../../notification/services/notification/notification-service.interface';
import { ICommentService } from '../services/comment/comment-service.interface';
import { CommentServiceToken } from '../config';
import { commentEntityMetaData } from '../../database/entity/comments/comment.metadata';
import { EntityValidator } from '../../common/validator/entity.validator';
import { reviewEntityMetaData } from '../../database/entity/review/review.metadata';
import {    Controller, Post, Body,Param, Inject, Query, BadRequestException, 
            Request, UseInterceptors, FileInterceptor, UploadedFile, Get, Delete } from '@nestjs/common';
import { RequestLoggingInterceptor } from '../../common/interceptors/request-loggin.interceptor';
import { UpdateReviewNotificationBuilder } from '../../common/helpers/update-review-notification.builder';
import { NotificationBuilderMananger } from '../../common/helpers/builder.manager';
import { User } from '../../database/entity/user/user.entity';
import { Notification } from '../../database/entity/notifications/notification.abstract.entity';
import { ReviewNotification } from '../../database/entity/notifications/review-notification.entity';
import { CommentedReviewNotificationBuilder } from '../../common/helpers/commentted-review-notification.builder';
import { plainToClass } from 'class-transformer';


@Controller('review')
@UseInterceptors(RequestLoggingInterceptor)
export class ReviewController {
//****************************************************************************************************
    constructor(@Inject(ReviewServiceToken) 
                private readonly reviewService : IReviewService,
                @Inject(NOTIFICATION_SSERVICE_TOKEN)
                private readonly notifService : INotificationService,
                private buildersManager : NotificationBuilderMananger,
                @Inject(CommentServiceToken)
                private readonly commentService : ICommentService,
                private validator : EntityValidator
    ){

    }
//****************************************************************************************************
    @Post('all')
    async getAllReviews(@Body() params : GetReviewsParams)  {
        try {
            return await this.reviewService.getAllReviews(params);
        } catch (error) {
            throw new BadRequestException(error.message);
        }
    }
//****************************************************************************************************
    @Delete(':id')
    async deleteReview(@Param('id') id : number){
        let r = new Review();
        r.id = id;
        await this.validator.validate(r,{groups : [reviewEntityMetaData.validationGroups.delete]},Review);
        return this.reviewService.deleteReview(r);
    }
//****************************************************************************************************
    @Post('add')
    @UseInterceptors (FileInterceptor('image',{storage : multerReviewConfig }))
    async addReview(@Request() req,@UploadedFile() file)  {
        try {
            let review : Review = plainToClass(Review,<Object>JSON.parse(req.body.review));
            review =  await this.reviewService.addReview(review,file);
            review.user = null;
            return review;
        } catch (error) {
            throw new BadRequestException(error.message);
        }
    }
//**************************************************************************************************** 
    @Get(':id')
    async getReviewById(@Param('id') id : number){
        let r = new Review();
        r.id = id;
        await this.validator.validate(r,{groups : [reviewEntityMetaData.validationGroups.find]},Review);
        return this.reviewService.getReviewById(r);
    }
//****************************************************************************************************
    @Post('comment/add')
    async addComment(@Body() comment : Comment) : Promise<Comment>{
        // validate data
        await this.validator.validate(comment,{groups : [commentEntityMetaData.validationGroups.add]},Comment);
        // add comment
        let c = await this.commentService.addComment(comment); 
        // create && send notification
        this.notifService.handleReviewNotification(this.createCommentReviewNotification(c));
        return c;
    }
//****************************************************************************************************  
    @Post('comment/update')
    async updateComment(@Body() comment : Comment) : Promise<boolean>{
        // validate data
        await this.validator.validate(comment,{groups : [commentEntityMetaData.validationGroups.update]},Comment);
        // update comment
        let r = await this.commentService.updateComment(comment); 
        // create && send notification 
        this.notifService.handleReviewNotification(this.createCommentReviewNotification(r));
        return r != null;
    }
//****************************************************************************************************   
    @Delete('comment/:id')
    async deleteComment(@Param('id') id : any) : Promise<boolean>{
      let c = new Comment();
      c.id = id;
      await this.validator.validate(c,{groups : [commentEntityMetaData.validationGroups.delete]},Comment);
      return (await this.commentService.deleteComment(c) != undefined);
    }
//****************************************************************************************************
  private createUpdateReviewNotification(review : Review,user? : User,date? : string) : ReviewNotification{
    if(!date)
        date = new Date().toISOString();
    return <ReviewNotification>
            (<UpdateReviewNotificationBuilder>(this.buildersManager.getBuilder(UpdateReviewNotificationBuilder)))
            .start()
            .setDateModification(date)
            .setReview(review)
            .setState(true)
            .build();
  }
//****************************************************************************************************
private createCommentReviewNotification(comment : Comment,date? : string) : ReviewNotification{
    if(!date)
        date = new Date().toISOString();
    let r  = <ReviewNotification>
            (<CommentedReviewNotificationBuilder>(this.buildersManager.getBuilder(CommentedReviewNotificationBuilder)))
            .start()
            .setComment(comment)
            .setDateModification(date)
            .setReview(comment.review)
            .setState(true)
            .build();

    return r;
  }  
//****************************************************************************************************
}
