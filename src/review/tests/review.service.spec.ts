import { Comment } from './../../database/entity/comments/comment.entity';
import { BadRequestException } from '@nestjs/common';
import { Where } from '../services/query-helper.interface';
import { reviewEntityMetaData } from '../../database/entity/review/review.metadata';
import { Review } from '../../database/entity/review/review.entity';
import { User } from '../../database/entity/user/user.entity';
import { UserServiceToken } from '../../user/config';
import { IUserService } from '../../user/services/user.service.interface';
import { ItemReview } from '../../database/entity/item-review/item-review.entity';
import { Image } from '../../database/entity/image/image.entity';
import { ImageRepositoryToken, ItemReviewRepositoryToken, ReviewServiceToken } from '../config';
import { CommonModule } from '../../common/common.module';
import { DatabaseModule } from '../../database/database.module';
import { UserModule } from '../../user/user.module';
import { Test, TestingModule } from '@nestjs/testing';
import { reviewProviders } from '../providers/review.provider';
import { IReviewService } from '../services/review.service.interface';
import { EntityValidator } from '../../common/validator/entity.validator';
import { validate } from 'class-validator';
import { ValidationException } from '../../common/exceptions/validation.exception';


describe('ReviewService tests', () => {
  let reviewService : IReviewService;
  let userService : IUserService;
  let user : User;
  let c : Comment;
  let review : Review;
  let validator : EntityValidator;
//*************************************************************************************
  beforeAll(async () => {
      const module: TestingModule = await Test.createTestingModule({
          imports : [UserModule,DatabaseModule,CommonModule],
          providers : reviewProviders
      }).compile();

      reviewService = module.get<IReviewService>(ReviewServiceToken)
      userService = module.get<IUserService>(UserServiceToken);
      validator = module.get<EntityValidator>(EntityValidator);
  });
//*************************************************************************************
  it('should be defined', () => {
      expect(reviewService).toBeDefined();
      expect(userService).toBeDefined();
  });
//*************************************************************************************  
  it('should get user associed to review', async ()  => {
      user = (await userService.findAll())[0];
      expect(user.name).toBeDefined();
  });
//*************************************************************************************
  it('should add review', async ()  => {
      review = new Review();
      review.link='test link' ;
      review.title='test title';
      review.description = 'description test';
      // image
      let image : Image = new Image();
      image.name="test.jpg";
      image.path="/test/path";
      image.desc="desc test";
      // item 1
      let item : ItemReview = new ItemReview();
      item.key = 'key test 0058433';
      item.values = 'test value';
      // item 2
      let item2 : ItemReview = new ItemReview();
      item2.key = 'key test 00584 2 323';
      item2.values = 'test value 2 ';
      review.image = image;
      review.items = [];
      review.items.push(item,item2);
      review.user = user;
      review = await reviewService.addReview(review);
      expect(review.id).toBeDefined();
      expect(review.image.id).toBeDefined();
      expect(review.items[0].id).toBeDefined();
  });
//*************************************************************************************
  test('should throw ValidationException Review', async ()  => {
    expect(review).toBeDefined();
    review.description=undefined;
    expect(validator.validate(review,{groups : [reviewEntityMetaData.validationGroups.update]},Review)).rejects.toThrow(ValidationException);
  });  
//*************************************************************************************
  test('should update Review', async ()  => {
      expect(review).toBeDefined();
      review.description='desc updated';
      review.items[0].key='updatedkey';
      let item : ItemReview = new ItemReview();
      item.key = 'new';
      item.values = 'test value';
      review.items.push(item);
      let r = await reviewService.updateReview(review);
      expect(r).toBeTruthy();

  });
  //************************************************************************************ 
  test('should get minimized Review', async ()  => {
      expect(review).toBeDefined();
      expect(review.id).toBeDefined();
      let r = await reviewService.getMinimizedReview(review);
      expect(r).toBeDefined();
      expect(r.items).toBeUndefined();
  });
  //************************************************************************************ 
  test('should get detailled Review', async ()  => {
    expect(review).toBeDefined();
    expect(review.id).toBeDefined();
    let r = await reviewService.getReviewById(review);
    expect(r).toBeDefined();
    expect(r.items).toBeDefined();
    expect(r.image).toBeDefined();
    expect(r.user).toBeDefined();
});
//************************************************************************************
  test('should get all-reviews', async ()  => {
    let nbElement : number = 3 ;
      // defin condition (simple condition to test operator)
      let where : Where = {
              where : reviewEntityMetaData.columnsAlias.id + ' > :val',
              params : {val : '0'}
              };           
      // get reviews 
      let r : Review[] = await reviewService.getAllReviews(
        {
          wheres : [where],
          orderBy : [{column : reviewEntityMetaData.columnsAlias.id , order : "DESC"}],
          take : nbElement,
          keywordSearch : 'test'
        }
      );
      expect(r).toBeDefined();
      expect(r.length).toBeLessThanOrEqual(nbElement);
  });   
//*************************************************************************************  
  test('should delete Review', async ()  => {
      let r  = await reviewService.deleteReview(review);
      expect(r).toBeTruthy();
  });
//*************************************************************************************  
  test('should throw BadRequest when updating',  ()  => {
    expect(reviewService.updateReview(review)).rejects.toThrow(BadRequestException)
  });
//*************************************************************************************
  test('should throw BadRequest when deleting',  ()  => {
    expect(reviewService.deleteReview(review)).rejects.toThrow(BadRequestException)
  });
//*************************************************************************************
});
