import { Where } from './../services/query-helper.interface';
import { reviewEntityMetaData } from './../../database/entity/review/review.metadata';
import { Review } from '../../database/entity/review/review.entity';
import { User } from '../../database/entity/user/user.entity';
import { UserServiceToken } from './../../user/config';
import { IUserService } from './../../user/services/user.service.interface';
import { ItemReview } from '../../database/entity/item-review/item-review.entity';
import { Image } from '../../database/entity/image/image.entity';
import { ImageRepositoryToken, ItemReviewRepositoryToken } from './../config';
import { CommonModule } from './../../common/common.module';
import { DatabaseModule } from './../../database/database.module';
import { UserModule } from './../../user/user.module';
import { Test, TestingModule } from '@nestjs/testing';
import { IReviewCrudRepository } from '../repositorys/review/review-crud.interface.repository';
import { reviewProviders } from '../providers/review.provider';
import { ReviewRepositoryToken } from '../config';
import { IImageRepositoy } from '../repositorys/image/image.repository.interface';
import { IItemReviewRepository } from '../repositorys/item-review/item.repository.interface';


describe('ReviewRepository tests', () => {
  let reviewRepository: IReviewCrudRepository;
  let userService : IUserService;
  let itemReviewRepository: IItemReviewRepository;
  let user : User;
  let review : Review;
//*************************************************************************************
  beforeAll(async () => {
      const module: TestingModule = await Test.createTestingModule({
          imports : [UserModule,DatabaseModule,CommonModule],
          providers : reviewProviders
      }).compile();

      reviewRepository = module.get<IReviewCrudRepository>(ReviewRepositoryToken);
      itemReviewRepository = module.get<IItemReviewRepository>(ItemReviewRepositoryToken);
      userService = module.get<IUserService>(UserServiceToken);
  });
//*************************************************************************************
  it('should be defined', () => {
      expect(reviewRepository).toBeDefined();
      expect(itemReviewRepository).toBeDefined();
      expect(userService).toBeDefined();
  });
//*************************************************************************************  
  it('should get user associed to review', async ()  => {
      user = (await userService.findAll())[0];
      expect(user.name).toBeDefined();
  });
//*************************************************************************************
  it('should add review', async ()  => {
      review = new Review();
      review.link='test link' ;
      review.title='test title';
      review.description = 'description test';
      // item 1
      let item : ItemReview = new ItemReview();
      item.key = 'key test 005258828433';
      item.values = 'test value';
      // item 2
      let item2 : ItemReview = new ItemReview();
      item2.key = 'key test 0057258784 2 323';
      item2.values = 'test value 2 ';
      review.image = new Image();
      review.image.name='empty';
      review.image.path='empty';
      review.image.desc='empty';
      review.items = [];
      review.items.push(item,item2);
      review.user = user;
      review = await reviewRepository.addReview(review);
      expect(review.id).toBeDefined();
      expect(review.image.id).toBeDefined();
      expect(review.items[0].id).toBeDefined();
  });
//*************************************************************************************
  test('should update Review', async ()  => {
      expect(review).toBeDefined();
      review.description='desc updated';
      review.items[0].key='updatedkey';
      let item : ItemReview = new ItemReview();
      item.key = 'new';
      item.values = 'test value';
      review.items.push(item);
      review = await reviewRepository.updateReview(review);
      expect(review.description).toEqual('desc updated');
      expect(review.items[0].key).toEqual('updatedkey');
      expect(review.items.length).toEqual(3);
  });
//************************************************************************************
  test('should get all-reviews', async ()  => {
    let nbElement : number = 3 ;
      // defin condition (simple condition to test operator)
      let where : Where = {
              where : reviewEntityMetaData.columnsAlias.id + ' > :val',
              params : {val : '0'}
              };           
      // get reviews 
      let r : Review[] = await reviewRepository.getReviews(
        {
          wheres : [where],
          orderBy : [{column : reviewEntityMetaData.columnsAlias.id , order : "DESC"}],
          take : nbElement,
          keywordSearch : 'test'
        }
      );
      expect(r).toBeDefined();
      expect(r.length).toBeLessThanOrEqual(nbElement);
  });   
//*************************************************************************************  
  test('should delete Review', async ()  => {
      let r : Review = await reviewRepository.deleteReview(review);
      expect(r).toBeDefined();
  });
//*************************************************************************************

});
