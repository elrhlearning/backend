import { Where } from '../services/query-helper.interface';
import { reviewEntityMetaData } from '../../database/entity/review/review.metadata';
import { Review } from '../../database/entity/review/review.entity';
import { User } from '../../database/entity/user/user.entity';
import { UserServiceToken } from '../../user/config';
import { IUserService } from '../../user/services/user.service.interface';
import {  CommentServiceToken } from '../config';
import { CommonModule } from '../../common/common.module';
import { DatabaseModule } from '../../database/database.module';
import { UserModule } from '../../user/user.module';
import { Test, TestingModule } from '@nestjs/testing';
import { IReviewCrudRepository } from '../repositorys/review/review-crud.interface.repository';
import { reviewProviders } from '../providers/review.provider';
import { ReviewRepositoryToken } from '../config';
import { ICommentService } from '../services/comment/comment-service.interface';
import { Comment } from '../../database/entity/comments/comment.entity';
import { BadRequestException } from '@nestjs/common';
import { ValidationException } from '../../common/exceptions/validation.exception';


describe('CommentService tests ', () => {
  let reviewRepository: IReviewCrudRepository;
  let userService : IUserService;
  let commentService : ICommentService;
  let user : User;
  let comment : Comment;
  let id : number;
  let review : Review;
//*************************************************************************************
  beforeAll(async () => {
      const module: TestingModule = await Test.createTestingModule({
          imports : [UserModule,DatabaseModule,CommonModule],
          providers : reviewProviders
      }).compile();

      reviewRepository = module.get<IReviewCrudRepository>(ReviewRepositoryToken);
      userService = module.get<IUserService>(UserServiceToken);
      commentService = module.get<ICommentService>(CommentServiceToken);
  });
//*************************************************************************************
  it('should be defined', () => {
      expect(reviewRepository).toBeDefined();
      expect(userService).toBeDefined();
      expect(commentService).toBeDefined();
  });
//*************************************************************************************  
  it('should get user associed to review', async ()  => {
      user = (await userService.findAll())[0];
      expect(user.name).toBeDefined();
  });
//*************************************************************************************
  it('should add review', async ()  => {
      review = new Review();
      review.link='test link' ;
      review.title='test title';
      review.description = 'description test';
      review.user = user;
      review = await reviewRepository.addReview(review);
      expect(review.id).toBeDefined();
  });
//*************************************************************************************
  test('should add comment', async ()  => {
      expect(review).toBeDefined();
      comment = new Comment();
      comment.content = 'test comment';
      comment.review = review;
      comment.owner = user;
      // save
      comment = await commentService.addComment(comment);
      expect(comment).toBeDefined();
      expect(comment.id).toBeDefined();
  });
//*************************************************************************************  
  test('should get minimized comment', async ()  => {
    expect(comment).toBeDefined();
    expect(comment.id).toBeDefined();
    // save
    comment = await commentService.getMinimizedComment(comment);
    expect(comment).toBeDefined();
    expect(comment.id).toBeDefined();
    expect(comment.owner).toBeUndefined();
  });
//*************************************************************************************  
  test('should get detailled comment', async ()  => {
    expect(comment).toBeDefined();
    expect(comment.id).toBeDefined();
    // save
    comment = await commentService.getComment(comment);
    expect(comment).toBeDefined();
    expect(comment.id).toBeDefined();
    expect(comment.owner).toBeDefined();
    expect(comment.review).toBeDefined();
  });     
//*************************************************************************************  
  test('should update comment', async ()  => {
    id = comment.id;
    comment.content='updated';
    comment = await commentService.updateComment(comment);
    expect(comment).toBeDefined();
    expect(comment.id).toEqual(id);
    expect(comment.content).toEqual('updated');
}); 
//*************************************************************************************  
  test('should delete comment', async ()  => {
    comment = await commentService.deleteComment(comment);
    expect(comment).toBeDefined();
  }); 
//*************************************************************************************  
  test('should throw badrequerst', async ()  => {
    expect((commentService.getMinimizedComment(comment))).rejects.toThrow(BadRequestException);
  });   
//*************************************************************************************
  test('should delete Review', async ()  => {
      let r : Review = await reviewRepository.deleteReview(review);
      expect(r).toBeDefined();
  });
//*************************************************************************************

});
