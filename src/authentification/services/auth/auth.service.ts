import { userEntityMetaData } from '../../../database/entity/user/user.metadata';
import { PrivateKeyPath, verifyOptionsConst, signOptionsConst } from './../../config';
import { Injectable, Inject, UnauthorizedException } from '@nestjs/common';
import { IUserService } from './../../../user/services/user.service.interface';
import { UserServiceToken } from './../../../user/config';
import { JwtPayload } from 'src/authentification/interfaces/jwt-payload.interface';
import { User } from '../../../database/entity/user/user.entity';
import { plainToClass, classToPlain } from 'class-transformer';
import { IAuthService } from './../../../authentification/interfaces/auth.service.interface';
import { AppLogger } from '../../../common/loggers/app-logger';

var jwt = require('jsonwebtoken');
var fs = require('fs');

@Injectable()
export class AuthService implements IAuthService{
//******************************************************************************************************************************  
    constructor(
        @Inject(UserServiceToken) private readonly usersService: IUserService,
        private readonly logger : AppLogger
      ) {}    
//****************************************************************************************************************************** 
      async signIn(user : User): Promise<any> {
          this.logger.init(__filename);
          await this.usersService.validateEntity(user, {groups : userEntityMetaData.validationGroups.signIn})  
          user = await this.usersService.signIn(user);
          let userPayload = classToPlain(user);    
          let token : any = jwt.sign(userPayload,fs.readFileSync(PrivateKeyPath),signOptionsConst);
          user.password = null;
          user.token =  {
            accessToken : token,
            expiresIn : verifyOptionsConst.expiresIn,
            date : new Date()
          };
          this.logger.getLogger().info(`signIn() - user authentificated : ${this.logger.logObject(user)}`);
          return user; 
    }
//****************************************************************************************************************************** 
      async validateUser(payload: JwtPayload): Promise<any> {
        this.logger.init(__filename);
        if(!payload.email || !payload.password)
          throw new UnauthorizedException();
        try {
          return await this.usersService.signIn(plainToClass(User,payload));  
        } catch (error) {
          this.logger.getLogger().error(`validateUser() - error : ${this.logger.logObject(error)}`);
        }
      }
//******************************************************************************************************************************       
}
