import { PrivateKeyPath, signOptionsConst } from './../../config';
import { JwtPayload } from 'src/authentification/interfaces/jwt-payload.interface';
import { User } from '../../../database/entity/user/user.entity';
import { classToPlain } from 'class-transformer';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from '../auth/auth.service';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { PublicKeyPath, verifyOptionsConst } from './../../../authentification/config';
const fs = require('fs');
import * as passport from 'passport';

/**
 * Class to define strategy that will be used in authentification (JWT with private and public key)
 */
@Injectable()
export class JwtStrategyService extends Strategy {
  //******************************************************************************************************************************   
    constructor(private readonly authService: AuthService) {
      super(
        {
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            passReqToCallback: true,
            secretOrKey: fs.readFileSync(PublicKeyPath),
            expiresIn: 3600,
            algorithm : signOptionsConst.algorithm,
            issuer : signOptionsConst.issuer
        },
        async (req, payload, next) => await this.verify(req, payload, next)
      );
      passport.use(this);
    }
//******************************************************************************************************************************     
    public async verify(req, payload : JwtPayload, done) {
      let validUser : User  = <User>await this.authService.validateUser(payload);
      if (!validUser) {
          return done('Unauthorized', false);
      }
      validUser.password = undefined;
      req.user = classToPlain(validUser);
      done(null, payload);
  }
//******************************************************************************************************************************  
}