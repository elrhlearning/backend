const path = require("path");

export const PrivateKeyPath : string = __dirname + "/private.key"
export const PublicKeyPath : string = __dirname + "/public.key";
const expiration : number = 12*60*60;
export const signOptionsConst =  {
    expiresIn: expiration,
    algorithm : "RS256",
    issuer : 'ElrhFirstNodeJSApp'
}
export const verifyOptionsConst =  {
    expiresIn: expiration,
    algorithm : ["RS256"],
    issuer : 'ElrhFirstNodeJSApp'
}

export const AuthServiceToken : string = "AST";
