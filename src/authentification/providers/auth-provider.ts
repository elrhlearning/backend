import { AuthService } from "../services/auth/auth.service";
import { AuthServiceToken } from "../config";
import { JwtStrategyService } from "../services/jwt-strategy/jwt-strategy.service";
import { RolesGuard } from "../guards/role-guard";

export const authProviders = [
    JwtStrategyService,
    AuthService,
    {
        provide : AuthServiceToken,
        useClass : AuthService
    },
    RolesGuard
];
export const authProvidersToExport = [
    AuthService,
    {
        provide : AuthServiceToken,
        useClass : AuthService
    },
];