/**
 * attribute that will used in the JWT payload
 */
export interface JwtPayload {
    email: string;
    name : string;
    lastname : string;
    password : string;
    roles : []
}