import { User } from "../../database/entity/user/user.entity";
import { JwtPayload } from "./jwt-payload.interface";

/**
 * @author KARIM EL RHOURHA
 */
export interface IAuthService  {
//******************************************************************************************************************************    
    /**
     * singIn the user after credential verification
     * @param user userData sent
     */
    signIn(user : User): Promise<any> ;
//******************************************************************************************************************************
    /**
     * validate user that send the token to acces the APi
     * @param payload payload decoded
     */
    validateUser(payload: JwtPayload): Promise<any>
//******************************************************************************************************************************    
}