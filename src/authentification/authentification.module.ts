import { Module, forwardRef } from '@nestjs/common';
import { UserModule } from './../user/user.module';
import { AuthentificationController } from './controllers/authentification/authentification.controller';
import { PrivateKeyPath, signOptionsConst, PublicKeyPath, verifyOptionsConst } from './config';
import { authProviders, authProvidersToExport } from './providers/auth-provider';
const fs = require('fs');


/**
 * configure public and private key
 */
@Module({
    imports: [
        forwardRef(() => UserModule),
      ],
      providers: [...authProviders],
      controllers: [AuthentificationController],
      exports : [...authProvidersToExport]
})
export class AuthentificationModule {}
