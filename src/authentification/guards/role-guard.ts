import { User } from './../../database/entity/user/user.entity';
import { Role } from './../../database/entity/role/role.entity';
import { UserServiceToken } from './../../user/config';
import { Injectable, CanActivate, ExecutionContext, Inject } from '@nestjs/common';
import { Observable } from 'rxjs';
import { IUserService } from 'src/user/services/user.service.interface';
import { Reflector } from '@nestjs/core';


@Injectable()
export class RolesGuard implements CanActivate {
  
    constructor(
                private readonly reflector: Reflector
               ){

    }
//***************************************************************************************************************************************************    
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles || roles.length == 0) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    let user : User = request.user;
    const hasRole = () => user.roles.some((role : Role) => {
      if(roles.indexOf(role.label) != -1)
          return true;
      return false;
    });
    return user && user.roles && hasRole();
  }
//***************************************************************************************************************************************************  
}