import { RequestLoggingInterceptor } from './../../../common/interceptors/request-loggin.interceptor';
import { AppLogger } from '../../../common/loggers/app-logger';
import { Controller, Post, Body, Inject, UseInterceptors, HttpException } from '@nestjs/common';
import { User } from '../../../database/entity/user/user.entity';
import { AuthServiceToken } from './../../../authentification/config';
import { IAuthService } from './../../interfaces/auth.service.interface';


/**
 * used to get token from server
 */
@Controller('auth')
@UseInterceptors(RequestLoggingInterceptor)
export class AuthentificationController {
//******************************************************************************************************************************    
    constructor(    
                    @Inject(AuthServiceToken) private readonly authService: IAuthService,
                    private readonly logger : AppLogger
               ) {}
//******************************************************************************************************************************
    @Post('token')
    async createToken(@Body()user : User): Promise<any> {
        return await this.authService.signIn(user); 
    }
//******************************************************************************************************************************    
}
