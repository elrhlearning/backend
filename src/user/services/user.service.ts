import { ValidationException } from './../../common/exceptions/validation.exception';
import { userEntityMetaData } from '../../database/entity/user/user.metadata';
import { UserRepositoryToken } from '../config';
import {  Inject, Injectable, BadRequestException, InternalServerErrorException } from '@nestjs/common';
import { User} from '../../database/entity/user/user.entity';
import { IUserRepository } from '../repositorys/user.repository.interface';
import { IUserService } from './user.service.interface';
import { plainToClass } from 'class-transformer';
import { validate, ValidationOptions } from 'class-validator';
import { AppLogger } from '../../common/loggers/app-logger';

@Injectable()
export class UserService implements IUserService{
 //***************************************************************************************************************************************************   
    constructor(  @Inject(UserRepositoryToken) 
                  private readonly userRepository : IUserRepository,
                  private readonly logger : AppLogger
                  ){

    }
//***************************************************************************************************************************************************
    public async findAll() : Promise<any> {
      this.logger.init(__filename);
      try {
        return await this.userRepository.find();   
      } catch (error) {
        this.logger.getLogger().error(`findAll() - error : ${this.logger.logObject(error)}`);
        throw new InternalServerErrorException();
      }
    }   
//***************************************************************************************************************************************************
    public async addUser(user : User) : Promise<number> {
      this.logger.init(__filename);
      let u : User; 
        try {
            u = await this.userRepository.save(user);
        } catch (error) {
            this.logger.getLogger().error(`addUser() - error : ${this.logger.logObject(error)}`);
            throw new InternalServerErrorException();
        }
        this.logger.getLogger().info(
          `findAll() - creating new user : ${this.logger.logObject(u)}`
        );
        return u.id;
    }
//***************************************************************************************************************************************************
    /**
     * delete user by id
     * @param id_user user id
     */

    async deleteUser(id_user : number) : Promise<Boolean>{
      this.logger.init(__filename);
      let r : Boolean;
      try {
        r = await this.userRepository.deleteUserByWhere(userEntityMetaData.columns.id.name+"= :id",{id : id_user});
        this.logger.getLogger().info(`deleteUser() - user deleted ID :  ${id_user}`);
        return r;
      } catch (error) {
        this.logger.getLogger().error(`deleteUser() - error ${this.logger.logObject(error)}`);
        throw new InternalServerErrorException();
      }
    }   
//*************************************************************************************************************************************************** 
    async signIn(user : User) : Promise<User | null> {
        this.logger.init(__filename);
        try {
          user = await this.userRepository.findByEmailAndPassword(user.email,user.password);
          this.logger.getLogger().info(`signIn() - user signIn ${this.logger.logObject(user)}`);
          return user;
        } catch (error) {
          this.logger.getLogger().error(`signIn() - error ${this.logger.logObject(error)}`);
          throw new InternalServerErrorException();
        }
    }
//***************************************************************************************************************************************************    
    async getUser(user: User): Promise<User | any> {
      this.logger.init(__filename);
      try {
        user = await this.userRepository.getUser(user.id);
        this.logger.getLogger().info(`getUser() - user : ${this.logger.logObject(user)}`);
        return user;
      } catch (error) {
          this.logger.getLogger().error(`getUser() - error ${this.logger.logObject(error)}`);
          throw new InternalServerErrorException(); 
      }
    }    
//***************************************************************************************************************************************************    
    async getUserWithImage(user: User): Promise<User> {
      this.logger.init(__filename);
      try {
        user = await this.userRepository.getUser(user.id,true,false); 
        this.logger.getLogger().info(`getUserWithImage() - user : ${this.logger.logObject(user)}`);
        return user;
      } catch (error) {
          this.logger.getLogger().error(`getUserWithImage() - error ${this.logger.logObject(error)}`);
          throw new InternalServerErrorException(); 
      }   
    }
//***************************************************************************************************************************************************    
    async getUserWithRoles(user: User): Promise<User> {
        this.logger.init(__filename);
        try {
          user = await this.userRepository.getUser(user.id,false,true);
          this.logger.getLogger().info(`getUserWithImage() - user : ${this.logger.logObject(user)}`);
          return user;
        } catch (error) {
            this.logger.getLogger().error(`getUserWithImage() - error ${this.logger.logObject(error)}`);
            throw new InternalServerErrorException(); 
        } 
}
//***************************************************************************************************************************************************
    async validateEntity(user: User,validationOptions ? : ValidationOptions ) : Promise<User> {
        if(!(user instanceof User))
            user = plainToClass(User,<Object>user);
        const errors: any = await validate(user,validationOptions);      
        if (errors.length > 0) {
            throw new ValidationException(errors);
        }
        return user;
}
//***************************************************************************************************************************************************
} 