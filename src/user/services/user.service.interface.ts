import { User } from '../../database/entity/user/user.entity';
export interface IUserService {
    findAll() : Promise<any>;
    addUser(user : User) : Promise<number>;
    deleteUser(id_user : number) : any;
    signIn(user : User) : Promise<User | null>;
    getUser(user : User) : Promise<User | any>;
    getUserWithImage(user : User) : Promise<User | any>;
    getUserWithRoles(user : User) : Promise<User | any>;
    validateEntity(user: User,validationOptions ? : any ) : Promise<User>;
}