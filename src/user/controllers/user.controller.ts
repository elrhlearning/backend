import { userEntityMetaData } from '../../database/entity/user/user.metadata';
import { Roles } from './../../authentification/decorators/roles.decorator';
import { RolesGuard } from './../../authentification/guards/role-guard';
import { RequestLoggingInterceptor } from './../../common/interceptors/request-loggin.interceptor';
import { UserServiceToken } from '../config';
import { Controller, Get, Param, Post, Body, Delete, Inject, UseGuards, UseInterceptors } from '@nestjs/common';
import { User} from '../../database/entity/user/user.entity';
import { IUserService } from '../services/user.service.interface';
import { EntityValidator } from '../../common/validator/entity.validator';





@Controller('user')
@UseInterceptors(RequestLoggingInterceptor)
export class UserController {
//***************************************************************************************************************************************************
    constructor(
        @Inject(UserServiceToken) private userService : IUserService,
        private readonly valadator : EntityValidator
        ){

    }
//***************************************************************************************************************************************************
    @Get()
    @Roles('user')
    getAllUsers() : any{
        return this.userService.findAll();
    }   
//***************************************************************************************************************************************************
    @Post()
    async addUser(@Body() user : User) : Promise<number>{
        user = <User> await this.valadator.validate(user,{groups : [userEntityMetaData.validationGroups.addUser]},User);
        return this.userService.addUser(user);
    }
//***************************************************************************************************************************************************   
    @Delete(':id')
    async deleteUser(@Param() user : User) : Promise<Boolean>{
        user = <User> await this.valadator.validate(user,{groups : [userEntityMetaData.validationGroups.id]},User);
        return this.userService.deleteUser(user.id);
    }  
//***************************************************************************************************************************************************    
}
