import { CommonModule } from './../../common/common.module';
import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { DatabaseModule } from '../../database/database.module';
import { userProviders } from '../providers/user.provider';
import { User } from '../../database/entity/user/user.entity';


describe('User Controller', () => {
  let controller: UserController;
  let user : User = new User();
// ********************************************************************************************
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports : [DatabaseModule,CommonModule],
      controllers: [UserController],
      providers: [...userProviders]
    }).compile();

    controller = module.get<UserController>(UserController);
  });
// ********************************************************************************************
  test('should add user', async () => {
      let userObject : any = {
        "name": "elrhourha",
        "lastname": "laila",
        "email": "karim.elrhourha@gmail.com",
        "password": "salutcava"
      };  
      try {
          let id : number = await controller.addUser(userObject);
          expect(id).toBeDefined();
          expect(id).toBeGreaterThan(0);
          user.id = id;
      } catch (error) {
          expect(error).toBeUndefined();
      }
  });
// ********************************************************************************************  
  test('sould delete user inserted before this test', async () => {
    try {
        let r : Boolean = await controller.deleteUser(user);
        expect(r).toBeDefined();
        expect(r).toBeTruthy();
    } catch (error) {
        expect(error).toBeUndefined();
    }
  });
// ********************************************************************************************
});
