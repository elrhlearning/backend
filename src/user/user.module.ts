import {UserController} from './controllers/user.controller';
import { Module, forwardRef } from '@nestjs/common';
import { userProviders } from './providers/user.provider';
import { AuthentificationModule } from './../authentification/authentification.module';


@Module({
    imports : [forwardRef(() => AuthentificationModule)],
    controllers: [UserController],
    providers: [...userProviders],
    exports : [...userProviders]
})
export class UserModule {}