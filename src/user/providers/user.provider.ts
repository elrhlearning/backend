import { UserService } from '../services/user.service';
import { UserRepository } from '../repositorys/user.repository';
import { UserRepositoryToken, UserServiceToken } from '../config';

export const userProviders = [
    UserRepository,
    {
        provide : UserRepositoryToken,
        useClass : UserRepository
    },
    UserService,
    {
        provide : UserServiceToken,
        useClass : UserService  
    }
];