import { imageEntityMetaData } from '../../database/entity/image/image.metadata';
import { roleEntityMetaData } from '../../database/entity/role/role.metadata';
import { userEntityMetaData } from '../../database/entity/user/user.metadata';
import { UserTypeOrmRepositoryToken, CacheTokens} from '../../database/config';
import { Inject,Injectable } from '@nestjs/common';
import { Repository, DeleteResult, SelectQueryBuilder } from "typeorm";
import { IUserRepository } from './user.repository.interface';
import { User} from '../../database/entity/user/user.entity';



@Injectable()
export class UserRepository implements IUserRepository{
//***************************************************************************************************************************************************      
    private cacheTokensArray : any[] = null;
//***************************************************************************************************************************************************
    constructor(@Inject(UserTypeOrmRepositoryToken) private userRepository: Repository<User>){
    }
//***************************************************************************************************************************************************      
    public async deleteUserByWhere(where : string,param : any) : Promise<Boolean>{
        let r : DeleteResult = await this.getQueryBuilder().delete().where(where,param).execute();
        if(r != null)
            return true;
        return false;
    }
//***************************************************************************************************************************************************
    public async finUsersThatHaveAttribute(attribute : string) : Promise<User[]>{
        return  this.getQueryBuilder()
                            .leftJoinAndSelect(userEntityMetaData.relations.role, roleEntityMetaData.tableName)
                            .where(attribute + " IS NOT NULL")
                            .getMany();
    }
//***************************************************************************************************************************************************
    private getQueryBuilder() : SelectQueryBuilder<User>{
        return this.userRepository.createQueryBuilder(userEntityMetaData.tableName);
    } 
//***************************************************************************************************************************************************       
    public find(): Promise<User[]> {
        return this.getQueryBuilder()
                            .leftJoinAndSelect(userEntityMetaData.relations.role, roleEntityMetaData.tableName)
                            .getMany();
    }
//***************************************************************************************************************************************************
    public save(user: User): Promise<User> {
        return this.userRepository.save(user);
    } 
//***************************************************************************************************************************************************   
    public async findByEmailAndPassword(email: string, password: string): Promise<User> {
        let emailColumn = userEntityMetaData.tableName + '.' + userEntityMetaData.columns.email.name;
        let passwordColumn = userEntityMetaData.tableName + '.' + userEntityMetaData.columns.password.name;
        let user : User = await   
                            this.getQueryBuilder()
                            .addSelect(passwordColumn)
                            .leftJoinAndSelect(userEntityMetaData.relations.role,roleEntityMetaData.tableName)
                            .where(emailColumn +"= :email",{email : email})
                            .andWhere(passwordColumn + "= :password",{password : password})
                            .getOne();
        if(!user || !user.id)
            return null;
        return user;
    }    
//***************************************************************************************************************************************************    
    getUser(id: number,withImage? : boolean,withRoles? : boolean): Promise<User> {
        let queryBuilder = this.getQueryBuilder();
        let idColumn =  userEntityMetaData.tableName + '.' +userEntityMetaData.columns.id.name;
        if(withImage)
            queryBuilder = queryBuilder.leftJoinAndSelect(userEntityMetaData.relations.image,imageEntityMetaData.tableName)
        if(withRoles)
            queryBuilder = queryBuilder.leftJoinAndSelect(userEntityMetaData.relations.role,roleEntityMetaData.tableName);

        return  queryBuilder
                .where(idColumn + "= :id_param",{id_param : id})
                .getOne();
    }
//***************************************************************************************************************************************************    
}