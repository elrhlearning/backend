import { User } from "../../database/entity/user/user.entity";


/**
 * User repository
 */
export interface IUserRepository {
//********************************************************************************************************************   
    /**
     * get all users from database, the app store data in cache for 60000 miliseconds
     */
    find() : Promise<User[]>;
//********************************************************************************************************************   
    /**
     * 
     * @param user user entity that will be saved in database
     * @returns Promise<number> : user entity after insertion (with id)
     */
    save(user : User) : Promise<User>;
//********************************************************************************************************************   
    /**
     * 
     * @param where where query, should be one condition
     * @param param param object 
     * example : this.deleteUserByWhere("user_id = :user_id",{user_id : 4}
     */
    deleteUserByWhere(where : string,param : any) : Promise<Boolean>;
//********************************************************************************************************************   
    /**
     * get all users that have the @param attribute defined in the database
     * @param attribute string that represente attribut of the entity
     */
    finUsersThatHaveAttribute(attribute : string) : Promise<User[]>;
//********************************************************************************************************************   
    /**
     * 
     * @param email get User by email and password
     * @param password 
     */
    findByEmailAndPassword(email:string, password : string) : Promise<User>;
//********************************************************************************************************************
    /**
     * find user by ID
     * @param id user id
     */
    getUser(id : number,withImage? : boolean,withRoles? : boolean) : Promise<User>;      
//********************************************************************************************************************     
}