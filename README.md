## Description

PlanetReview API - my first NestJS realtime app 

## Preview

App deployed in Heroku Cloud :  [PlanetReview](https://planetreviews.herokuapp.com/ "PlanetReview")

## Architecture

*  NestJS :  is a framework for building efficient, scalable Node.js with full support of typescript based on Angular architecture, Nest provide :
    *  Dependency Injection
    *  Middelware and Guards for security
    *  HTTP Interceptor 
    *  Integration with TypeORM 
    *  Unit testing with Jest Test
    *  E2E testing with Mocha
    *  Full Integration with Socket.io
    *  Integration with passport-iwt
*  [TypeORM](http://typeorm.io "TypeORM") for Mapping Object Relationnel
*  Winston Logger for logging
*  Socket.io for realtime interaction (messaging - notifications)
*  SQLLite for Relationnel Database
*  passport-jwt : the API is secured with TOKEN based authentification, the verification is handled by Nest Middleware
*  class-validator : library used with TypeORM to validate entity in controller before starting services.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

```

## License

  Nest is [MIT licensed](LICENSE).
